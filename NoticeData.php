<?php
// Connecting to the database
include("connection.php");
include("session.php");

  if (!isset($_SESSION['session'])) {
        header('Location: index.php');
        session_destroy();
    }
        echo $_POST["name"]."<br>";
        echo $_POST["user_id"]."<br>";
        echo $_POST["title"]."<br>";
        echo $_POST["email"]."<br>";
        echo $_POST["description"]."<br>";
        echo $_POST["startdate"]."<br>";
        echo $_POST["enddate"]."<br>";
        $now = date("Y/m/d H:m:s"); 

if(isset($_POST["name"]) && isset($_POST["user_id"])&& isset($_POST["email"]) && isset($_POST["title"]) && isset($_POST["description"]) && isset($_POST["startdate"]) && isset($_POST["enddate"])){

        
        $created_by = $_POST["name"];
        $user_id = $_POST["user_id"];
        $createdby_email = $_POST["email"];      
        $title = $_POST["title"];	
        $startdate = date("Y-m-d", strtotime($_POST["startdate"])); 
        $enddate = date("Y-m-d", strtotime($_POST["enddate"]));
        $description = $_POST["description"];
        $created_date = $now;
        $status = "New";           
      try
      {        
        $stmt = $conn->prepare("INSERT INTO notice (user_id, created_by, title, description, created_date, startdate, enddate, status, createdby_email) VALUES  (:user_id, :created_by, :title, :description, :created_date, :startdate, :enddate, :status, :createdby_email)");         
        $stmt->bindParam(':user_id', $user_id);
        $stmt->bindParam(':created_by', $created_by);       
        $stmt->bindParam(':title', $title);       
        $stmt->bindParam(':description', $description);
        $stmt->bindParam(':startdate', $startdate);
        $stmt->bindParam(':enddate', $enddate);
        $stmt->bindParam(':created_date', $created_date);
        $stmt->bindParam(':status', $status);
        $stmt->bindParam(':createdby_email', $createdby_email);
		$stmt->execute();
      } catch(PDOException $e){
        $message = "ERROR : ".$e->getMessage();  
      }
}
echo $message;
header('Location: notice.php');
?>