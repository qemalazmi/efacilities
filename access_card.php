<?php
    include('header.php');
    include("session.php");



?>

				<div class="app-content my-3 my-md-5">
					<div class="side-app">
						<div class="page-header">
							
							<ol class="breadcrumb1">
											<li class="breadcrumb-item1 active">Fix Assets Management</li>
											<li class="breadcrumb-item1 active">Access Card</li>
							</ol>
                                    </div>
                       <!-- Message Modal -->
                        <div class="modal fade" id="AddAccessCard" tabindex="-1" role="dialog"  aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form id="modal" class="form-horizontal" method="POST" action="accesscard_add.php">
                                   <div class="modal-header">
                                        <h5 class="modal-title" id="example-Modal3">Add Access Card</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                            <div class="form-group">
                                                <label for="recipient-name" class="form-control-label">Unit Access Card:</label>
                                                <input type="text" class="form-control" name="unit_AccessCard" id="unit_AccessCard">
                                            </div>
                                            <div class="form-group">
                                                <label for="recipient-name" class="form-control-label">Status:</label>
                                               <select class="form-control select2 custom-select" data-placeholder="Choose one" name="status" id="status">
                                                            <option label="Choose one"></option>
                                                            <option value="available">Available</option>
                                                            <option value="Not Available">Not Available</option>
                                                </select>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Insert Access Card</button>
                                            </div>
                                        
                                    </div>
                                  </form>
                                </div>
                            </div>
                        </div>
                         <!-- Message Modal Edit -->
                        <div class="modal fade" id="EditAccessCard" tabindex="-1" role="dialog"  aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form id="modal" class="form-horizontal" method="POST" action="accesscard_edit.php" enctype="multipart/form-data">
                                   <div class="modal-header">
                                        <h5 class="modal-title" id="example-Modal3">Update Access Card</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                            <div class="form-group">
                                                <label for="recipient-name" class="form-control-label">Unit Access Card:</label>
                                                <input type="text" class="form-control" name="unit" id="unit">
                                            </div>
                                            <div class="form-group">
                                                <label for="recipient-name" class="form-control-label">Status:</label>
                                               <select class="form-control select2 custom-select" data-placeholder="Choose one" name="status" id="status">
                                                            <option label="Choose one"></option>
                                                            <option value="available">Available</option>
                                                            <option value="Not Available">Not Available</option>
                                                </select>
                                            </div>
                                            <div class="modal-footer">
                                                <input type="hidden" name="access_id" id="access_id">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Edit Access Card</button>
                                            </div>
                                        
                                    </div>
                                  </form>
                                </div>
                            </div>
                        </div>
                        <!-- Message Modal view -->
                        <div class="modal fade" id="ViewAccessCard" tabindex="-1" role="dialog"  aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form id="modal" class="form-horizontal" method="POST" action="" enctype="multipart/form-data">
                                   <div class="modal-header">
                                        <h5 class="modal-title" id="example-Modal3">View Access Card</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                            <div class="form-group">
                                                <label for="recipient-name" class="form-control-label">Unit Access Card:</label>
                                                <input type="text" class="form-control" name="unit" id="unit_AccessCard" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label for="message-text" class="form-control-label">Status:</label>
                                                <input type="text" class="form-control" name="status" id="status" readonly>
                                            </div>
                                            <div class="modal-footer">
                                                <input type="hidden" name="access_id" id="access_id">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            </div>
                                        
                                    </div>
                                  </form>
                                </div>
                            </div>
                        </div>
                <div class="row">
				<div class="col-md-12 col-lg-12">
                        <div class="card">
									<div class="card-status bg-azure br-tr-3 br-tl-3"></div>
									<div class="card-header">
										<h3 class="card-title">Record Access Card</h3>
									</div>
									<div class="card-body">
										<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
											<div class="panel panel-default active">
												<div class="panel-heading " role="tab" id="headingOne">
													<h4 class="panel-title">
														<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true"  aria-controls="collapseOne">

															List Access Card 
														</a>
													</h4>
												</div>
												<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
													<div class="panel-body">
                                                        <div class="card-footer text-right">
                                                            <div class="card-body">
                                                                 <button type="button" class="btn btn-info" data-toggle="modal" data-target="#AddAccessCard">Insert Access Card</button>

                                                            </div>
                                                        </div>
                                                       
														
                                            <div class="card-body">
                                                <div class="table-responsive">
                                                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                        <thead>
                                                            <tr>
                                                                <th class="wd-20p">Level</th>
                                                                <th class="wd-20p">No Parking</th>
                                                                <th class="wd-20p">Action</th>

                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                             <?php
                                                             $counter = 1;
                                                               if($job == "admin"){

                                                                        $statement = $conn->prepare("SELECT * FROM access_card");

                                                               }else {
                                                                    $statement = $conn->prepare("SELECT * FROM access_card");

                                                               }
                                                                        $statement->execute();

                                                                       while($data = $statement->fetch(PDO::FETCH_ASSOC))
                                                                       {

                                                                       ?>
                                                                            <tr>
                                                                              <td><?php echo $data["unit_AccessCard"]; ?></td>
                                                                              <td><?php echo $data["status"];?></td>
                                                                                 <td>
                                                                                    <button type="button" name="edit" id="edit" class="btn btn-info btn-xs"  data-toggle="modal" data-target="#ViewAccessCard"
                                                                                    data-access_id="<?php echo $data["access_id"]; ?>"
                                                                                    data-unit="<?php echo $data["unit_AccessCard"];?>" 
                                                                                    data-status="<?php echo $data["status"]; ?>"
                                                                                    > <i class="fa fa-pencil"></i>&nbsp; View</button>

                                                                                    <button type="button" name="edit" id="edit" class="btn btn-success btn-xs"  data-toggle="modal" data-target="#EditAccessCard"
                                                                                    data-access_id="<?php echo $data["access_id"]; ?>"
                                                                                    data-unit="<?php echo $data["unit_AccessCard"]; ?>" 
                                                                                    data-status="<?php echo $data["status"]; ?>"
                                                                                    > <i class="fa fa-pencil"></i>&nbsp; Edit</button>
                                                                                </td>
                                                                            </tr>
                                                                       <?php
                                                                           $counter++;
                                                                       }
                                                                       ?>

                                                        </tbody>
											</table>
										</div>
									</div>
													</div>
												</div>
											</div>
											<div class="panel panel-default mt-2">
												<div class="panel-heading" role="tab" id="headingTwo">
													<h4 class="panel-title">
														<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">

															Record All Record Apply by Resident
														</a>
													</h4>
												</div>
												<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
													<div class="panel-body">
														Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
													</div>
												</div>
											</div>
											<div class="panel panel-default mt-2">
												<div class="panel-heading" role="tab" id="headingThree">
													<h4 class="panel-title">
														<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">

															Collapsible Group Item #3
														</a>
													</h4>
												</div>
												<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
													<div class="panel-body">
														Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
													</div>
												</div>
											</div>
										</div><!-- panel-group -->
									</div>
								</div>
                    </div>
</div>

									<!-- table-wrapper -->
								</div>
								<!-- section-wrapper -->

							</div>
						</div>
					</div>
                    
                    <?php
    
                        include('footer.php');
                    ?>
