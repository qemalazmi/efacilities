<?php
// Connecting to the database
include("connection.php");
include("session.php");
  if (!isset($_SESSION['session'])) {
        header('Location: index.php');
        session_destroy();
    }

if(isset($_POST["unit_AccessCard"]) && isset($_POST["status"])){

        $unit_AccessCard = $_POST["unit_AccessCard"];
        $status = $_POST["status"];
       
      try
      {
        
        $stmt = $conn->prepare("INSERT INTO access_card (unit_AccessCard, status) VALUES  (:unit_AccessCard, :status)");
	    $stmt->bindParam(':unit_AccessCard', $unit_AccessCard);
        $stmt->bindParam(':status', $status);
		$stmt->execute();
      } catch(PDOException $e){
        $message = "ERROR : ".$e->getMessage();  
      }

}
echo $message;

header('Location: access_card.php');

?>