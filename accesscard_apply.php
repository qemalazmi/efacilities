<!doctype html>
<html lang="en" dir="ltr">
<?php
    
    include('header.php');
?>
				<div class="my-3 my-md-5 app-content">
					<div class="side-app">
						<div class="page-header">
							
							<ol class="breadcrumb1">
											<li class="breadcrumb-item1 active">Fix Assets Management</li>
											<li class="breadcrumb-item1 active">Access Card</li>
							</ol>
						</div>

						
							
							<div class="col-lg-12">
								<form class="card" method="POST" action="vendor.php">
									<div class="card-header">
										<h3 class="card-title">Register Vendor</h3>
									</div>
									<div class="card-body">
										<div class="row">
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Company Name</label>
													<input type="text" name="CompanyName" id="CompanyName" class="form-control" required >
												</div>
											</div>
											<div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Office Contact</label>
													<input type="text" name="OfficeContact" id="OfficeContact" class="form-control" required >
												</div>
											</div>                                             
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Name of Officer</label>
													<input type="text" name="NameOfOfficer" id="NameOfOfficer" class="form-control" required >
												</div>
											</div>
											<div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Contact of Officer</label>
													<input type="text" name="ContactOfOfficer" id="ContactOfOfficer" class="form-control" required >
												</div>
											</div>
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Starting Vendor Maintenance</label>
                                                    <input class="form-control fc-datepicker" name="StartMaintenance" id="StartMaintenance" placeholder="MM/DD/YYYY" type="date" required>
												</div>
											</div>
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Ending Vendor Maintenance</label>
                                                    <input class="form-control fc-datepicker" name="EndMaintenance" id="EndMaintenance" placeholder="MM/DD/YYYY" type="date" required>
												</div>
											</div>
                                            <div class="col-sm-6 col-md-12">
												<div class="form-group">
													<label class="form-label">List Maintenance</label>
													<input type="text" name="TypeMaintenance" id="TypeMaintenance" class="form-control" required >
												</div>
											</div>
                                            <div class="col-sm-6 col-md-12">
												<div class="form-group">
													<label class="form-label">Office Address</label>
													<input type="text" name="OfficeAddress" id="OfficeAddress" class="form-control" required >
												</div>
											</div> 
											<div class="col-md-5">
												<div class="form-group">
													<label class="form-label">Postal Code</label>
													<input type="text" name="PostalCode" id="PostalCode" class="form-control"  placeholder="" required >
												</div>
											</div>
											<div class="col-sm-6 col-md-3">
												<div class="form-group">
													<label class="form-label">City</label>
													<input type="text" name="City" id="City" class="form-control" placeholder="" required >
												</div>
											</div>
											<div class="col-sm-6 col-md-4">
												<div class="form-group">
													<label class="form-label">Country</label>
													<input type="text" name="Country" id="Country" class="form-control" placeholder="" required>
												</div>
											</div>						
										</div>
									</div>
                                    <div class="card-footer text-right">
										<button type="submit" class="btn btn-primary" >Cancel</button>
									</div>
									<div class="card-footer text-right">
										<button type="submit" class="btn btn-primary" >Register Vendor</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					
<?php
    
    include('footer.php');
?>

