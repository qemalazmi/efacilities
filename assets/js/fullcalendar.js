 $(window).load(function() {
        var date = new Date(),
            d = date.getDate(),
            m = date.getMonth(),
            y = date.getFullYear(),
            started,
            categoryClass;

        var calendar = $('#calendar1').fullCalendar({
          header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
          },
          selectable: true,
          selectHelper: true,
          select: function(start, end, allDay) {
            //$('#fc_create').click();
            $('#ModalAdd #start_date').val(moment(start).format("YYYY-MM-DD"));
            $('#ModalAdd #start_time').val(moment(start).format("hh:MM A"));
			$('#ModalAdd #end_date').val(moment(end).format("YYYY-MM-DD"));
            $('#ModalAdd #end_time').val(moment(end).format("hh:MM A"));
            $('#ModalAdd').modal('show');

            started = start;
            ended = end;

            $(".antosubmit").on("click", function() {
              var title = $("#title").val();
              if (end) {
                ended = end;
              }

              categoryClass = $("#event_type").val();

              if (title) {
                calendar.fullCalendar('renderEvent', {
                    title: title,
                    start: started,
                    end: end,
                    allDay: allDay
                  },
                  true // make the event "stick"
                );
              }

              $('#title').val('');

              calendar.fullCalendar('unselect');

              $('.antoclose').click();

              return false;
            });
          },
          eventClick: function(calEvent, jsEvent, view) {
            //$('#fc_edit').click();
            //$('#title2').val(calEvent.title);
            $('#ModalEdit #title').val(calEvent.end);
            $('#ModalEdit').modal('show');

            categoryClass = $("#event_type").val();

            $(".antosubmit2").on("click", function() {
              calEvent.title = $("#title2").val();

              calendar.fullCalendar('updateEvent', calEvent);
              $('.antoclose2').click();
            });

            calendar.fullCalendar('unselect');
          },
          editable: true,
          events: [
              <?php
                $searchData = "SELECT * FROM notice WHERE status = 'New'";
                $statement = $conn->prepare($searchData);
                $statement->execute();
                $events = $statement->fetchAll();
              
                foreach($events as $data):
              ?>
               {
                   title: '<?php echo $data["title"]; ?>',
				   event_id: '<?php echo $data["event_id"]; ?>',
                   start: '<?php echo $data["start_date"]; ?>',
                   end: '<?php echo $data["end_date"]; ?>',
               },
              <?php
                endforeach;
              ?>
          ]
        });
      });