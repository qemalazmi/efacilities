<?php
    include('header.php');
    include("session.php");



?>
                <div class="app-content my-3 my-md-5">
					<div class="side-app">
						<div class="page-header">
							
							<ol class="breadcrumb1">
											<li class="breadcrumb-item1 active">Fix Assets Management</li>
											<li class="breadcrumb-item1 active">Unit House</li>
							</ol>
                                    </div>
                        
        

                       <!-- Message Modal -->
                        <div class="modal fade" id="AddBulding" tabindex="-1" role="dialog"  aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form id="modal" class="form-horizontal" method="POST" action="insertBuilding.php">
                                   <div class="modal-header">
                                        <h5 class="modal-title" id="example-Modal3">Add Unit House</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                            <div class="form-group">
                                                <label for="recipient-name" class="form-control-label">Unit House:</label>
                                                <input type="text" class="form-control" name="NoHouse" id="NoHouse">
                                            </div>
                                            <div class="form-group">
                                                <label for="recipient-name" class="form-control-label">level : </label>
                                                <select class="form-control select2 custom-select" data-placeholder="Choose one" name="level" id="level">
                                                            <option label="Choose one"></option>
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                            <option value="13">13</option>
                                                            <option value="14">14</option>
                                                            <option value="15">15</option>
                                                     </select>
                                            </div>
                                        
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Insert unit House</button>
                                            </div>
                                        
                                    </div>
                                  </form>
                                </div>
                            </div>
                        </div>
                         <!-- Message Modal Edit -->
                        <div class="modal fade" id="EditBulding" tabindex="-1" role="dialog"  aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form id="modal" class="form-horizontal" method="POST" action="editBulding.php" enctype="multipart/form-data">
                                   <div class="modal-header">
                                        <h5 class="modal-title" id="example-Modal3">Update Unit House</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                            <div class="form-group">
                                                <label for="recipient-name" class="form-control-label">Unit House:</label>
                                                <input type="text" class="form-control" name="NoHouse" id="NoHouse">
                                            </div>
                                            <div class="form-group">
                                                <label for="recipient-name" class="form-control-label">level : </label>
                                                <input type="text" class="form-control" name="level" id="level">
                                            </div>
                                        
                                            <div class="modal-footer">
                                                <input type="hidden" name="house_ID" id="house_ID">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Edit Unit House</button>
                                            </div>
                                        
                                    </div>
                                  </form>
                                </div>
                            </div>
                        </div>
                        <!-- Message Modal view -->
                        <div class="modal fade" id="ViewBulding" tabindex="-1" role="dialog"  aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form id="modal" class="form-horizontal" method="POST" action="" enctype="multipart/form-data">
                                   <div class="modal-header">
                                        <h5 class="modal-title" id="example-Modal3">View Unit House</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                   <div class="modal-body">
                                            <div class="form-group">
                                                <label for="recipient-name" class="form-control-label">Unit House:</label>
                                                <input type="text" class="form-control" name="NoHouse" id="NoHouse" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label for="recipient-name" class="form-control-label">level : </label>
                                                <input type="text" class="form-control" name="level" id="level" readonly>
                                            </div>
                                            <div class="modal-footer">
                                                <input type="hidden" name="house_ID" id="house_ID">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            </div>
                                    </div>
                                  </form>
                                </div>
                            </div>
                        </div>
                <div class="row">
				<div class="col-md-12 col-lg-12">
                        <div class="card">
									<div class="card-status bg-azure br-tr-3 br-tl-3"></div>
									<div class="card-header">
										<h3 class="card-title">Record unit House</h3>
									</div>
									<div class="card-body">
										<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
											<div class="panel panel-default active">
												<div class="panel-heading " role="tab" id="headingOne">
													<h4 class="panel-title">
														<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true"  aria-controls="collapseOne">

															Unit House
														</a>
													</h4>
												</div>
												<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
													<div class="panel-body">
                                                        <div class="card-footer text-right">
                                                            <div class="card-body">
                                                                 <button type="button" class="btn btn-info" data-toggle="modal" data-target="#AddBulding">Insert New Unit House</button>

                                                            </div>
                                                        </div>
                                                       
														
                                            <div class="card-body">
                                                <div class="table-responsive">
                                                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                        <thead>
                                                            <tr>
                                                                <th class="wd-20p">Unit Rumah</th>
                                                                <th class="wd-20p">Level</th>
                                                                <th class="wd-20p">Action</th>

                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                             <?php
                                                             $counter = 1;
                                                               if($job == "admin"){

                                                                        $statement = $conn->prepare("SELECT * FROM house");

                                                               }else {
                                                                    $statement = $conn->prepare("SELECT * FROM house");

                                                               }
                                                                        $statement->execute();

                                                                       while($data = $statement->fetch(PDO::FETCH_ASSOC))
                                                                       {

                                                                       ?>
                                                                            <tr>
                                                                              <td><?php echo $data["NoHouse"]; ?></td>
                                                                              <td><?php echo $data["level"];?></td>
                                                                                 <td>
                                                                                    <button type="button" name="edit" id="edit" class="btn btn-info btn-xs"  data-toggle="modal" data-target="#ViewBulding"
                                                                                    data-house_ID="<?php echo $data["house_ID"]; ?>"
                                                                                    data-NoHouse="<?php echo $data["NoHouse"];?>" 
                                                                                    data-level="<?php echo $data["level"];?>" 
                                                                                    > <i class="fa fa-pencil"></i>&nbsp; View</button>
                                                                                     
                                                                                     
                                                                                    <button type="button" name="edit" id="edit" class="btn btn-success btn-xs"  data-toggle="modal" data-target="#EditBulding"
                                                                                    data-house_ID="<?php echo $data["house_ID"]; ?>"
                                                                                    data-NoHouse="<?php echo $data["NoHouse"];?>" 
                                                                                    data-level="<?php echo $data["level"];?>" 
                                                                                    > <i class="fa fa-pencil"></i>&nbsp;Edit </button>
                                                                                </td>
                                                                            </tr>
                                                                       <?php
                                                                           $counter++;
                                                                       }
                                                                       ?>

                                                        </tbody>
											</table>
										</div>
									</div>
													</div>
												</div>
											</div>
											<div class="panel panel-default mt-2">
												<div class="panel-heading" role="tab" id="headingTwo">
													<h4 class="panel-title">
														<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">

															All Record Complaint Pool
														</a>
													</h4>
												</div>
												<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
													<div class="panel-body">
														Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
													</div>
												</div>
											</div>
											<div class="panel panel-default mt-2">
												<div class="panel-heading" role="tab" id="headingThree">
													<h4 class="panel-title">
														<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">

															Collapsible Group Item #3
														</a>
													</h4>
												</div>
												<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
													<div class="panel-body">
														Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
													</div>
												</div>
											</div>
										</div><!-- panel-group -->
									</div>
								</div>
                    </div>
</div>

									<!-- table-wrapper -->
								</div>
								<!-- section-wrapper -->

							</div>
						</div>
					</div>
                    
                    <?php
    
                        include('footer.php');
                    ?>
