
<?php
    
    include('header.php');
?>
        <?php
            // Connection database
            //include("connection.php");
            // Check, for session 'user_session'
            include("session.php");

            // Set Default Time Zone for Asia/Kuala_Lumpur
            date_default_timezone_set("Asia/Kuala_Lumpur");

            // Check, if username session is NOT set then this page will jump to login page
            if (!isset($_SESSION['session']) && !isset($_SESSION['job'])) {
                header('Location: login.php');
                session_destroy();
            }

            $job = $_SESSION["job"];
            $now = date("Y-m-d");
        ?>
    
				<div class="app-content my-3 my-md-5">
					<div class="side-app">
						<div class="page-header">
							<h4 class="page-title">Home / Complaint</h4>
							
						</div>
 <?php
                if(isset($_SESSION["session"]))
                {
                    $email = $_SESSION["session"];
                    $sql = "SELECT * FROM user WHERE email = :email";
                    $stmt = $conn->prepare($sql);
                    $stmt->bindParam(":email", $email);
                    $stmt->execute();
                    
                    if($dt = $stmt->fetch(PDO::FETCH_ASSOC))
                    {
                        $user_id = $dt["user_id"];
                        $name = $dt["name"];
                        $email = $dt["email"];
                        $nric = $dt["password"];
                        $NoHouse = $dt["NoHouse"];
                    }
                }
                else
                {
                    echo "Data is not found!";
                }
                ?>

						<div class="row">
							<div class="col-md-12">
								<div class="card">
									<div class="card-status bg-blue br-tr-3 br-tl-3"></div>
									<div class="card-header">
										<div class="card-title"> Complaint Form</div>
									</div>
									<div class="card-body p-6">
										<div class="wizard-container">
											<div class="wizard-card m-0" data-color="blue" id="wizardProfile">
												<form method="post" action="complaintadd.php" enctype="multipart/form-data">
													<div class="wizard-navigation">
														<ul>
															<li><a href="#yourdetail" data-toggle="tab">Your Detail</a></li>
															<li><a href="#typecomplaint" data-toggle="tab">Type Complaint</a></li>
															<li><a href="#picture" data-toggle="tab">Picture</a></li>
														</ul>
													</div>

													<div class="tab-content">
														<div class="tab-pane" id="yourdetail">
														  <div class="row">
																<div class="col-sm-6">
																	<div class="input-group">

																		<div class="form-group label-floating">
																		  <label class="control-label">Full Name</label>
																		  <input name="name"  type="text" value="<?php echo $name; ?>" class="form-control" readonly>
																		</div>
																	</div>
                                                              </div>
                                                              <div class="col-sm-6">
																	<div class="input-group">
																		<div class="form-group label-floating">
																		  <label class="control-label">NRIC / Password</small></label>
																		  <input name="lastname" type="text" value="<?php echo $nric; ?>" class="form-control">
																		</div>
																	</div>
                                                              </div>
                                                            <div class="col-sm-6">
																	<div class="input-group">
																		<div class="form-group label-floating">
																		  <label class="control-label">No.House</label>
																		  <input name="lastname" type="text" class="form-control" value="<?php echo $NoHouse; ?>" readonly>
																		</div>
																	</div>
                                                              </div>
                                                              <div class="col-sm-6">
																	<div class="input-group">
																		<div class="form-group label-floating">
																			<label class="control-label">Email <small>(required)</small></label>
																			<input name="email" type="email" class="form-control" value="<?php echo $email; ?>" readonly>
																		</div>
																	</div>
																</div>
                                                                </div>
												            </div>
														<div class="tab-pane" id="typecomplaint">
															<h4 class="info-text"> What are you doing? (checkboxes) </h4>
															<div class="row">
																	<div class="col-sm-4">
																		<div class="choice" data-toggle="wizard-checkbox">
																			<input type="checkbox" name="service[]" value="Mosque">
																			<div class="icon">
																				<i class="fas fa-mosque"></i>
																			</div>
																			<h6>Mosque</h6>
																		</div>
																	</div>
																	<div class="col-sm-4">
																		<div class="choice" data-toggle="wizard-checkbox">
																			<input type="checkbox" name="service[]" value="Parking">
																			<div class="icon">
																				<i class="fas fa-parking"></i>
																			</div>
																			<h6>Parking</h6>
																		</div>
																	</div> 
																	<div class="col-sm-4">
																		<div class="choice" data-toggle="wizard-checkbox">
																			<input type="checkbox" name="service[]" value="AcessCard">
																			<div class="icon">
																				<i class="far fa-address-card"></i>
																			</div>
																			<h6>Access Card</h6>
																		</div>
																	</div>
                                                                    <div class="col-sm-4">
																		<div class="choice" data-toggle="wizard-checkbox">
																			<input type="checkbox" name="service[]" value="SwimmingPool">
																			<div class="icon">
																				<i class="fas fa-swimmer"></i>
																			</div>
																			<h6>Swimming Pool</h6>
																		</div>
																	</div>
                                                                <div class="col-sm-4">
																		<div class="choice" data-toggle="wizard-checkbox">
																			<input type="checkbox" name="service[]" value="Lift">
																			<div class="icon">
																				<i class="fas fa-angle-double-up"></i>
																			</div>
																			<h6>Lift</h6>
																		</div>
																	</div>
                                                                <div class="col-sm-4">
																		<div class="choice" data-toggle="wizard-checkbox">
																			<input type="checkbox" name="service[]" value="PlayGround">
																			<div class="icon">
																				<i class="fas fa-volleyball-ball"></i>
																			</div>
																			<h6>Playground</h6>
																		</div>
																	</div>
															</div>
														</div>
														<div class="tab-pane" id="picture">
															<div class="row">
																<div class="col-sm-12">
																	<h4 class="info-text"> Are you living in a nice area? </h4>
																</div>
																<div class="col-sm-8 ">
																	<div class="form-group label-floating">
																		<label class="control-label">Decription</label>
																		<input type="text" name="description" id="description" class="form-control">
																	</div>
																</div>
																<div class="col-sm-4">
																	<div class="form-group label-floating">
																		<label class="control-label">Take Picture</label>
                                                                        
                                         
                                                                        <input type="file" name="image" id="image" accept="image/gif, image/jpeg, image/png, image/jpg" class="form-control input-sm">
																		<br>
																	</div>
																</div>						
															</div>
														</div>													
													<div class="wizard-footer">
														<div class="pull-right">
															<input type='button' class='btn btn-next btn-fill btn-primary btn-wd m-0' name='next' value='Next' />
															<input type='button' class='btn btn-finish btn-fill btn-success btn-wd m-0' name='finish' value='Finish' />
														</div>

														<div class="pull-left">
															<input type='button' class='btn btn-previous btn-fill btn-default btn-wd m-0' name='previous' value='Previous' />
														</div>
														<div class="clearfix"></div>
													</div>
												</form>
                                                
                                                
											</div>
										</div> <!-- wizard container -->
									</div>
								</div>
							</div>
						</div>
					</div>
                </div>

    
<?php
    
    include('footer.php');
?>