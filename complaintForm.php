<!doctype html>
<html lang="en" dir="ltr">
<?php
    
    include('header.php');
?>
        <?php
            // Connection database
            //include("connection.php");
            // Check, for session 'user_session'
            include("session.php");

            // Set Default Time Zone for Asia/Kuala_Lumpur
            date_default_timezone_set("Asia/Kuala_Lumpur");

            // Check, if username session is NOT set then this page will jump to login page
            if (!isset($_SESSION['session']) && !isset($_SESSION['job'])) {
                header('Location: login.php');
                session_destroy();
            }

            $job = $_SESSION["job"];
            $now = date("Y-m-d");
        ?>
				<div class="my-3 my-md-5 app-content">
					<div class="side-app">
						<div class="page-header">
							<ol class="breadcrumb1">
											<li class="breadcrumb-item1 active">Manage Services</li>
											<li class="breadcrumb-item1 active">Complaint Form </li>
							</ol>
							
						</div>
                        <?php
                            if(isset($_SESSION["session"]))
                            {
                                $email = $_SESSION["session"];
                                $sql = "SELECT * FROM user WHERE email = :email";
                                $stmt = $conn->prepare($sql);
                                $stmt->bindParam(":email", $email);
                                $stmt->execute();

                                if($dt = $stmt->fetch(PDO::FETCH_ASSOC))
                                {
                                   $user_id = $dt["user_id"];
                                    $name = $dt["name"];
                                    $email = $dt["email"];
                                    $password = $dt["password"];
                                    $phone = $dt["phone"];
                                    $NoHouse = $dt["NoHouse"];
                                }
                            }
                            else
                            {
                                echo "Data is not found!";
                            }
                            ?>
							<div class="col-lg-12">
								<form class="card" method="POST" action="complaintData.php"  enctype="multipart/form-data">
									<div class="card-header">
										<h3 class="card-title">Complaint Form</h3>
									</div>
									<div class="card-body">
										<div class="row">
                                            <div class="col-sm-6 col-md-12">
												<div class="form-group">
													<label class="form-label">Full Name</label>
													<input type="text" class="form-control" name="name" id="name"  value="<?php  echo $name; ?>"readonly >
													<input type="text" class="form-control" name="user_id" id="user_id"  value="<?php  echo $user_id; ?>" hidden>
												</div>
											</div>
											<div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">NRIC / Passport</label>
													<input type="text" class="form-control" name="nric" id="nric" value="<?php  echo $password; ?>" readonly>
                                                    
												</div>
											</div>
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Contact Number</label>
													<input type="text" class="form-control" name="phone" id="phone" value="<?php  echo $phone; ?>" readonly>
												</div>
											</div>
                                             <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Email</label>
													<input type="text" class="form-control" name="email" id="email" value="<?php  echo $email; ?>" readonly>
												</div>
											</div>
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">No. Unit House</label>
													<input type="text" class="form-control" name="NoHouse" id="NoHouse" value="<?php  echo $NoHouse; ?>" readonly>
                                   
												</div>
											</div>
                                             <div class="col-sm-6 col-md-12">
												<div class="form-group">
													<label class="form-label">Complaint Type</label>
													<select class="form-control select2 custom-select" data-placeholder="Choose one" name="type" id="type">
                                                            <option label="Choose one"></option>
                                                            <option value="Mosque">Mosque</option>
                                                            <option value="Parking">Parking</option>
                                                            <option value="Access Card">Access Card</option>
                                                            <option value="Swimming Pool">Swimming Pool</option>
                                                            <option value="Lift">Lift</option>
                                                            <option value="Playground">Playground</option>                                                          
                                                     </select>
												</div>
											</div>
                                           
                                            
                                            <div class="col-sm-6 col-md-12">
												<div class="form-group">
													<label class="form-label">Description</label>
													<input type="text" class="form-control" name="description" id="description" >
												</div>
											</div> 
																
										</div>
									</div>
									<div class="card-footer text-right">
										<button type="submit" class="btn btn-primary" >Create Complaint</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					
<?php
    
    include('footer.php');
?>

