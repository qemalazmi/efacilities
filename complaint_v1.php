<?php
    include('header.php');
    include("session.php");



?>
                <div class="app-content my-3 my-md-5">
					<div class="side-app">
						<div class="page-header">
							
							<ol class="breadcrumb1">
											<li class="breadcrumb-item1 active">Manage Services</li>
											<li class="breadcrumb-item1 active">Complaint</li>
							</ol>
                                    </div>
                        
        

                       <!-- Message Modal -->
                        
                         <!-- Message Modal Edit -->
                        
                        <!-- Message Modal view -->
                       
                <div class="row">
				<div class="col-md-12 col-lg-12">
                        <div class="card">
                            <div class="card-footer text-right">

                                                          <button type="submit" class="btn btn-primary" onclick="window.location.href='complaintForm.php'" >Create Complaint</button>

                                        </div>
									<div class="card-status bg-azure br-tr-3 br-tl-3"></div>
									<div class="card-header">
										<h3 class="card-title">Record Complaint</h3>
                                        
									</div>
									<div class="card-body">
										<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
											<div class="panel panel-default active">
												<div class="panel-heading " role="tab" id="headingOne">
													<h4 class="panel-title">
														<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true"  aria-controls="collapseOne">

															List Complaint
														</a>
													</h4>
                                                    
												</div>
												<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
													<div class="panel-body">
                                                        
                                                       
														
                                            <div class="card-body">
                                                <div class="table-responsive">
                                                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                        <thead>
                                                            <tr>
                                                                <th class="wd-20p">Name</th>
                                                                <th class="wd-20p">Type Complaint</th>
                                                                <th class="wd-20p">Date Complaint</th>
                                                                <th class="wd-20p">Description Complaint</th>
                                                                <th class="wd-20p">Status</th>
                                                                <th class="wd-20p">Action</th>

                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                             <?php
                                                             $counter = 1;
                                                             $email = $_SESSION["session"];
                                                               if($job == "Admin"){

                                                                        $statement = $conn->prepare("SELECT * FROM complaint ");

                                                               }else {
                                                                    $statement = $conn->prepare("SELECT * FROM complaint");

                                                               }
                                                                        $statement->execute();

                                                                       while($data = $statement->fetch(PDO::FETCH_ASSOC))
                                                                       {

                                                                       ?>
                                                                            <tr>
                                                                              <td><?php echo $data["user_id"]; ?></td>
                                                                              <td><?php echo $data["type"];?></td>
                                                                              <td><?php echo $data["complaintDate"];?></td>
                                                                              <td><?php echo $data["description"];?></td>
                                                                              <td><?php echo $data["status"];?></td>
                                                                              <td style="text-align: center;vertical-align: middle;">
                                                                            <a href="complaint_edit.php?id=<?php echo $data["complaint_ID"]; ?>">
                                                                              <button type="button" name="update" id="update" class="btn btn-primary btn-sm">
                                                                                  <i class="fa fa-pencil"></i>&nbsp;&nbsp;View
                                                                              </button>

                                                                            </a>
                                                                        </td>
                                                                                 
                                                                            </tr>
                                                                       <?php
                                                                           $counter++;
                                                                       }
                                                                       ?>

                                                        </tbody>
											</table>
										</div>
									</div>
													</div>
												</div>
											</div>
											
											
										</div><!-- panel-group -->
									</div>
								</div>
                    </div>
</div>

									<!-- table-wrapper -->
								</div>
								<!-- section-wrapper -->

							</div>
						</div>
					</div>
                    
                    <?php
    
                        include('footer.php');
                    ?>
