<!doctype html>
<html lang="en" dir="ltr">
<?php
    include('header.php');
    include("session.php");

            // Set Default Time Zone for Asia/Kuala_Lumpur
            date_default_timezone_set("Asia/Kuala_Lumpur");

            // Check, if username session is NOT set then this page will jump to login page
            if (!isset($_SESSION['session']) && !isset($_SESSION['job'])) {
                header('Location: login.php');
                session_destroy();
            }
?>
				<div class="my-3 my-md-5 app-content">
					<div class="side-app">
						<div class="page-header">
							<ol class="breadcrumb1">
											<li class="breadcrumb-item1 active">User Management</li>
											<li class="breadcrumb-item1 active">Update User </li>
							</ol>
							
						</div>
            
                        <?php
			 //--------------code get data ----------------
                if(isset($_GET["user_id"]))
                {
                    $user_id = $_GET["user_id"];                    
                    $sql = 
                        "SELECT * FROM user where user_id = :user_id" ; 
                         
                    $stmt = $conn->prepare($sql);
                    $stmt->bindParam(":user_id", $user_id);
                    $stmt->execute();
                    
                    if($dt = $stmt->fetch(PDO::FETCH_ASSOC))
                    {
                        $user_id = $dt["user_id"];
                        $name = $dt["name"];						
                        $nric = $dt["nric"];
                        $password = $dt["password"];
                        $phone = $dt["phone"];
                        $position = $dt["position"];
                        $address = $dt["address"];
                        $status = $dt["status"];
                        $gender = $dt["gender"];
                        $state = $dt["state"];
                        $region = $dt["region"];
						$NoHouse = $dt["NoHouse"];
						$descendant = $dt["descendant"];
						$postal = $dt["postal"];
						$city = $dt["city"];
						$country = $dt["country"];
						$email = $dt["email"];
                       
                    }
                }
                else
                {
                    echo "Data is not found!";
                }
                ?> 
							<div class="col-lg-12">
								<form class="card" method="POST" action="registerUser_edit.php"  enctype="multipart/form-data">
									<div class="card-header">
										<h3 class="card-title">Update User</h3>
									</div>
									<div class="card-body">
										<div class="row">
                                            <div class="col-sm-6 col-md-12">
												<div class="form-group">
													<label class="form-label">Full Name</label>
													<input type="text" class="form-control" name="name" id="name" value="<?php echo $name; ?>" >
												</div>
											</div>
											<div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">NRIC / Passport</label>
													<input type="text" class="form-control" name="nric" id="nric" value="<?php echo $nric; ?>" >
                                                    <input type="text" class="form-control" name="password" id="password" value="<?php echo $password; ?>" hidden>
                                                    <input type="text" class="form-control" name="user_id" id="user_id" value="<?php echo $user_id; ?>" hidden>
												</div>
											</div>
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Contact Number</label>
													<input type="text" class="form-control" name="phone" id="phone" value="<?php echo $phone; ?>">
												</div>
											</div>
                                             <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Email</label>
													<input type="text" class="form-control" name="email" id="email" value="<?php echo $email; ?>" >
												</div>
											</div>
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">No. Unit House</label>
													<input type="text" class="form-control" name="NoHouse" id="NoHouse" value="<?php echo $NoHouse; ?>" >
                                   
												</div>
											</div>
                                            <?php
                                          if($job == "Admin")
                                          {
                                            ?>
                                               <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Position</label>
													<select class="form-control select2 custom-select" data-placeholder="Choose one" name="position" id="position">
                                                             <option value="" <?php if($position == "NULL"){ echo "selected"; }else{} ?>>-- Choose --</option>
                                                           <option value="Admin" <?php if($position == "Admin"){ echo "selected"; }else{} ?>>Admin</option>
                                                            <option value="Employee" <?php if($position == "Employee"){ echo "selected"; }else{} ?>>Employee</option>
                                                            <option value="Resident" <?php if($position == "Resident"){ echo "selected"; }else{} ?>>Resident</option>
                                                     </select>
                                                    
												</div>
											</div>
                                            <?php   
                                          }
                                          
                                          else
                                          {
                                              ?>
                                                
                                                <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Position</label>
													<input type="text" class="form-control" name="NoHouse" id="NoHouse" value="<?php echo $position; ?>" readonly>
                                                    
												</div>
											</div>
                                              <?php
                                          }
                                            ?>
                                            
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Status</label>
													<select class="form-control select2 custom-select" data-placeholder="Choose one" name="status" id="status">
                                                            <option value="" <?php if($status == "NULL"){ echo "selected"; }else{} ?>>-- Choose --</option>
                                                           <option value="Single" <?php if($status == "Single"){ echo "selected"; }else{} ?>>Single</option>
                                                            <option value="Marry" <?php if($status == "Marry"){ echo "selected"; }else{} ?>>Marry</option>
                                                            
                                                     </select>
												</div>
											</div>
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Gender</label>
													<select class="form-control select2 custom-select" data-placeholder="Choose one" name="gender" id="gender">                                                           
                                                            <option value="" <?php if($gender == "NULL"){ echo "selected"; }else{} ?>>-- Choose --</option>
                                                           <option value="Male" <?php if($gender == "Male"){ echo "selected"; }else{} ?>>Male</option>
                                                            <option value="Female" <?php if($gender == "Female"){ echo "selected"; }else{} ?>>Female</option>
                                                     </select>
												</div>
											</div>
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">State</label>
													<select class="form-control select2 custom-select" data-placeholder="Choose one" name="state" id="state">
                                                        
                                                            <option value="" <?php if($state == "NULL"){ echo "selected"; }else{} ?>>-- Choose --</option>
                                                           <option value="Johor" <?php if($state == "Johor"){ echo "selected"; }else{} ?>>Johor</option>
                                                            <option value="Kedah" <?php if($state == "Kedah"){ echo "selected"; }else{} ?>>Kedah</option>
                                                            <option value="Kelantan" <?php if($state == "Kelantan"){ echo "selected"; }else{} ?>>Kelantan</option>
                                                            <option value="Perak" <?php if($state == "Perak"){ echo "selected"; }else{} ?>>Perak</option>
                                                            <option value="Selangor" <?php if($state == "Selangor"){ echo "selected"; }else{} ?>>Selangor</option>
                                                            <option value="Wilayah Persekutuan" <?php if($state == "Wilayah Persekutuan"){ echo "selected"; }else{} ?>>Wilayah Persekutuan</option>
                                                            <option value="Melaka" <?php if($state == "Melaka"){ echo "selected"; }else{} ?>>Melaka</option>
                                                            <option value="Negeri Sembilan" <?php if($state == "Negeri Sembilan"){ echo "selected"; }else{} ?>>Negeri Sembilan</option>
                                                            <option value="Pahang" <?php if($state == "Pahang"){ echo "selected"; }else{} ?>>Pahang</option>
                                                            <option value="Perlis" <?php if($state == "Perlis"){ echo "selected"; }else{} ?>>Perlis</option>
                                                            <option value="Pulau Penang" <?php if($state == "Pulau Penang"){ echo "selected"; }else{} ?>>Pulau Penang</option>
                                                            <option value="Sabah" <?php if($state == "Sabah"){ echo "selected"; }else{} ?>>Sabah</option>
                                                            <option value="Serawak" <?php if($state == "Serawak"){ echo "selected"; }else{} ?>>Serawak</option>
                                                            <option value="Terengganu" <?php if($state == "Terengganu"){ echo "selected"; }else{} ?>>Terengganu</option>
                                                        
                                                        
                                                     </select>
												</div>
											</div>
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Region</label>
													<select class="form-control select2 custom-select" data-placeholder="Choose one" name="region" id="region">
                                                            <option value="" <?php if($region == "NULL"){ echo "selected"; }else{} ?>>-- Choose --</option>
                                                           <option value="Islam" <?php if($region == "Islam"){ echo "selected"; }else{} ?>>Islam</option>
                                                            <option value="Hindu" <?php if($region == "Hindu"){ echo "selected"; }else{} ?>>Hindu</option>
                                                            <option value="Buddha" <?php if($region == "Buddha"){ echo "selected"; }else{} ?>>Buddha</option>
                                                            <option value="Kristian" <?php if($region == "Kristian"){ echo "selected"; }else{} ?>>Kristian</option>
                                                     </select>
												</div>
											</div>
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Descendant</label>
													<select class="form-control select2 custom-select" data-placeholder="Choose one" name="descendant" id="descendant">
                                                            <option value="" <?php if($descendant == "NULL"){ echo "selected"; }else{} ?>>-- Choose --</option>
                                                           <option value="Melayu" <?php if($descendant == "Melayu"){ echo "selected"; }else{} ?>>Melayu</option>
                                                            <option value="India" <?php if($descendant == "India"){ echo "selected"; }else{} ?>>India</option>
                                                            <option value="Cina" <?php if($descendant == "Cina"){ echo "selected"; }else{} ?>>Cina</option>
                                                            <option value="Other" <?php if($descendant == "Other"){ echo "selected"; }else{} ?>>Other</option>
                                                     </select>
												</div>
											</div> 
                                            <div class="col-sm-6 col-md-12">
												<div class="form-group">
													<label class="form-label">Address</label>
													<textarea class="form-control" name="address" rows="6" placeholder=""><?php echo $address; ?></textarea>
												</div>
											</div> 
											<div class="col-md-5">
												<div class="form-group">
													<label class="form-label">Postal Code</label>
													<input type="text" class="form-control"  placeholder="e.g : 86400" name="postal" id="postal"    value="<?php echo $postal; ?>" >
												</div>
											</div>
											<div class="col-sm-6 col-md-3">
												<div class="form-group">
													<label class="form-label">City</label>
													<input type="text" class="form-control" placeholder="e.g : Parit Raja" name="city" id="city"  value="<?php echo $city; ?>" >
												</div>
											</div>
											<div class="col-sm-6 col-md-4">
												<div class="form-group">
													<label class="form-label">Country</label>
													<input type="text" class="form-control" placeholder="" name="country" id="country"  value="<?php echo $country; ?>" >
												</div>
											</div>						
										</div>
									</div>
									<div class="card-footer text-right">
                                        <button type="submit" class="btn btn-secondary" >Cancel</button>
										<button type="submit" class="btn btn-primary" >Update User</button>
                                        
									</div>
                                    
								</form>
							</div>
						</div>
					</div>
					
<?php
    
    include('footer.php');
?>

