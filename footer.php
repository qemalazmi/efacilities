<!--footer-->
<footer class="footer">
    <div class="container">
        <div class="row align-items-center flex-row-reverse">
            <div class="col-lg-12 col-sm-12 mt-3 mt-lg-0 text-center">
                Copyright © 2020 <a href="#">Ren</a>. Designed by <a href="#">EKMAL</a> All rights reserved.
            </div>
        </div>
    </div>
</footer>
<!-- End Footer-->
</div>
</div>
</div>

<!-- Back to top -->
<a href="#top" id="back-to-top" style="display: inline;"><i class="fas fa-angle-up"></i></a>
<!-- Dashboard Css -->

<script src="assets/js/vendors/jquery-3.2.1.min.js"></script>
<script src="assets/js/vendors/bootstrap.bundle.min.js"></script>
<script src="assets/js/vendors/jquery.sparkline.min.js"></script>
<script src="assets/js/vendors/selectize.min.js"></script>
<script src="assets/js/vendors/jquery.tablesorter.min.js"></script>
<script src="assets/js/vendors/circle-progress.min.js"></script>
<script src="assets/plugins/rating/jquery.rating-stars.js"></script>
<script src='assets/plugins/fullcalendar/moment.min.js'></script>
<script src='assets/plugins/fullcalendar/fullcalendar.min.js'></script>
<script src="assets/js/vendors/selectize.min.js"></script>

<!-- Side menu js -->
<script src="assets/plugins/toggle-sidebar/js/sidemenu.js"></script>

<!-- Custom scroll bar Js-->
<script src="assets/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js"></script>

<!--Select2 js -->
<script src="assets/plugins/select2/select2.full.min.js"></script>

<!-- Timepicker js -->
<script src="assets/plugins/time-picker/jquery.timepicker.js"></script>
<script src="assets/plugins/time-picker/toggles.min.js"></script>

<!-- Datepicker js -->
<script src="assets/plugins/date-picker/spectrum.js"></script>
<script src="assets/plugins/date-picker/jquery-ui.js"></script>
<script src="assets/plugins/input-mask/jquery.maskedinput.js"></script>

<!-- 3Dlines-animation -->
<script src="assets/plugins/3Dlines-animation/three.min.js"></script>
<script src="assets/plugins/3Dlines-animation/projector.js"></script>
<script src="assets/plugins/3Dlines-animation/canvas-renderer.js"></script>
<script src="assets/plugins/3Dlines-animation/3d-lines-animation.js"></script>
<script src="assets/plugins/3Dlines-animation/color.js"></script>

<!-- Inline js -->
<script src="assets/js/select2.js"></script>

<!--Counters -->
<script src="assets/plugins/counters/counterup.min.js"></script>
<script src="assets/plugins/counters/waypoints.min.js"></script>

<!-- Custom js -->
<script src="assets/js/fullcalendar.js"></script>
<script src="assets/js/custom.js"></script>

<!-- Data tables -->
<script src="assets/plugins/datatable/jquery.dataTables.min.js"></script>
<script src="assets/plugins/datatable/dataTables.bootstrap4.min.js"></script>

<!-- forn-wizard js-->
<script src="assets/plugins/forn-wizard/js/material-bootstrap-wizard.js"></script>
<script src="assets/plugins/forn-wizard/js/jquery.validate.min.js"></script>
<script src="assets/plugins/forn-wizard/js/jquery.bootstrap.js"></script>

<!-- Data table js -->
<script>
    $(function(e) {
        $('#example').DataTable();
    });
</script>
<script>
    $(function(e) {
        $('#example1').DataTable();
    });
</script>

<script>
    $('#EditParking').on('show.bs.modal', function(e) {
        var parking_id = $(e.relatedTarget).data('parking_id');
        var level = $(e.relatedTarget).data('level');
        var noparking = $(e.relatedTarget).data('noparking');
        var type = $(e.relatedTarget).data('type');

        //sent data to modal view
        $(e.currentTarget).find('input[name="parking_id"]').val(parking_id);
        $(e.currentTarget).find('input[name="level"]').val(level);
        $(e.currentTarget).find('input[name="noparking"]').val(noparking);
        $(e.currentTarget).find('input[name="type"]').val(type);

    });
</script>
<script>
    $('#viewParking').on('show.bs.modal', function(e) {
        var parking_id = $(e.relatedTarget).data('parking_id');
        var level = $(e.relatedTarget).data('level');
        var noparking = $(e.relatedTarget).data('noparking');
        var type = $(e.relatedTarget).data('type');

        //sent data to modal view
        $(e.currentTarget).find('input[name="parking_id"]').val(parking_id);
        $(e.currentTarget).find('input[name="level"]').val(level);
        $(e.currentTarget).find('input[name="noparking"]').val(noparking);
        $(e.currentTarget).find('input[name="type"]').val(type);
    });
</script>
<script>
    $('#EditPlayground').on('show.bs.modal', function(e) {
        var playground_id = $(e.relatedTarget).data('playground_id');
        var NamePark = $(e.relatedTarget).data('park');
        var quantity = $(e.relatedTarget).data('quantity');
        var status = $(e.relatedTarget).data('status');

        //sent data to modal view
        $(e.currentTarget).find('input[name="playground_id"]').val(playground_id);
        $(e.currentTarget).find('input[name="park"]').val(NamePark);
        $(e.currentTarget).find('input[name="quantity"]').val(quantity);
        $(e.currentTarget).find('input[name="status"]').val(status);

    });
</script>
<script>
    $('#ViewPlayground').on('show.bs.modal', function(e) {
        var playground_id = $(e.relatedTarget).data('playground_id');
        var NamePark = $(e.relatedTarget).data('park');
        var quantity = $(e.relatedTarget).data('quantity');
        var status = $(e.relatedTarget).data('status');

        //sent data to modal view
        $(e.currentTarget).find('input[name="playground_id"]').val(playground_id);
        $(e.currentTarget).find('input[name="park"]').val(NamePark);
        $(e.currentTarget).find('input[name="quantity"]').val(quantity);
        $(e.currentTarget).find('input[name="status"]').val(status);

    });
</script>
<script>
    $('#EditPool').on('show.bs.modal', function(e) {
        var pool_id = $(e.relatedTarget).data('pool_id');
        var type = $(e.relatedTarget).data('type');
        var quantity = $(e.relatedTarget).data('quantity');
        var area = $(e.relatedTarget).data('area');
        var volume = $(e.relatedTarget).data('volume');
        var status = $(e.relatedTarget).data('status');

        //sent data to modal view
        $(e.currentTarget).find('input[name="pool_id"]').val(pool_id);
        $(e.currentTarget).find('input[name="type"]').val(type);
        $(e.currentTarget).find('input[name="quantity"]').val(quantity);
        $(e.currentTarget).find('input[name="area"]').val(area);
        $(e.currentTarget).find('input[name="volume"]').val(volume);
        $(e.currentTarget).find('input[name="status"]').val(status);

    });
</script>
<script>
    $('#ViewPool').on('show.bs.modal', function(e) {
        var pool_id = $(e.relatedTarget).data('pool_id');
        var type = $(e.relatedTarget).data('type');
        var quantity = $(e.relatedTarget).data('quantity');
        var area = $(e.relatedTarget).data('area');
        var volume = $(e.relatedTarget).data('volume');
        var status = $(e.relatedTarget).data('status');

        //sent data to modal view
        $(e.currentTarget).find('input[name="pool_id"]').val(pool_id);
        $(e.currentTarget).find('input[name="type"]').val(type);
        $(e.currentTarget).find('input[name="quantity"]').val(quantity);
        $(e.currentTarget).find('input[name="area"]').val(area);
        $(e.currentTarget).find('input[name="volume"]').val(volume);
        $(e.currentTarget).find('input[name="status"]').val(status);

    });
</script>

<script>
    $('#EditBulding').on('show.bs.modal', function(e) {
        var house_ID = $(e.relatedTarget).data('house_ID');
        var NoHouse = $(e.relatedTarget).data('NoHouse');
        var level = $(e.relatedTarget).data('level');

        //sent data to modal view
        $(e.currentTarget).find('input[name="house_ID"]').val(house_ID);
        $(e.currentTarget).find('input[name="NoHouse"]').val(NoHouse);
        $(e.currentTarget).find('input[name="level"]').val(level);

    });
</script>
<script>
    $('#ViewBulding').on('show.bs.modal', function(e) {
        var house_ID = $(e.relatedTarget).data('house_ID');
        var NoHouse = $(e.relatedTarget).data('NoHouse');
        var level = $(e.relatedTarget).data('level');

        //sent data to modal view
        $(e.currentTarget).find('input[name="house_ID"]').val(house_ID);
        $(e.currentTarget).find('input[name="NoHouse"]').val(NoHouse);
        $(e.currentTarget).find('input[name="level"]').val(level);

    });
</script>


<script>
    $('#EditAccessCard').on('show.bs.modal', function(e) {
        var access_id = $(e.relatedTarget).data('access_id');
        var unit_AccessCard = $(e.relatedTarget).data('unit');
        var status = $(e.relatedTarget).data('status');
        //sent data to modal view
        $(e.currentTarget).find('input[name="access_id"]').val(access_id);
        $(e.currentTarget).find('input[name="unit"]').val(unit_AccessCard);
        $(e.currentTarget).find('input[name="status"]').val(status);


    });
</script>
<script>
    $('#ViewAccessCard').on('show.bs.modal', function(e) {
        var access_id = $(e.relatedTarget).data('access_id');
        var unit_AccessCard = $(e.relatedTarget).data('unit');
        var status = $(e.relatedTarget).data('status');
        //sent data to modal view
        $(e.currentTarget).find('input[name="access_id"]').val(access_id);
        $(e.currentTarget).find('input[name="unit"]').val(unit_AccessCard);
        $(e.currentTarget).find('input[name="status"]').val(status);


    });
</script>
<script>
    $('#ViewHall').on('show.bs.modal', function(e) {
        var hall_id = $(e.relatedTarget).data('hall_id');
        var NameHall = $(e.relatedTarget).data('name');
        var area = $(e.relatedTarget).data('area');
        var avenue = $(e.relatedTarget).data('avenue');
        var status = $(e.relatedTarget).data('status');
        //sent data to modal view
        $(e.currentTarget).find('input[name="hall_id"]').val(hall_id);
        $(e.currentTarget).find('input[name="name"]').val(NameHall);
        $(e.currentTarget).find('input[name="area"]').val(area);
        $(e.currentTarget).find('input[name="avenue"]').val(avenue);
        $(e.currentTarget).find('input[name="status"]').val(status);


    });
</script>
<script>
    $('#EditHall').on('show.bs.modal', function(e) {
        var hall_id = $(e.relatedTarget).data('hall_id');
        var NameHall = $(e.relatedTarget).data('name');
        var area = $(e.relatedTarget).data('area');
        var avenue = $(e.relatedTarget).data('avenue');
        var status = $(e.relatedTarget).data('status');
        //sent data to modal view
        $(e.currentTarget).find('input[name="hall_id"]').val(hall_id);
        $(e.currentTarget).find('input[name="name"]').val(NameHall);
        $(e.currentTarget).find('input[name="area"]').val(area);
        $(e.currentTarget).find('input[name="avenue"]').val(avenue);
        $(e.currentTarget).find('input[name="status"]').val(status);

    });
</script>

<script>
    $('#viewResident').on('show.bs.modal', function(e) {
        var resident_id = $(e.relatedTarget).data('resident_id');
        var name = $(e.relatedTarget).data('name');
        var nric = $(e.relatedTarget).data('nric');
        var phone = $(e.relatedTarget).data('phone');
        var address = $(e.relatedTarget).data('address');
        var status = $(e.relatedTarget).data('status');
        var unit_house = $(e.relatedTarget).data('unit_house');
        //sent data to modal view
        $(e.currentTarget).find('input[name="resident_id"]').val(resident_id);
        $(e.currentTarget).find('input[name="name"]').val(name);
        $(e.currentTarget).find('input[name="nric"]').val(nric);
        $(e.currentTarget).find('input[name="phone"]').val(phone);
        $(e.currentTarget).find('input[name="address"]').val(address);
        $(e.currentTarget).find('input[name="status"]').val(status);
        $(e.currentTarget).find('input[name="unit_house"]').val(unit_house);


    });
</script>
<script>
    $('#viewEmployee').on('show.bs.modal', function(e) {
        var employee_id = $(e.relatedTarget).data('employee_id');
        var name = $(e.relatedTarget).data('name');
        var nric = $(e.relatedTarget).data('nric');
        var phone = $(e.relatedTarget).data('phone');
        var email = $(e.relatedTarget).data('email');
        var position = $(e.relatedTarget).data('position');
        var status = $(e.relatedTarget).data('status');
        var gender = $(e.relatedTarget).data('gender');
        var state = $(e.relatedTarget).data('state');
        var region = $(e.relatedTarget).data('region');
        var descendant = $(e.relatedTarget).data('descendant');
        var address = $(e.relatedTarget).data('address');
        var postal = $(e.relatedTarget).data('postal');
        var city = $(e.relatedTarget).data('city');
        var country = $(e.relatedTarget).data('country');
        //sent data to modal view
        $(e.currentTarget).find('input[name="employee_id"]').val(employee_id);
        $(e.currentTarget).find('input[name="name"]').val(name);
        $(e.currentTarget).find('input[name="nric"]').val(nric);
        $(e.currentTarget).find('input[name="phone"]').val(phone);
        $(e.currentTarget).find('input[name="email"]').val(email);
        $(e.currentTarget).find('input[name="position"]').val(position);
        $(e.currentTarget).find('input[name="status"]').val(status);
        $(e.currentTarget).find('input[name="gender"]').val(gender);
        $(e.currentTarget).find('input[name="state"]').val(state);
        $(e.currentTarget).find('input[name="region"]').val(region);
        $(e.currentTarget).find('input[name="descendant"]').val(descendant);
        $(e.currentTarget).find('input[name="address"]').val(address);
        $(e.currentTarget).find('input[name="postal"]').val(postal);
        $(e.currentTarget).find('input[name="city"]').val(city);
        $(e.currentTarget).find('input[name="country"]').val(country);


    });
</script>
<!-- FullCalendar -->
<script>
    $(window).load(function() {
        var date = new Date(),
            d = date.getDate(),
            m = date.getMonth(),
            y = date.getFullYear(),
            started,
            categoryClass;

        var calendar = $('#calendar1').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            selectable: true,
            selectHelper: true,
            select: function(start, end, allDay) {
                //$('#fc_create').click();
                $('#ModalAdd #start_date').val(moment(start).format("YYYY-MM-DD"));
                $('#ModalAdd #start_time').val(moment(start).format("hh:MM A"));
                $('#ModalAdd #end_date').val(moment(end).format("YYYY-MM-DD"));
                $('#ModalAdd #end_time').val(moment(end).format("hh:MM A"));
                $('#ModalAdd').modal('show');

                started = start;
                ended = end;

                $(".antosubmit").on("click", function() {
                    var title = $("#title").val();
                    if (end) {
                        ended = end;
                    }

                    categoryClass = $("#event_type").val();

                    if (title) {
                        calendar.fullCalendar('renderEvent', {
                                title: title,
                                start: started,
                                end: end,
                                allDay: allDay
                            },
                            true // make the event "stick"
                        );
                    }

                    $('#title').val('');

                    calendar.fullCalendar('unselect');

                    $('.antoclose').click();

                    return false;
                });
            },
            eventClick: function(calEvent, jsEvent, view) {
                //$('#fc_edit').click();
                //$('#title2').val(calEvent.title);
                $('#ModalEdit #title').val(calEvent.end);
                $('#ModalEdit').modal('show');

                categoryClass = $("#event_type").val();

                $(".antosubmit2").on("click", function() {
                    calEvent.title = $("#title2").val();

                    calendar.fullCalendar('updateEvent', calEvent);
                    $('.antoclose2').click();
                });

                calendar.fullCalendar('unselect');
            },
            editable: true,
            events: [
                <?php
                $searchData = "SELECT * FROM event_info WHERE status_event = 'Approve'";
                $statement = $conn->prepare($searchData);
                $statement->execute();
                $events = $statement->fetchAll();

                foreach ($events as $data) :
                ?> {
                        title: '<?php echo $data["title"]; ?>',
                        event_id: '<?php echo $data["event_id"]; ?>',
                        start: '<?php echo $data["start_date"]; ?>',
                        end: '<?php echo $data["end_date"]; ?>',
                    },
                <?php
                endforeach;
                ?>
            ]
        });
    });
</script>
<!-- /FullCalendar -->



<script>
    $(document).ready(function() {
        var i = 1;
        $('#add').click(function() {
            i++;
            $('#dynamic_field').append('<tr id="row' + i + '"><td><input type="text" name="name[]" placeholder="Enter your Name" class="form-control name_list" /></td><td><input type="text" name="nric_penyewa" placeholder="Enter your NRIC" class="form-control name_list" /></td><td><input type="text" name="relay[]" placeholder="Enter your relation" class="form-control name_list" /></td><td><button type="button" name="remove" id="' + i + '" class="btn btn-danger btn_remove">X</button></td></tr>');

        });
        $(document).on('click', '.btn_remove', function() {
            var button_id = $(this).attr("id");
            $('#row' + button_id + '').remove();
        });
        $('#submit').click(function() {
            $.ajax({
                url: "residentadd.php",
                method: "POST",
                data: $('#add_name').serialize(),
                success: function(data) {
                    alert(data);
                    $('#add_name')[0].reset();
                }
            });
        });
    });
</script>

</body>

<!-- Mirrored from spruko.com/demo/ren/Blue-animated/LeftMenu/form-elements.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 20 Sep 2018 05:54:43 GMT -->

</html>