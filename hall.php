<?php
    include('header.php');
    include("session.php");



?>
                <div class="app-content my-3 my-md-5">
					<div class="side-app">
						<div class="page-header">
							
							<ol class="breadcrumb1">
											<li class="breadcrumb-item1 active">Fix Assets Management</li>
											<li class="breadcrumb-item1 active">Hall</li>
							</ol>
                                    </div>

                       <!-- Message Modal -->
                        <div class="modal fade" id="AddHall" tabindex="-1" role="dialog"  aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form id="modal" class="form-horizontal" method="POST" action="inserthall.php">
                                   <div class="modal-header">
                                        <h5 class="modal-title" id="example-Modal3">Add Hall</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                            <div class="form-group">
                                                <label for="recipient-name" class="form-control-label">Name Hall:</label>
                                                <input type="text" class="form-control" name="NameHall" id="NameHall">
                                            </div>
                                            <div class="form-group">
                                                <label for="recipient-name" class="form-control-label">Area (m²)</label>
                                                <input type="text" class="form-control" name="area" id="area">
                                            </div>
                                            <div class="form-group">
                                                <label for="recipient-name" class="form-control-label">avenue</label>
                                                <input type="text" class="form-control" name="avenue" id="avenue">
                                            </div>
                                            <div class="form-group">
                                                <label for="recipient-name" class="form-control-label">Status:</label>
                                               <select class="form-control select2 custom-select" data-placeholder="Choose one" name="status" id="status">
                                                            <option label="Choose one"></option>
                                                            <option value="Available">Available</option>
                                                            <option value="Not Available">Not Available</option>
                                                </select>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Insert Hall</button>
                                            </div>
                                        
                                    </div>
                                  </form>
                                </div>
                            </div>
                        </div>
                         <!-- Message Modal Edit -->
                        <div class="modal fade" id="EditHall" tabindex="-1" role="dialog"  aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form id="modal" class="form-horizontal" method="POST" action="edithall.php" enctype="multipart/form-data">
                                   <div class="modal-header">
                                        <h5 class="modal-title" id="example-Modal3">Update Hall</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                            <div class="form-group">
                                                <label for="recipient-name" class="form-control-label">Name Hall:</label>
                                                <input type="text" class="form-control" name="name" id="name">
                                            </div>
                                            <div class="form-group">
                                                <label for="recipient-name" class="form-control-label">Area (m²)</label>
                                                <input type="text" class="form-control" name="area" id="area">
                                            </div>
                                            <div class="form-group">
                                                <label for="recipient-name" class="form-control-label">avenue</label>
                                                <input type="text" class="form-control" name="avenue" id="avenue">
                                            </div>
                                            <div class="form-group">
                                                <label for="recipient-name" class="form-control-label">Status:</label>
                                                <input type="text" class="form-control" name="status" id="status">
                                               <!--<select class="form-control select2 custom-select" data-placeholder="Choose one" name="status" id="status">
                                                            <option label="Choose one"></option>
                                                            <option value="available">Available</option>
                                                            <option value="Not Available">Not Available</option>
                                                </select> -->
                                            </div>
                                            <div class="modal-footer">
                                                <input type="hidden" name="hall_id" id="hall_id">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Edit Hall</button>
                                            </div>
                                        
                                    </div>
                                  </form>
                                </div>
                            </div>
                        </div>
                        <!-- Message Modal view -->
                        <div class="modal fade" id="ViewHall" tabindex="-1" role="dialog"  aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form id="modal" class="form-horizontal" method="POST" action="" enctype="multipart/form-data">
                                   <div class="modal-header">
                                        <h5 class="modal-title" id="example-Modal3">View Hall</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                            <div class="form-group">
                                                <label for="recipient-name" class="form-control-label">Name Hall:</label>
                                                <input type="text" class="form-control" name="name" id="name" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label for="recipient-name" class="form-control-label">Area (m²)</label>
                                                <input type="text" class="form-control" name="area" id="area" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label for="recipient-name" class="form-control-label">avenue</label>
                                                <input type="text" class="form-control" name="avenue" id="avenue" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label for="message-text" class="form-control-label">Status:</label>
                                                <input type="text" class="form-control" name="status" id="status" readonly>
                                            </div>
                                            <div class="modal-footer">
                                                <input type="hidden" name="hall_id" id="hall_id">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            </div>
                                        
                                    </div>
                                  </form>
                                </div>
                            </div>
                        </div>
                <div class="row">
				<div class="col-md-12 col-lg-12">
                        <div class="card">
									<div class="card-status bg-azure br-tr-3 br-tl-3"></div>
									<div class="card-header">
										<h3 class="card-title">Record Hall</h3>
									</div>
									<div class="card-body">
										<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
											<div class="panel panel-default active">
												<div class="panel-heading " role="tab" id="headingOne">
													<h4 class="panel-title">
														<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true"  aria-controls="collapseOne">

															List Hall
														</a>
													</h4>
												</div>
												<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
													<div class="panel-body">
                                                        <div class="card-footer text-right">
                                                            <div class="card-body">
                                                                 <button type="button" class="btn btn-info" data-toggle="modal" data-target="#AddHall">Insert New Hall</button>

                                                            </div>
                                                        </div>
                                                       
														
                                            <div class="card-body">
                                                <div class="table-responsive">
                                                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                        <thead>
                                                            <tr>
                                                                <th class="wd-20p">Name Hall</th>
                                                                <th class="wd-20p">Area (m²)</th>
                                                                <th class="wd-20p">Avenue</th>
                                                                <th class="wd-20p">Status</th>
                                                                <th class="wd-20p">Action</th>

                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                             <?php
                                                             $counter = 1;
                                                               if($job == "admin"){

                                                                        $statement = $conn->prepare("SELECT * FROM hall");

                                                               }else {
                                                                    $statement = $conn->prepare("SELECT * FROM hall");

                                                               }
                                                                        $statement->execute();

                                                                       while($data = $statement->fetch(PDO::FETCH_ASSOC))
                                                                       {

                                                                       ?>
                                                                            <tr>
                                                                              <td><?php echo $data["NameHall"]; ?></td>
                                                                              <td><?php echo $data["area"];?></td>
                                                                              <td><?php echo $data["avenue"];?></td>
                                                                              <td><?php echo $data["status"];?></td>
                                                                                 <td>
                                                                                    <button type="button" name="edit" id="edit" class="btn btn-info btn-xs"  data-toggle="modal" data-target="#ViewHall"
                                                                                    data-hall_id="<?php echo $data["hall_id"]; ?>"
                                                                                    data-name="<?php echo $data["NameHall"];?>" 
                                                                                    data-area="<?php echo $data["area"];?>" 
                                                                                    data-avenue="<?php echo $data["avenue"];?>" 
                                                                                    data-status="<?php echo $data["status"]; ?>"
                                                                                    > <i class="fa fa-pencil"></i>&nbsp; View</button>
                                                                                     
                                                                                     
                                                                                    <button type="button" name="edit" id="edit" class="btn btn-success btn-xs"  data-toggle="modal" data-target="#EditHall"
                                                                                    data-hall_id="<?php echo $data["hall_id"]; ?>"
                                                                                    data-name="<?php echo $data["NameHall"];?>" 
                                                                                    data-area="<?php echo $data["area"];?>" 
                                                                                    data-avenue="<?php echo $data["avenue"];?>" 
                                                                                    data-status="<?php echo $data["status"]; ?>"
                                                                                    > <i class="fa fa-pencil"></i>&nbsp;Edit </button>
                                                                                </td>
                                                                            </tr>
                                                                       <?php
                                                                           $counter++;
                                                                       }
                                                                       ?>

                                                        </tbody>
											</table>
										</div>
									</div>
													</div>
												</div>
											</div>
											<div class="panel panel-default mt-2">
												<div class="panel-heading" role="tab" id="headingTwo">
													<h4 class="panel-title">
														<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">

															All Record Hall Apply by Resident
														</a>
													</h4>
												</div>
												<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
													<div class="panel-body">
														Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
													</div>
												</div>
											</div>
											<div class="panel panel-default mt-2">
												<div class="panel-heading" role="tab" id="headingThree">
													<h4 class="panel-title">
														<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">

															Collapsible Group Item #3
														</a>
													</h4>
												</div>
												<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
													<div class="panel-body">
														Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
													</div>
												</div>
											</div>
										</div><!-- panel-group -->
									</div>
								</div>
                    </div>
</div>

									<!-- table-wrapper -->
								</div>
								<!-- section-wrapper -->

							</div>
						</div>
					</div>
                    
                    <?php
    
                        include('footer.php');
                    ?>
