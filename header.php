 <?php include("session.php"); 

            // Set Default Time Zone for Asia/Kuala_Lumpur
            date_default_timezone_set("Asia/Kuala_Lumpur");

            // Check, if username session is NOT set then this page will jump to login page
            if (!isset($_SESSION['session']) && !isset($_SESSION['job'])) {
                header('Location: login.php');
                //session_destroy();
            }
$job = $_SESSION["job"];
?>

<html lang="en" dir="ltr">
<!-- Mirrored from spruko.com/demo/ren/Blue-animated/LeftMenu/form-elements.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 20 Sep 2018 05:54:34 GMT -->
<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="msapplication-TileColor" content="#0061da">
		<meta name="theme-color" content="#1643a3">
		<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="mobile-web-app-capable" content="yes">
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<link rel="icon" href="favicon.ico" type="image/x-icon"/>
		<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />

		<!-- Title -->
		<title>Sistem eFacilities</title>

        <!--Font Awesome-->
		<link href="assets/plugins/fontawesome-free/css/all.css" rel="stylesheet">
    <!--css plan bulding
        <link href="assets/css/plan.css" rel="stylesheet"> -->

		<!-- Font Family -->
		<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500" rel="stylesheet">

		<!-- Dashboard Css -->
		<link href="assets/css/dashboard.css" rel="stylesheet" />

		<!-- Custom scroll bar css-->
		<link href="assets/plugins/scroll-bar/jquery.mCustomScrollbar.css" rel="stylesheet" />

		<!-- Sidemenu Css -->
		<link href="assets/plugins/toggle-sidebar/css/sidemenu.css" rel="stylesheet">

		<!-- c3.js Charts Plugin -->
		<link href="assets/plugins/charts-c3/c3-chart.css" rel="stylesheet" />

		<!-- select2 Plugin -->
		<link href="assets/plugins/select2/select2.min.css" rel="stylesheet" />

		<!-- Time picker Plugin -->
		<link href="assets/plugins/time-picker/jquery.timepicker.css" rel="stylesheet" />

		<!-- Date Picker Plugin -->
		<link href="assets/plugins/date-picker/spectrum.css" rel="stylesheet" />
    
        <!-- forn-wizard css-->
		<link href="assets/plugins/forn-wizard/css/material-bootstrap-wizard.css" rel="stylesheet" />
		<link href="assets/plugins/forn-wizard/css/demo.css" rel="stylesheet" />
        <!-- forn-full calendar-->
        <link href='assets/plugins/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
        <link href='assets/plugins/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />

		<!---Font icons-->
		<link href="assets/plugins/iconfonts/plugin.css" rel="stylesheet" />
    
        <!-- Data table css -->
		<link href="assets/plugins/datatable/dataTables.bootstrap4.min.css" rel="stylesheet" />
        
	</head>
     <?php
                if(isset($_SESSION["session"]))
                {
                    $email = $_SESSION["session"];
                    $sql = "SELECT * FROM user WHERE email = :email";
                    $stmt = $conn->prepare($sql);
                    $stmt->bindParam(":email", $email);
                    $stmt->execute();
                    
                    if($dt = $stmt->fetch(PDO::FETCH_ASSOC))
                    {
                        $user_id = $dt["user_id"];
                        $name = $dt["name"];                       
                        $email = $dt["email"];
                        $position = $dt["position"];
                       
						
                    }
                }
                else
                {
                    echo "Data is not found!";
                }	
                ?>
    
	<body class="app sidebar-mini rtl">
		<div id="global-loader" ><div class="showbox"><div class="lds-ring"><div></div><div></div><div></div><div></div></div></div></div>
		<div class="page">
			<div class="page-main">
				<!-- Navbar-->
				<header class="app-header header">
				
					<!-- Header Background Animation-->
					<div id="canvas" class="gradient"></div>
					
					<!-- Navbar Right Menu-->
					<div class="container-fluid">
						<div class="d-flex">
							<a class="header-brand" href="index-2.html">
								<img alt="ren logo" class="header-brand-img" src="assets/images/brand/logo.png">
							</a>
							<!-- Sidebar toggle button-->
							<a aria-label="Hide Sidebar" class="app-sidebar__toggle" data-toggle="sidebar" href="#"></a>
							<div class="d-flex order-lg-2 ml-auto">
								<div class="">
									<form class="input-icon  mr-2">
										<input class="form-control header-search" placeholder="Search&hellip;" tabindex="1" type="search">
										<div class="input-icon-addon">
											<i class="fe fe-search"></i>
										</div>
									</form>
								</div>
								<div class="dropdown d-none d-md-flex">
									<a class="nav-link icon" data-toggle="dropdown">
										<i class="fas fa-user"></i>
										<span class="nav-unread bg-green"></span>
									</a>
									<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
										<a class="dropdown-item d-flex pb-3" href="#">
											<span class="avatar brround mr-3 align-self-center" style="background-image: url(assets/images/faces/male/4.jpg)"></span>
											<div>
												<strong>EKMAL AZMI</strong> Sent you add request
												<div class="small text-muted">
													view profile
												</div>
											</div>
										</a>
										<a class="dropdown-item d-flex pb-3" href="#">
											<span class="avatar brround mr-3 align-self-center" style="background-image: url(assets/images/faces/female/14.jpg)"></span>
											<div>
												<strong>rebica</strong> Suggestions for you
												<div class="small text-muted">
													view profile
												</div>
											</div>
										</a>
										<a class="dropdown-item d-flex pb-3" href="#">
											<span class="avatar brround mr-3 align-self-center" style="background-image: url(assets/images/faces/male/1.jpg)"></span>
											<div>
												<strong>Devid robott</strong> sent you add request
												<div class="small text-muted">
													view profile
												</div>
											</div>
										</a>
										<div class="dropdown-divider"></div><a class="dropdown-item text-center text-muted-dark" href="#">View all contact list</a>
									</div>
								</div>
								<div class="dropdown d-none d-md-flex">
									<a class="nav-link icon" data-toggle="dropdown">
										<i class="fas fa-bell"></i>
										<span class="nav-unread bg-danger"></span>
									</a>
									<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
										<a class="dropdown-item d-flex pb-3" href="#">
											<div class="notifyimg">
												<i class="fas fa-thumbs-up"></i>
											</div>
											<div>
												<strong>Someone likes our posts.</strong>
												<div class="small text-muted">
													3 hours ago
												</div>
											</div>
										</a>
										<a class="dropdown-item d-flex pb-3" href="#">
											<div class="notifyimg">
												<i class="fas fa-comment-alt"></i>
											</div>
											<div>
												<strong>3 New Comments</strong>
												<div class="small text-muted">
													5 hour ago
												</div>
											</div>
										</a>
										<a class="dropdown-item d-flex pb-3" href="#">
											<div class="notifyimg">
												<i class="fas fa-cogs"></i>
											</div>
											<div>
												<strong>Server Rebooted.</strong>
												<div class="small text-muted">
													45 mintues ago
												</div>
											</div>
										</a>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item text-center text-muted-dark" href="#">View all Notification</a>
									</div>
								</div>
								<div class="dropdown d-none d-md-flex">
									<a class="nav-link icon" data-toggle="dropdown"><i class="fas fa-envelope"></i> <span class="badge badge-info badge-pill">2</span></a>
									<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
										<a class="dropdown-item text-center text-dark" href="#">2 New Messages</a>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item d-flex pb-3" href="#">
											<span class="avatar brround mr-3 align-self-center" style="background-image: url(assets/images/faces/male/41.jpg)"></span>
											<div>
												<strong>Madeleine</strong> Hey! there I' am available....
												<div class="small text-muted">
													3 hours ago
												</div>
											</div>
										</a>
										<a class="dropdown-item d-flex pb-3" href="#"><span class="avatar brround mr-3 align-self-center" style="background-image: url(assets/images/faces/female/1.jpg)"></span>
											<div>
												<strong>Anthony</strong> New product Launching...
												<div class="small text-muted">
													5 hour ago
												</div>
											</div>
										</a>
										<a class="dropdown-item d-flex pb-3" href="#"><span class="avatar brround mr-3 align-self-center" style="background-image: url(assets/images/faces/female/18.jpg)"></span>
											<div>
												<strong>Olivia</strong> New Schedule Realease......
												<div class="small text-muted">
													45 mintues ago
												</div>
											</div>
										</a>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item text-center text-muted-dark" href="#">See all Messages</a>
									</div>
								</div>
								<div class="dropdown">
									<a class="nav-link pr-0 leading-none d-flex" data-toggle="dropdown" href="#">
										<span class="avatar avatar-md brround" style="background-image: url(assets/images/faces/female/25.jpg)"></span>
										<span class="ml-2 d-none d-lg-block">
											<span class="text-white"><?php echo $name; ?></span>
										</span>
									</a>
									<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
										<a class="dropdown-item" href="profile.php"><i class="dropdown-icon mdi mdi-account-outline"></i> Profile</a>
										<a class="dropdown-item" href="#"><i class="dropdown-icon mdi mdi-settings"></i> Settings</a>
										<a class="dropdown-item" href="#"><span class="float-right"><span class="badge badge-primary">6</span></span> <i class="dropdown-icon mdi mdi-message-outline"></i> Inbox</a>
										<a class="dropdown-item" href="#"><i class="dropdown-icon mdi mdi-comment-check-outline"></i> Message</a>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item" href="#"><i class="dropdown-icon mdi mdi-compass-outline"></i> Need help?</a>
										<a class="dropdown-item" href="logout.php"><i class="dropdown-icon mdi mdi-logout-variant"></i> Sign out</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</header>

								<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
				<aside class="app-sidebar">
					<div class="app-sidebar__user">
						<div class="dropdown">
							<a class="nav-link p-0 leading-none d-flex" data-toggle="dropdown" href="#">
								<span class="avatar avatar-md brround" style="background-image: url(assets/images/faces/female/25.jpg)"></span>
								<span class="ml-2 "><span class="text-white app-sidebar__user-name font-weight-semibold"><?php echo $name; ?></span><br>
									<span class="text-muted app-sidebar__user-name text-sm"> <?php echo $position; ?></span>
								</span>
							</a>
							<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
								<a class="dropdown-item" href="#"><i class="dropdown-icon mdi mdi-account-outline"></i> Profile</a>
								<a class="dropdown-item" href="#"><i class="dropdown-icon mdi mdi-settings"></i> Settings</a>
								<a class="dropdown-item" href="#"><span class="float-right"><span class="badge badge-primary">6</span></span> <i class="dropdown-icon mdi mdi-message-outline"></i> Inbox</a>
								<a class="dropdown-item" href="#"><i class="dropdown-icon mdi mdi-comment-check-outline"></i> Message</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="#"><i class="dropdown-icon mdi mdi-compass-outline"></i> Need help?</a>
								<a class="dropdown-item" href="login.html"><i class="dropdown-icon mdi mdi-logout-variant"></i> Sign out</a>
							</div>
						</div>
					</div>
					<ul class="side-menu">
                        <li>
							<a class="side-menu__item" href="dashboard.php"><i class="side-menu__icon fas fa-home"></i><span class="side-menu__label">Dashboard</span></a>
						</li>											
						<?php
                                          if($job == "Admin")
                                          {
                                            ?>
                                                <li class="slide">
                                                    <a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fas fa-chart-bar"></i><span class="side-menu__label">Building Facility</span><i class="angle fas fa-angle-right"></i></a>
                                                    <ul class="slide-menu">
                                                        <li>
                                                            <a href="plan.php" class="slide-item">Plan Building</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="slide">
                                                    <a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fas fa-chart-bar"></i><span class="side-menu__label">Fix Asset Management</span><i class="angle fas fa-angle-right"></i></a>
                                                    <ul class="slide-menu">
                                                        <li>
                                                            <a href="building.php" class="slide-item">Building</a>
                                                        </li>	
                                                        <li>
                                                            <a href="pool.php" class="slide-item">Pool</a>
                                                        </li>	
                                                        <li>
                                                            <a href="playground.php" class="slide-item">Playground</a>
                                                        </li>	
                                                        <li>
                                                            <a href="hall.php" class="slide-item">Hall</a>
                                                        </li>	
                                                        <li>
                                                            <a href="parkingLot.php" class="slide-item">Parking Lot</a>
                                                        </li>	
                                                        <li>
                                                            <a href="access_card.php" class="slide-item">Access Card</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="slide-item">Utility</a>
                                                        </li>						
                                                    </ul>
                                                </li>
                                            <?php   
                                          }
                                          
                                          else
                                          {
                                              ?>
                                                
                                              <?php
                                          }
                                          ?>
                       
                        <li class="slide">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fas fa-chart-bar"></i><span class="side-menu__label">Currect Asset Management</span><i class="angle fas fa-angle-right"></i></a>
							<ul class="slide-menu">
                                <li>
									<a href="invoice.php" class="slide-item">Fee Maintenance</a>
								</li>						
							</ul>
						</li>
                         <?php
                                          if($job == "Admin")
                                          {
                                            ?>
                                                <li class="slide">
                                                    <a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fas fa-chart-bar"></i><span class="side-menu__label">Manage services</span><i class="angle fas fa-angle-right"></i></a>
                                                    <ul class="slide-menu">
                                                        <li>
                                                            <a href="complaint_v1.php" class="slide-item">Complaint</a>
                                                        </li>	
                                                         <li>
                                                            <a href="servicesPool.php" class="slide-item">Swimming Pool</a>
                                                        </li>
                                                        <li>
                                                            <a href="servicesAccessCard.php" class="slide-item">Access Card</a>
                                                        </li>                                                        
                                                        <li>
                                                            <a href="servicesParking.php" class="slide-item">Parking Reservation</a>
                                                        </li>
                                                        <li>
                                                            <a href="servicesVisitor.php" class="slide-item">Apply Visitor</a>
                                                        </li>
                                                        <li>
                                                            <a href="servicesRent.php" class="slide-item">Manage Rent House</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                
                                            <?php   
                                          }                                          
                                          else
                                          {
                                              ?>   
                                                <li class="slide">
                                                    <a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fas fa-chart-bar"></i><span class="side-menu__label">Services Provided</span><i class="angle fas fa-angle-right"></i></a>
                                                    <ul class="slide-menu">
                                                        <li>
                                                            <a href="complaint_v1.php" class="slide-item">Complaint</a>
                                                        </li>	
                                                         <li>
                                                            <a href="servicesPool.php" class="slide-item">Swimming Pool</a>
                                                        </li>
                                                        <li>
                                                            <a href="servicesAccessCard.php" class="slide-item">Access Card</a>
                                                        </li>                                                        
                                                        <li>
                                                            <a href="servicesParking.php" class="slide-item">Parking Reservation</a>
                                                        </li>
                                                        <li>
                                                            <a href="servicesVisitor.php" class="slide-item">Apply Visitor</a>
                                                        </li>
                                                        <li>
                                                            <a href="servicesRent.php" class="slide-item">Rent House</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                              <?php
                                          }
                                          ?>
                        
                        
                         
						
                        <?php
                                          if($job == "Admin")
                                          {
                                            ?>
                                                <li class="slide">
                                                    <a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fas fa-chart-bar"></i><span class="side-menu__label">Vendor Management</span><i class="angle fas fa-angle-right"></i></a>
                                                    <ul class="slide-menu">
                                                        <li>
                                                            <a href="managevendor.php" class="slide-item">Information Vendor</a>
                                                        </li>	
                                                    </ul>
                                                </li>
                                                <li class="slide">
                                                    <a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fas fa-chart-bar"></i><span class="side-menu__label">User Management</span><i class="angle fas fa-angle-right"></i></a>
                                                    <ul class="slide-menu">
                                                        <li>
                                                            <a href="registerUser.php" class="slide-item">Register User</a>
                                                        </li>
                                                        <li>
                                                            <a href="manageoccupant.php" class="slide-item">Information Resident</a>
                                                        </li>
                                                        <li>
                                                            <a href="manageemployee.php" class="slide-item">Information Employee</a>
                                                        </li>								
                                                    </ul>
                                                </li>
                                                
                                            <?php   
                                          }                                          
                                          else
                                          {
                                              ?>                                           
                                              <?php
                                          }
                                          ?>
                        
                        
                        <li>
							<a class="side-menu__item" href="notice.php"><i class="side-menu__icon fas fa-images"></i><span class="side-menu__label">Notification</span></a>
						</li>
                         <li>
							<a class="side-menu__item" href="logout.php"><i class="side-menu__icon fas fa-images"></i><span class="side-menu__label">Log Out</span></a>
						</li>
						
				</aside>