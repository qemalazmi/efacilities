<?php
// Connecting to the database
include("connection.php");
include("session.php");
  if (!isset($_SESSION['session'])) {
        header('Location: index.php');
        session_destroy();
    }

if(isset($_POST["name"]) && isset($_POST["nric"]) && isset($_POST["phone"]) && isset($_POST["address"]) && isset($_POST["status"]) && isset($_POST["unit_house"])){

        $name = $_POST["name"];
        $nric = $_POST["nric"];
        $phone = $_POST["phone"];
        $address = $_POST["address"];
        $status = $_POST["status"];
        $unit_house = $_POST["unit_house"];
        $position ="user";
        $email = $_POST["email"];
        $password = $_POST["nric"];
        
       
      try
      {
        
        $stmt = $conn->prepare("INSERT INTO resident (name, nric, phone, address, status, unit_house, position, email, password) VALUES  (:name, :nric, :phone, :address, :status, :unit_house, :position, :email, :password)");
	    $stmt->bindParam(':name', $name);
        $stmt->bindParam(':nric', $nric);
        $stmt->bindParam(':phone', $phone);
        $stmt->bindParam(':address', $address);
        $stmt->bindParam(':status', $status);
        $stmt->bindParam(':unit_house', $unit_house);
        $stmt->bindParam(':position', $position);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':password', $password);
		$stmt->execute();
      } catch(PDOException $e){
        $message = "ERROR : ".$e->getMessage();  
      }

}
echo $message;

header('Location: manageoccupant.php');

?>