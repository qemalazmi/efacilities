<?php
    include('header.php');
?>

				<div class="app-content my-3 my-md-5">
					<div class="side-app">
						<div class="page-header">
							
                            <ol class="breadcrumb1">
											<li class="breadcrumb-item1 active">User Management</li>
											<li class="breadcrumb-item1 active">Employee </li>
							</ol>
							<div class="card-footer text-right">
                                       
										      <button type="submit" class="btn btn-primary" onclick="window.location.href='registerUser.php'" >Register Empolyee</button>
                                       
				            </div>
                            
						</div>
                         <!--modal view -->
                        <div class="modal fade" id="viewEmployee" tabindex="-1" role="dialog"  aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form id="modal" class="form-horizontal" method="POST" action="" enctype="multipart/form-data">
                                   <div class="modal-header">
                                        <h5 class="modal-title" id="example-Modal3">View Information Resident</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                            <div class="row">
                                             
                                           <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Full Name</label>
													<input type="text" class="form-control" name="name" id="name" readonly >
												</div>
											</div>
											<div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">NRIC / Passport</label>
													<input type="text" class="form-control" name="nric" id="nric" readonly >
												</div>
											</div>
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Contact Number</label>
													<input type="text" class="form-control" name="phone" id="phone" readonly >
												</div>
											</div>
                                             <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Email</label>
													<input type="text" class="form-control" name="email" id="email" readonly >
												</div>
											</div>
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Position</label>
													<input type="text" class="form-control" name="position" id="position" readonly >
												</div>
											</div>
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Status</label>
													<input type="text" name="status" id="status" class="form-control" readonly>
												</div>
											</div>
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Gender</label>
													<input type="text" name="gender" id="gender" class="form-control" readonly>
												</div>
											</div>
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">State</label>
													<input type="text" name="state" id="state" class="form-control" readonly>
												</div>
											</div>
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Region</label>
													<input type="text" name="region" id="region" class="form-control" readonly>
												</div>
											</div>
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Descendant</label>
													<input type="text" name="descendant" id="descendant" class="form-control" readonly>
												</div>
											</div> 
                                            <div class="col-sm-6 col-md-12">
												<div class="form-group">
													<label class="form-label">Address</label>
													<input type="text" class="form-control" name="address" id="address" readonly >
												</div>
											</div> 
											<div class="col-md-5">
												<div class="form-group">
													<label class="form-label">Postal Code</label>
													<input type="text" class="form-control" name="postal" id="postal" readonly >
												</div>
											</div>
											<div class="col-sm-6 col-md-3">
												<div class="form-group">
													<label class="form-label">City</label>
													<input type="text" class="form-control" name="city" id="city" readonly >
												</div>
											</div>
											<div class="col-sm-6 col-md-4">
												<div class="form-group">
													<label class="form-label">Country</label>
													<input type="text" class="form-control" name="country" id="country" readonly>
												</div>
											</div>
                                                <div class="col-sm-12 col-md-12">
                                            <div class="modal-footer">
                                                <input type="hidden" name="employee_id" id="employee_id">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            </div>
                                            </div>
                                        
                                    </div>
                                        </div>
                                  </form>
                                </div>
                            </div>
                        </div>
						<div class="row">
							<div class="col-md-12 col-lg-12">
								<div class="card">
									<div class="card-status bg-yellow br-tr-3 br-tl-3"></div>
									<div class="card-header">
										<div class="card-title">Record Employee</div>
									</div>
                                    
									<div class="card-body">
										<div class="table-responsive">
											<table id="example" class="table table-striped table-bordered" style="width:100%">
												<thead>
													<tr>
														<th class="wd-15p">Name Employee</th>
														<th class="wd-15p">NRIC</th>
														<th class="wd-20p">Contact Number</th>
														<th class="wd-15p">Position</th>
														<th class="wd-10p">email</th>
                                                        <th class="wd-25p">Action</th>
                                                        <th class="wd-25p">Action</th>
													</tr>
												</thead>
												<tbody>
													 <?php
                                                     $counter = 1;
                                                       if($job == "Admin"){
                                                           
                                                                $statement = $conn->prepare("SELECT * FROM user where position='Employee'");
                                                           
                                                       }else {
                                                            $statement = $conn->prepare("SELECT * FROM user where position='Employee'");
                                                           
                                                       }
                                                                $statement->execute();

                                                               while($data = $statement->fetch(PDO::FETCH_ASSOC))
                                                               {
                                                                   
                                                               ?>
                                                                    <tr>
                                                                      <td><?php echo $data["name"]; ?></td>
                                                                      <td><?php echo $data["nric"]; ?></td>
                                                                      <td><?php echo $data["phone"]; ?></td> 
                                                                      <td><?php echo $data["position"]; ?></td> 
                                                                      <td><?php echo $data["email"]; ?></td>
                                                                      
                                                                     
                                                                        
                                                                        
                                                                       
                                                                        <td style="text-align: center;vertical-align: middle;">
                                                                            <a href="employee_view.php?user_id=<?php echo $data["user_id"]; ?>">
                                                                              <button type="button" name="update" id="update" class="btn btn-info btn-sm">
                                                                                  <i class="fa fa-pencil"></i>&nbsp;&nbsp;View
                                                                              </button>

                                                                            </a>
                                                                        </td>
                                                                        <td style="text-align: center;vertical-align: middle;">
                                                                            <a href="employeeedit.php?user_id=<?php echo $data["user_id"]; ?>">
                                                                              <button type="button" name="update" id="update" class="btn btn-info btn-sm">
                                                                                  <i class="fa fa-pencil"></i>&nbsp;&nbsp;Edit
                                                                              </button>

                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                               <?php
                                                                   $counter++;
                                                               }
                                                               ?>
												</tbody>
											</table>
										</div>
									</div>
									<!-- table-wrapper -->
								</div>
								<!-- section-wrapper -->

							</div>
						</div>
					</div>
                    <?php
    
                        include('footer.php');
                    ?>
