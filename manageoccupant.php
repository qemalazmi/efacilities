<?php
    include('header.php');
    include("session.php");

            // Set Default Time Zone for Asia/Kuala_Lumpur
            date_default_timezone_set("Asia/Kuala_Lumpur");

            // Check, if username session is NOT set then this page will jump to login page
            if (!isset($_SESSION['session']) && !isset($_SESSION['job'])) {
                header('Location: login.php');
                //session_destroy();
            }
$job = $_SESSION["job"];


?>

				<div class="app-content my-3 my-md-5">
					<div class="side-app">
						<div class="page-header">
                            <ol class="breadcrumb1">
											<li class="breadcrumb-item1 active">User Management</li>
											<li class="breadcrumb-item1 active">Resident </li>
							</ol>
							
							<div class="card-footer text-right">
                                       
										      <button type="submit" class="btn btn-primary" onclick="window.location.href='registerUser.php'" >Register Resident</button>
                                       
				            </div>
                            
						</div>
                        <!--modal view -->
                        <div class="modal fade" id="viewResident" tabindex="-1" role="dialog"  aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form id="modal" class="form-horizontal" method="POST" action="" enctype="multipart/form-data">
                                   <div class="modal-header">
                                        <h5 class="modal-title" id="example-Modal3">View Information Resident</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                            <div class="row">
                                             
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Name</label>
													<input type="text" name="name" id="name" class="form-control" readonly>
												</div>
											</div>
											<div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">NRIC</label>
													<input type="text" name="nric" id="nric" class="form-control" readonly>
												</div>
											</div>                                             
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Contact Number</label>
													<input type="text" name="phone" id="phone" class="form-control" readonly>
												</div>
											</div>
											<div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Address</label>
													<input type="text" name="address" id="address" class="form-control" readonly >
												</div>
											</div>
                                             <div class="col-sm-6 col-md-6">
                                                <label for="recipient-name" class="form-control-label">Status:</label>
                                                <input type="text" name="status" id="status" class="form-control" readonly >
                                            </div>
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">No Unit House</label>
													<input type="text" name="unit_house" id="unit_house" class="form-control" readonly >
												</div>
											</div>
                                            <div class="col-sm-12 col-md-12">
                                            <div class="modal-footer">
                                                <input type="hidden" name="resident_id" id="resident_id">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            </div>
                                            </div>
                                        
                                    </div>
                                        </div>
                                  </form>
                                </div>
                            </div>
                        </div>
						<div class="row">
							<div class="col-md-12 col-lg-12">
								<div class="card">
									<div class="card-status bg-yellow br-tr-3 br-tl-3"></div>
									<div class="card-header">
										<div class="card-title">Record Resident</div>
									</div>
                                    
									<div class="card-body">
										<div class="table-responsive">
											<table id="example" class="table table-striped table-bordered" style="width:100%">
												<thead>
													<tr>
														<th class="wd-15p">Name Resident</th>
														<th class="wd-15p">NRIC</th>
														<th class="wd-20p">Contact Number</th>
														<th class="wd-15p">Address</th>
														<th class="wd-10p">Status Resident</th>
														<th class="wd-25p">Unit House</th>
                                                        <th class="wd-25p">Action</th>
                                                        <th class="wd-25p">Action</th>
													</tr>
												</thead>
												<tbody>
													 <?php
                                                     $counter = 1;
                                                       if($job == "Admin"){
                                                           
                                                                $statement = $conn->prepare("SELECT * FROM user");
                                                           
                                                       }else {
                                                            $statement = $conn->prepare("SELECT * FROM user where position = 'Resident'");
                                                           
                                                       }
                                                                $statement->execute();

                                                               while($data = $statement->fetch(PDO::FETCH_ASSOC))
                                                               {
                                                                   
                                                               ?>
                                                                    <tr>
                                                                      <td><?php echo $data["name"]; ?></td>
                                                                      <td><?php echo $data["nric"]; ?></td>
                                                                      <td><?php echo $data["phone"]; ?></td> 
                                                                      <td><?php echo $data["address"]; ?></td> 
                                                                      <td><?php echo $data["status"]; ?></td>
                                                                      <td><?php echo $data["NoHouse"]; ?></td>
                                                                     
                                                                        
                                                                        <td style="text-align: center;vertical-align: middle;">
                                                                            <a href="residentView.php?user_id=<?php echo $data["user_id"]; ?>">
                                                                              <button type="submit" name="submit" id="submit" class="btn btn-info btn-sm">
                                                                                  <i class="fa fa-pencil"></i>&nbsp;&nbsp;View
                                                                              </button>

                                                                            </a>
                                                                        </td>
                                                                        <td style="text-align: center;vertical-align: middle;">
                                                                            <a href="residentedit.php?user_id=<?php echo $data["user_id"]; ?>">
                                                                              <button type="submit" name="update" id="update" class="btn btn-info btn-sm">
                                                                                  <i class="fa fa-pencil"></i>&nbsp;&nbsp;Edit
                                                                              </button>

                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                               <?php
                                                                   $counter++;
                                                               }
                                                               ?>
												</tbody>
											</table>
										</div>
									</div>
									<!-- table-wrapper -->
								</div>
								<!-- section-wrapper -->

							</div>
						</div>
					</div>
                    <?php
    
                        include('footer.php');
                    ?>
