<?php
    include('header.php');
    include("session.php");

            // Set Default Time Zone for Asia/Kuala_Lumpur
            date_default_timezone_set("Asia/Kuala_Lumpur");

            // Check, if username session is NOT set then this page will jump to login page
            if (!isset($_SESSION['session']) && !isset($_SESSION['job'])) {
                header('Location: login.php');
                //session_destroy();
            }
$job = $_SESSION["job"];


?>

				<div class="app-content my-3 my-md-5">
					<div class="side-app">
						<div class="page-header">
							<h4 class="page-title">Home > Vendor Management</h4>
							<div class="card-footer text-right">
                                       
										      <button type="submit" class="btn btn-primary" onclick="window.location.href='registervendor.php'" >Register Vendor</button>
                                       
				            </div>
                            
						</div>
						<div class="row">
							<div class="col-md-12 col-lg-12">
								<div class="card">
									<div class="card-status bg-yellow br-tr-3 br-tl-3"></div>
									<div class="card-header">
										<div class="card-title">Record Vendor Management</div>
									</div>
                                    
									<div class="card-body">
										<div class="table-responsive">
											<table id="example" class="table table-striped table-bordered" style="width:100%">
												<thead>
													<tr>
														<th class="wd-20p">Company Name</th>
														<th class="wd-20p">Address</th>
														<th class="wd-10p">Contact</th>
														<th class="wd-10p">Date Start Maintenance</th>
														<th class="wd-10p">End Start Maintenance</th>
														<th class="wd-15p">Name Of Officer</th>
                                                        <th class="wd-20p">Action</th>
                                                        <th class="wd-20p">Action</th>
														
													</tr>
												</thead>
												<tbody>
													 <?php
                                                     $counter = 1;
                                                       if($job == "admin"){
                                                           
                                                                $statement = $conn->prepare("SELECT * FROM vendor");
                                                           
                                                       }else {
                                                            $statement = $conn->prepare("SELECT * FROM vendor");
                                                           
                                                       }
                                                                $statement->execute();

                                                               while($data = $statement->fetch(PDO::FETCH_ASSOC))
                                                               {
                                                                   
                                                               ?>
                                                                    <tr>
                                                                      <td><?php echo $data["CompanyName"]; ?></td>
                                                                      <td>
                                                                        <?php echo $data["OfficeAddress"]; echo "<br>";
                                                                              echo $data["PostalCode"]; echo "<br>";
                                                                              echo $data["City"]; echo "<br>";
                                                                              echo $data["Country"];
                                                                          ?>
                                                                        </td>
                                                                      <td><?php echo $data["OfficeContact"]; ?></td> 
                                                                      <td><?php echo $data["StartMaintenance"]; ?></td> 
                                                                      <td><?php echo $data["EndMaintenance"]; ?></td> 
                                                                      <td>
                                                                          <?php 
                                                                              echo $data["NameOfOfficer"] ;
                                                                              echo $data["ContactOfOfficer"]; 
                                                                          ?>
                                                                      </td>
                                                                        
                                                                        <td style="text-align: center;vertical-align: middle;">
                                                                            <a href="vendor_view.php?VendorID=<?php echo $data["VendorID"]; ?>">
                                                                              <button type="button" name="update" id="update" class="btn btn-info btn-sm">
                                                                                  <i class="fa fa-pencil"></i>&nbsp;&nbsp;View
                                                                              </button>

                                                                            </a>
                                                                        </td>
                                                                        <td style="text-align: center;vertical-align: middle;">
                                                                            <a href="vendor_edit.php?VendorID=<?php echo $data["VendorID"]; ?>">
                                                                              <button type="button" name="update" id="update" class="btn btn-info btn-sm">
                                                                                  <i class="fa fa-pencil"></i>&nbsp;&nbsp;Edit
                                                                              </button>

                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                               <?php
                                                                   $counter++;
                                                               }
                                                               ?>
                                                                        
												</tbody>
											</table>
										</div>
									</div>
									<!-- table-wrapper -->
								</div>
								<!-- section-wrapper -->

							</div>
						</div>
					</div>
                    <?php
    
                        include('footer.php');
                    ?>
