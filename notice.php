<?php
    
    include('header.php');
    
?>

				
				<div class="app-content my-3 my-md-5">
					<div class="side-app">
						<div class="page-header">
							<h4 class="page-title">Full Calendar</h4>                            
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#">Calendar</a></li>
								<li class="breadcrumb-item active" aria-current="page">Full calendar</li>
							</ol>                            
						</div><div class="card-footer text-right">
                                       
										      <button type="submit" class="btn btn-primary" onclick="window.location.href='noticeadd.php'" >Insert Notice</button>
                                       
				            </div>
                        

						<div class="">
							<div class="card">
								<div class="card-status bg-primary br-tr-3 br-tl-3"></div>
								<div class="card-body">
									 <div id='calendar1'></div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 col-lg-12">
								<div class="card">
									<div class="card-status bg-yellow br-tr-3 br-tl-3"></div>
									<div class="card-header">
										<div class="card-title">New Notice</div>
									</div>
                                    
									<div class="card-body">
										<div class="table-responsive">
											<table id="example" class="table table-striped table-bordered" style="width:100%">
												<thead>
													<tr>
														<th class="wd-15p">title</th>
														<th class="wd-15p">Description</th>
														<th class="wd-20p">Created date</th>
														<th class="wd-15p">Start date</th>
														<th class="wd-10p">End Date</th>
                                                        <th class="wd-25p">Status</th>                                                        
                                                           <?php
                                                          if($job == "Admin")
                                                          {
                                                            ?>
                                                              <th class="wd-25p">Action</th>
                                                            <?php   
                                                          }

                                                          else
                                                          {
                                                              ?> 
                                                              <?php
                                                          }
                                                            ?>
													</tr>
												</thead>
												<tbody>
													 <?php
                                                     $counter = 1;
                                                       if($job == "Admin"){
                                                           
                                                                $statement = $conn->prepare("SELECT * FROM notice  ");
                                                           
                                                       }else {
                                                            $statement = $conn->prepare("SELECT * FROM notice where status='New'");
                                                           
                                                       }
                                                                $statement->execute();

                                                               while($data = $statement->fetch(PDO::FETCH_ASSOC))
                                                               {
                                                                   
                                                               ?>
                                                                    <tr>
                                                                      <td><?php echo $data["title"]; ?></td>
                                                                      <td><?php echo $data["description"]; ?></td>
                                                                      <td><?php echo $data["created_date"]; ?></td> 
                                                                      <td><?php echo $data["startdate"]; ?></td> 
                                                                      <td><?php echo $data["enddate"]; ?></td> 
                                                                      <td><?php echo $data["status"]; ?></td>
                                                                      
                                                                     
                                                                        
                                                                    
                                                                        
                                                                        
                                                            <?php
                                                              if($job == "Admin")
                                                              {
                                                                ?>
                                                                    <td style="text-align: center;vertical-align: middle;">
                                                                        <a href="notice_edit.php?notice_id=<?php echo $data["notice_id"]; ?>">
                                                                          <button type="submit" name="update" id="update" class="btn btn-info btn-sm">
                                                                              <i class="fa fa-pencil"></i>&nbsp;&nbsp;Edit
                                                                          </button>

                                                                        </a>
                                                                    </td>
                                                                <?php   
                                                              }

                                                              else
                                                              {
                                                                  ?> 
                                                                  <?php
                                                              }
                                                                ?>
                                                                    </tr>
                                                               <?php
                                                                   $counter++;
                                                               }
                                                               ?>
												</tbody>
											</table>
										</div>
									</div>
									<!-- table-wrapper -->
								</div>
								<!-- section-wrapper -->
                                
							</div>
						</div>
                        
					</div>
					
				</div>
<?php
    
    include('footer.php');
?>