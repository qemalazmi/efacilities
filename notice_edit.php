<!doctype html>
<html lang="en" dir="ltr">
<?php
    
    include('header.php');
?>
        <?php
            // Connection database
            //include("connection.php");
            // Check, for session 'user_session'
            include("session.php");

            // Set Default Time Zone for Asia/Kuala_Lumpur
            date_default_timezone_set("Asia/Kuala_Lumpur");

            // Check, if username session is NOT set then this page will jump to login page
            if (!isset($_SESSION['session']) && !isset($_SESSION['job'])) {
                header('Location: login.php');
                session_destroy();
            }

            $job = $_SESSION["job"];
            $now = date("Y-m-d");
        ?>
				<div class="my-3 my-md-5 app-content">
					<div class="side-app">
						<div class="page-header">
							<ol class="breadcrumb1">
											<li class="breadcrumb-item1 active">Notification</li>
											<li class="breadcrumb-item1 active">Notice Form </li>
							</ol>
							
						</div>
                        <?php
			 //--------------code get data ----------------
                if(isset($_GET["notice_id"]))
                {
                    $notice_id = $_GET["notice_id"];                    
                    $sql = 
                        "SELECT * FROM notice where notice_id = :notice_id" ; 
                         
                    $stmt = $conn->prepare($sql);
                    $stmt->bindParam(":notice_id", $notice_id);
                    $stmt->execute();
                    
                    if($dt = $stmt->fetch(PDO::FETCH_ASSOC))
                    {
                        $notice_id = $dt["notice_id"];
                        $created_by = $dt["created_by"];						
                        $title = $dt["title"];
                        $description = $dt["description"];
                        $startdate = $dt["startdate"];
                        $enddate = $dt["enddate"];
						$status = $dt["status"];
						$createdby_email = $dt["createdby_email"];
						$status = $dt["status"];
                       
                    }
                }
                else
                {
                    echo "Data is not found!";
                }
                ?>
							<div class="col-lg-12">
								<form class="card" method="POST" action="notice_update.php"  enctype="multipart/form-data">
									<div class="card-header">
										<h3 class="card-title">Notice Form</h3>
									</div>
									<div class="card-body">
										<div class="row">
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Name Created</label>
													<input type="text" class="form-control" name="name" id="name"  value="<?php  echo $created_by; ?>"readonly >
													<input type="text" class="form-control" name="notice_id" id="notice_id"  value="<?php  echo $notice_id; ?>" hidden>
												</div>
											</div>											
                                             <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Email</label>
													<input type="text" class="form-control" name="email" id="email" value="<?php  echo $createdby_email; ?>" readonly>
												</div>
											</div>
                                            <div class="col-sm-6 col-md-12">
												<div class="form-group">
													<label class="form-label">Title</label>
													<input type="text" class="form-control" name="title" id="title" value="<?php  echo $title; ?>" >
												</div>
											</div>
                                             
                                           <div class="col-sm-6 col-md-12">
												<div class="form-group">
													<label class="form-label">Description</label>
													<textarea rows="5" class="form-control" name="description" id="description" placeholder="Enter About your description Event" ><?php echo $description; ?></textarea>
												</div>
                                               
											</div>
                                            <div class="col-sm-6 col-md-6">
												
													<div class="form-group">
														<label class="fas fa-calendar tx-16 lh-0 op-6"> Start Date </label>
                                                        <input class="form-control" id="datepickerNoOfMonths" name="startdate" placeholder="MM/DD/YYYY" type="text" value="<?php  echo $startdate; ?>">
													</div>
												
											</div>
                                            <div class="col-sm-6 col-md-6">
												
													<div class="form-group">
														<label class="fas fa-calendar tx-16 lh-0 op-6"> End  Date </label>
                                                        <input class="form-control fc-datepicker"  name="enddate" placeholder="MM/DD/YYYY" type="text" value="<?php  echo $enddate; ?>">
													</div>
												
											</div>
                                            <div class="col-sm-6 col-md-12">
												<div class="form-group">
													<label class="form-label">Status Notice</label>
													<select class="form-control select2 custom-select" data-placeholder="Choose one" name="status" id="status">
                                                             <option value="" <?php if($status == "NULL"){ echo "selected"; }else{} ?>>-- Choose --</option>
                                                           <option value="New" <?php if($status == "New"){ echo "selected"; }else{} ?>>New</option>
                                                            <option value="Close" <?php if($status == "Close"){ echo "selected"; }else{} ?>>Close</option>
                                                     </select>
                                                    
												</div>
											</div>
                                            
																
										</div>
									</div>
									<div class="card-footer text-right">
										<button type="submit" class="btn btn-primary" >Update Notice</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					
<?php
    
    include('footer.php');
?>

