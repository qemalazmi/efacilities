<?php
// Connecting to the database
include("connection.php");
include("session.php");
  if (!isset($_SESSION['session'])) {
        header('Location: index.php');
        session_destroy();
    }

if(isset($_POST["level"]) && isset($_POST["noparking"]) && isset($_POST["type"])){

        $level = $_POST["level"];
        $noparking = $_POST["noparking"];
        $type = $_POST["type"];
        $status = "available";
      try
      {
        
        $stmt = $conn->prepare("INSERT INTO parking (level, noparking, type, status) VALUES  (:level, :noparking, :type, :status)");
	    $stmt->bindParam(':level', $level);
        $stmt->bindParam(':noparking', $noparking);
        $stmt->bindParam(':type', $type);
        $stmt->bindParam(':status', $status);
		$stmt->execute();
      } catch(PDOException $e){
        $message = "ERROR : ".$e->getMessage();  
      }

}
echo $message;

header('Location: parkingLot.php');

?>