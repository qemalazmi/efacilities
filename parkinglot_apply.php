<!doctype html>
<html lang="en" dir="ltr">
<?php    
    include('header.php');
?>
				<div class="my-3 my-md-5 app-content">
					<div class="side-app">
						<div class="page-header">
							<ol class="breadcrumb1">
											<li class="breadcrumb-item1 active">Fix Assets Management</li>
											<li class="breadcrumb-item1 active">Parking Lot</li>
							</ol>
						</div>
                        <div class="col-lg-12">
								<form class="card" method="POST" action="#" enctype="multipart/form-data">
									<div class="card-header">
										<h3 class="card-title">Apply Parking Lot</h3>
									</div>
									<div class="card-body">
										<div class="row">
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Name</label>
													<input type="text" name="CompanyName" id="CompanyName" class="form-control" required >
												</div>
											</div>
											<div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Contact Number</label>
													<input type="text" name="OfficeContact" id="OfficeContact" class="form-control" required >
												</div>
											</div>                                             
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Unit House</label>
													<input type="text" name="NameOfOfficer" id="NameOfOfficer" class="form-control" required >
												</div>
											</div>
											<div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Type Vehincle</label>
													<input type="text" name="ContactOfOfficer" id="ContactOfOfficer" class="form-control" required >
												</div>
											</div>
                                            <div class="col-sm-6 col-md-12">
												<div class="form-group">
													<label class="form-label">Plat Vehincle</label>
													<input type="text" name="TypeMaintenance" id="TypeMaintenance" class="form-control" required >
												</div>
											</div>
                                            <div class="col-sm-6 col-md-12">
												<div class="form-group">
													<label class="form-label">Number Lot Parking</label>
													<input type="text" name="OfficeAddress" id="OfficeAddress" class="form-control" required >
												</div>
											</div> 	
										</div>
									</div>
                                    <div class="card-footer text-right">
										<button type="submit" class="btn btn-primary" onclick="window.location.href='parkingLot.php'" >Cancel</button>
									
										<button type="submit" class="btn btn-primary" >Apply Parking Lot</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					
<?php
    
    include('footer.php');
?>

