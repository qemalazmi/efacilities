<?php
    include('header.php');

?>
<link href="assets/css/plan.css" rel="stylesheet">
                <div class="my-3 my-md-5 app-content">
					<div class="side-app">
						<div class="page-header">
							
							 <ol class="breadcrumb1">
											<li class="breadcrumb-item1 active">Building Facility</li>
											<li class="breadcrumb-item1 active">Plan Building</li>
							</ol>
						</div>
                        <div class="col-lg-12">
								<form class="card" method="post" action="">
                                    
                                <div class="main-content">
                                  <div class="door-hor"></div>
                                  <div class="utility">
                                    <div class="door-hor"></div>
                                    <div class="name">utility room</div>
                                  </div>
                                  <div class="kitchen">
                                    <div class="ref"></div>
                                    <div class="rug"></div>
                                    <div class="cabinets">
                                      <div class="stove"></div>
                                    </div>
                                    <div class="sink">
                                      <div class="sink-tap-1"></div>
                                      <div class="sink-tap-2"></div>
                                    </div>
                                    <div class="island">
                                      <div class="chair"></div>
                                    </div>
                                    <div class="name">kitchen</div>
                                  </div>
                                  <div class="bathroom-2">
                                    <div class="window-hor"></div>
                                    <div class="door-hor"></div>
                                    <div class="shower"></div>
                                    <div class="toilet"></div>
                                    <div class="vanity"></div>
                                    <div class="rug"></div>
                                    <div class="name">bath #1</div>
                                  </div>
                                  <div class="bedroom-2">
                                    <div class="door-ver"></div>
                                    <div class="window-hor"></div>
                                    <div class="bed-table"></div>
                                    <div class="bed">
                                      <div class="blanket"></div>
                                      <div class="pillow"></div>
                                    </div>
                                    <div class="table"></div>
                                    <div class="name">guest bedroom</div>
                                  </div>
                                  <div class="living-room">
                                    <div class="rug"></div>
                                    <div class="window-ver"></div>
                                    <div class="sofa"></div>
                                    <div class="coffee-table"></div>
                                    <div class="tv-set"></div>
                                    <div class="name">living room</div>
                                  </div>
                                  <div class="bedroom-1">
                                    <div class="rug"></div>
                                    <div class="door-hor"></div>
                                    <div class="window-hor"></div>
                                    <div class="bed">
                                      <div class="blanket"></div>
                                      <div class="pillow"></div>
                                      <div class="pillow"></div>
                                    </div>
                                    <div class="bed-table"></div>
                                    <div class="bed-table"></div>
                                    <div class="name">master bedroom</div>
                                  </div>
                                  <div class="closet">
                                    <div class="wall-gap"></div>
                                    <div class="name">walk-in closet</div>
                                  </div>
                                  <div class="bathroom-1">
                                    <div class="door-ver"></div>
                                    <div class="door-ver"></div>
                                    <div class="window-hor"></div>
                                    <div class="rug"></div>
                                    <div class="bathtub"></div>
                                    <div class="vanity"></div>
                                    <div class="toilet"></div>
                                    <div class="name">bath #2</div>
                                  </div>
                                  <div class="office">
                                    <div class="door-hor"></div>
                                    <div class="window-hor"></div>
                                    <div class="table"></div>
                                    <div class="name">home office</div>
                                  </div>
                                  <div class="patio">
                                    <div class="door-sliding"></div>
                                    <div class="real-patio">
                                      <div class="name">balcony</div>
                                    </div>
                                    <div class="patio-table"></div>
                                    <div class="patio-chair"></div>
                                    <div class="patio-chair"></div>
                                  </div>
                                </div>
                               
                            </form>
                        </div>
                    </div>
            </div>          


                    <?php
    
                        include('footer.php');
                    ?>
