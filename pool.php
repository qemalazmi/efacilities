<?php
    include('header.php');
    include("session.php");



?>
                <div class="app-content my-3 my-md-5">
					<div class="side-app">
						<div class="page-header">
							
							<ol class="breadcrumb1">
											<li class="breadcrumb-item1 active">Fix Assets Management</li>
											<li class="breadcrumb-item1 active">Pool</li>
							</ol>
                                    </div>
                        
        

                       <!-- Message Modal -->
                        <div class="modal fade" id="AddPool" tabindex="-1" role="dialog"  aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form id="modal" class="form-horizontal" method="POST" action="insertPool.php">
                                   <div class="modal-header">
                                        <h5 class="modal-title" id="example-Modal3">Add Pool</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                            <div class="form-group">
                                                <label for="recipient-name" class="form-control-label">Type Pool:</label>
                                                <input type="text" class="form-control" name="type" id="type">
                                            </div>
                                            <div class="form-group">
                                                <label for="recipient-name" class="form-control-label">Quantity: </label>
                                                <input type="text" class="form-control" name="quantity" id="quantity">
                                            </div>
                                        <div class="form-group">
                                                <label for="recipient-name" class="form-control-label">Area (m²): </label>
                                                <input type="text" class="form-control" name="area" id="area">
                                            </div>
                                        <div class="form-group">
                                                <label for="recipient-name" class="form-control-label">Volume (m3): </label>
                                                <input type="text" class="form-control" name="volume" id="volume">
                                            </div>
                                            <div class="form-group">
                                                <label for="recipient-name" class="form-control-label">Status:</label>
                                               <select class="form-control select2 custom-select" data-placeholder="Choose one" name="status" id="status">
                                                            <option label="Choose one"></option>
                                                            <option value="Available">Available</option>
                                                            <option value="Not Available">Not Available</option>
                                                </select>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Insert Pool</button>
                                            </div>
                                        
                                    </div>
                                  </form>
                                </div>
                            </div>
                        </div>
                         <!-- Message Modal Edit -->
                        <div class="modal fade" id="EditPool" tabindex="-1" role="dialog"  aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form id="modal" class="form-horizontal" method="POST" action="editPool.php" enctype="multipart/form-data">
                                   <div class="modal-header">
                                        <h5 class="modal-title" id="example-Modal3">Update Pool</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                            <div class="form-group">
                                                <label for="recipient-name" class="form-control-label">Type Pool:</label>
                                                <input type="text" class="form-control" name="type" id="type">
                                            </div>
                                            <div class="form-group">
                                                <label for="recipient-name" class="form-control-label">Quantity: </label>
                                                <input type="text" class="form-control" name="quantity" id="quantity">
                                            </div>
                                        <div class="form-group">
                                                <label for="recipient-name" class="form-control-label">Area (m²): </label>
                                                <input type="text" class="form-control" name="area" id="area">
                                            </div>
                                        <div class="form-group">
                                                <label for="recipient-name" class="form-control-label">Volume (m3): </label>
                                                <input type="text" class="form-control" name="volume" id="volume">
                                            </div>
                                            <div class="form-group">
                                                <label for="recipient-name" class="form-control-label">Status:</label>
                                               <input type="text" class="form-control" name="status" id="status">
                                            </div>
                                            <div class="modal-footer">
                                                <input type="hidden" name="pool_id" id="pool_id">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Edit Pool</button>
                                            </div>
                                        
                                    </div>
                                  </form>
                                </div>
                            </div>
                        </div>
                        <!-- Message Modal view -->
                        <div class="modal fade" id="ViewPool" tabindex="-1" role="dialog"  aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form id="modal" class="form-horizontal" method="POST" action="" enctype="multipart/form-data">
                                   <div class="modal-header">
                                        <h5 class="modal-title" id="example-Modal3">View Item Pool</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                   <div class="modal-body">
                                            <div class="form-group">
                                                <label for="recipient-name" class="form-control-label">Type Pool:</label>
                                                <input type="text" class="form-control" name="type" id="type" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label for="recipient-name" class="form-control-label">Quantity: </label>
                                                <input type="text" class="form-control" name="quantity" id="quantity" readonly>
                                            </div>
                                        <div class="form-group">
                                                <label for="recipient-name" class="form-control-label">Area (m²): </label>
                                                <input type="text" class="form-control" name="area" id="area" readonly>
                                            </div>
                                        <div class="form-group">
                                                <label for="recipient-name" class="form-control-label">Volume (m3): </label>
                                                <input type="text" class="form-control" name="volume" id="volume" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label for="recipient-name" class="form-control-label">Status:</label>
                                               <input type="text" class="form-control" name="status" id="status" readonly>
                                            </div>
                                           
                                            <div class="modal-footer">
                                                <input type="hidden" name="pool_id" id="pool_id">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            </div>
                                    </div>
                                  </form>
                                </div>
                            </div>
                        </div>
                <div class="row">
				<div class="col-md-12 col-lg-12">
                        <div class="card">
									<div class="card-status bg-azure br-tr-3 br-tl-3"></div>
									<div class="card-header">
										<h3 class="card-title">Record Pool</h3>
									</div>
									<div class="card-body">
										<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
											<div class="panel panel-default active">
												<div class="panel-heading " role="tab" id="headingOne">
													<h4 class="panel-title">
														<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true"  aria-controls="collapseOne">

															List Pool
														</a>
													</h4>
												</div>
												<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
													<div class="panel-body">
                                                        <div class="card-footer text-right">
                                                            <div class="card-body">
                                                                 <button type="button" class="btn btn-info" data-toggle="modal" data-target="#AddPool">Insert New Pool</button>

                                                            </div>
                                                        </div>
                                                       
														
                                            <div class="card-body">
                                                <div class="table-responsive">
                                                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                        <thead>
                                                            <tr>
                                                                <th class="wd-20p">Name Park</th>
                                                                <th class="wd-20p">Quantity</th>
                                                                <th class="wd-20p">Area (m²)</th>
                                                                <th class="wd-20p">Volume (m3)</th>
                                                                <th class="wd-20p">Status</th>
                                                                <th class="wd-20p">Action</th>

                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                             <?php
                                                             $counter = 1;
                                                               if($job == "admin"){

                                                                        $statement = $conn->prepare("SELECT * FROM pool");

                                                               }else {
                                                                    $statement = $conn->prepare("SELECT * FROM pool");

                                                               }
                                                                        $statement->execute();

                                                                       while($data = $statement->fetch(PDO::FETCH_ASSOC))
                                                                       {

                                                                       ?>
                                                                            <tr>
                                                                              <td><?php echo $data["type"]; ?></td>
                                                                              <td><?php echo $data["quantity"];?></td>
                                                                              <td><?php echo $data["area"];?></td>
                                                                              <td><?php echo $data["volume"];?></td>
                                                                              <td><?php echo $data["status"];?></td>
                                                                                 <td>
                                                                                    <button type="button" name="edit" id="edit" class="btn btn-info btn-xs"  data-toggle="modal" data-target="#ViewPool"
                                                                                    data-pool_id="<?php echo $data["pool_id"]; ?>"
                                                                                    data-type="<?php echo $data["type"];?>" 
                                                                                    data-quantity="<?php echo $data["quantity"];?>" 
                                                                                    data-area="<?php echo $data["area"];?>" 
                                                                                    data-volume="<?php echo $data["volume"];?>" 
                                                                                    data-status="<?php echo $data["status"]; ?>"
                                                                                    > <i class="fa fa-pencil"></i>&nbsp; View</button>
                                                                                     
                                                                                     
                                                                                    <button type="button" name="edit" id="edit" class="btn btn-success btn-xs"  data-toggle="modal" data-target="#EditPool"
                                                                                    data-pool_id="<?php echo $data["pool_id"]; ?>"
                                                                                    data-type="<?php echo $data["type"];?>" 
                                                                                    data-quantity="<?php echo $data["quantity"];?>" 
                                                                                    data-area="<?php echo $data["area"];?>" 
                                                                                    data-volume="<?php echo $data["volume"];?>" 
                                                                                    data-status="<?php echo $data["status"]; ?>"
                                                                                    > <i class="fa fa-pencil"></i>&nbsp;Edit </button>
                                                                                </td>
                                                                            </tr>
                                                                       <?php
                                                                           $counter++;
                                                                       }
                                                                       ?>

                                                        </tbody>
											</table>
										</div>
									</div>
													</div>
												</div>
											</div>
											<div class="panel panel-default mt-2">
												<div class="panel-heading" role="tab" id="headingTwo">
													<h4 class="panel-title">
														<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">

															All Record Complaint Pool
														</a>
													</h4>
												</div>
												<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
													<div class="panel-body">
														Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
													</div>
												</div>
											</div>
											<div class="panel panel-default mt-2">
												<div class="panel-heading" role="tab" id="headingThree">
													<h4 class="panel-title">
														<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">

															Collapsible Group Item #3
														</a>
													</h4>
												</div>
												<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
													<div class="panel-body">
														Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
													</div>
												</div>
											</div>
										</div><!-- panel-group -->
									</div>
								</div>
                    </div>
</div>

									<!-- table-wrapper -->
								</div>
								<!-- section-wrapper -->

							</div>
						</div>
					</div>
                    
                    <?php
    
                        include('footer.php');
                    ?>
