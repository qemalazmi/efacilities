<?php
  error_reporting(0);
	session_start();

	$tpg=43;

	$uid= $_POST['txtaffid'];
	$uemail= $_POST['txtaffmail'];
	$usubmitted= $_POST['submitted'];

	$ref=$_GET['ref'];

	if (!empty($ref))
	{
	$uid= $ref;
	$usubmitted= 1;
	}


	require_once('inc.config.php');
	require_once('inc.functions.php');
	require_once('inc.nufunctions.php');
	// require_once('xbackend/inc/inc.hdr.php');


	$Crypt = new MD5Crypt;

?>
<!doctype html>
<html lang="en" dir="ltr">

<!-- Mirrored from spruko.com/demo/ren/Blue-animated/LeftMenu/form-elements.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 20 Sep 2018 05:54:34 GMT -->
<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="msapplication-TileColor" content="#0061da">
		<meta name="theme-color" content="#1643a3">
		<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="mobile-web-app-capable" content="yes">
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<link rel="icon" href="favicon.ico" type="image/x-icon"/>
		<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />

		<!-- Title -->
		<title>Ren - a responsive, flat and full featured admin template</title>

        <!--Font Awesome-->
		<link href="assets/plugins/fontawesome-free/css/all.css" rel="stylesheet">

		<!-- Font Family -->
		<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500" rel="stylesheet">

		<!-- Dashboard Css -->
		<link href="assets/css/dashboard.css" rel="stylesheet" />

		<!-- Custom scroll bar css-->
		<link href="assets/plugins/scroll-bar/jquery.mCustomScrollbar.css" rel="stylesheet" />

		<!-- Sidemenu Css -->
		<link href="assets/plugins/toggle-sidebar/css/sidemenu.css" rel="stylesheet">

		<!-- c3.js Charts Plugin -->
		<link href="assets/plugins/charts-c3/c3-chart.css" rel="stylesheet" />

		<!-- select2 Plugin -->
		<link href="assets/plugins/select2/select2.min.css" rel="stylesheet" />

		<!-- Time picker Plugin -->
		<link href="assets/plugins/time-picker/jquery.timepicker.css" rel="stylesheet" />

		<!-- Date Picker Plugin -->
		<link href="assets/plugins/date-picker/spectrum.css" rel="stylesheet" />

		<!---Font icons-->
		<link href="assets/plugins/iconfonts/plugin.css" rel="stylesheet" />

	</head>
	<body class="app sidebar-mini rtl">
		<div id="global-loader" ><div class="showbox"><div class="lds-ring"><div></div><div></div><div></div><div></div></div></div></div>
		<div class="page">
			<div class="page-main">
				<!-- Navbar-->
				<header class="app-header header">

					<!-- Header Background Animation-->
					<div id="canvas" class="gradient"></div>

					<!-- Navbar Right Menu-->
					<div class="container-fluid">
						<div class="d-flex">
							<a class="header-brand" href="index-2.html">
								<img alt="ren logo" class="header-brand-img" src="assets/images/brand/logo.png">
							</a>
							<!-- Sidebar toggle button-->
							<a aria-label="Hide Sidebar" class="app-sidebar__toggle" data-toggle="sidebar" href="#"></a>
							<div class="d-flex order-lg-2 ml-auto">
								<div class="">
									<form class="input-icon  mr-2">
										<input class="form-control header-search" placeholder="Search&hellip;" tabindex="1" type="search">
										<div class="input-icon-addon">
											<i class="fe fe-search"></i>
										</div>
									</form>
								</div>
								<div class="dropdown d-none d-md-flex">
									<a class="nav-link icon" data-toggle="dropdown">
										<i class="fas fa-user"></i>
										<span class="nav-unread bg-green"></span>
									</a>
									<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
										<a class="dropdown-item d-flex pb-3" href="#">
											<span class="avatar brround mr-3 align-self-center" style="background-image: url(assets/images/faces/male/4.jpg)"></span>
											<div>
												<strong>Madeleine Scott</strong> Sent you add request
												<div class="small text-muted">
													view profile
												</div>
											</div>
										</a>
										<a class="dropdown-item d-flex pb-3" href="#">
											<span class="avatar brround mr-3 align-self-center" style="background-image: url(assets/images/faces/female/14.jpg)"></span>
											<div>
												<strong>rebica</strong> Suggestions for you
												<div class="small text-muted">
													view profile
												</div>
											</div>
										</a>
										<a class="dropdown-item d-flex pb-3" href="#">
											<span class="avatar brround mr-3 align-self-center" style="background-image: url(assets/images/faces/male/1.jpg)"></span>
											<div>
												<strong>Devid robott</strong> sent you add request
												<div class="small text-muted">
													view profile
												</div>
											</div>
										</a>
										<div class="dropdown-divider"></div><a class="dropdown-item text-center text-muted-dark" href="#">View all contact list</a>
									</div>
								</div>
								<div class="dropdown d-none d-md-flex">
									<a class="nav-link icon" data-toggle="dropdown">
										<i class="fas fa-bell"></i>
										<span class="nav-unread bg-danger"></span>
									</a>
									<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
										<a class="dropdown-item d-flex pb-3" href="#">
											<div class="notifyimg">
												<i class="fas fa-thumbs-up"></i>
											</div>
											<div>
												<strong>Someone likes our posts.</strong>
												<div class="small text-muted">
													3 hours ago
												</div>
											</div>
										</a>
										<a class="dropdown-item d-flex pb-3" href="#">
											<div class="notifyimg">
												<i class="fas fa-comment-alt"></i>
											</div>
											<div>
												<strong>3 New Comments</strong>
												<div class="small text-muted">
													5 hour ago
												</div>
											</div>
										</a>
										<a class="dropdown-item d-flex pb-3" href="#">
											<div class="notifyimg">
												<i class="fas fa-cogs"></i>
											</div>
											<div>
												<strong>Server Rebooted.</strong>
												<div class="small text-muted">
													45 mintues ago
												</div>
											</div>
										</a>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item text-center text-muted-dark" href="#">View all Notification</a>
									</div>
								</div>
								<div class="dropdown d-none d-md-flex">
									<a class="nav-link icon" data-toggle="dropdown"><i class="fas fa-envelope"></i> <span class="badge badge-info badge-pill">2</span></a>
									<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
										<a class="dropdown-item text-center text-dark" href="#">2 New Messages</a>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item d-flex pb-3" href="#">
											<span class="avatar brround mr-3 align-self-center" style="background-image: url(assets/images/faces/male/41.jpg)"></span>
											<div>
												<strong>Madeleine</strong> Hey! there I' am available....
												<div class="small text-muted">
													3 hours ago
												</div>
											</div>
										</a>
										<a class="dropdown-item d-flex pb-3" href="#"><span class="avatar brround mr-3 align-self-center" style="background-image: url(assets/images/faces/female/1.jpg)"></span>
											<div>
												<strong>Anthony</strong> New product Launching...
												<div class="small text-muted">
													5 hour ago
												</div>
											</div>
										</a>
										<a class="dropdown-item d-flex pb-3" href="#"><span class="avatar brround mr-3 align-self-center" style="background-image: url(assets/images/faces/female/18.jpg)"></span>
											<div>
												<strong>Olivia</strong> New Schedule Realease......
												<div class="small text-muted">
													45 mintues ago
												</div>
											</div>
										</a>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item text-center text-muted-dark" href="#">See all Messages</a>
									</div>
								</div>
								<div class="dropdown">
									<a class="nav-link pr-0 leading-none d-flex" data-toggle="dropdown" href="#">
										<span class="avatar avatar-md brround" style="background-image: url(assets/images/faces/female/25.jpg)"></span>
										<span class="ml-2 d-none d-lg-block">
											<span class="text-white">Sindy Scribner</span>
										</span>
									</a>
									<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
										<a class="dropdown-item" href="#"><i class="dropdown-icon mdi mdi-account-outline"></i> Profile</a>
										<a class="dropdown-item" href="#"><i class="dropdown-icon mdi mdi-settings"></i> Settings</a>
										<a class="dropdown-item" href="#"><span class="float-right"><span class="badge badge-primary">6</span></span> <i class="dropdown-icon mdi mdi-message-outline"></i> Inbox</a>
										<a class="dropdown-item" href="#"><i class="dropdown-icon mdi mdi-comment-check-outline"></i> Message</a>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item" href="#"><i class="dropdown-icon mdi mdi-compass-outline"></i> Need help?</a>
										<a class="dropdown-item" href="login.html"><i class="dropdown-icon mdi mdi-logout-variant"></i> Sign out</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</header>

								<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
				<aside class="app-sidebar">
					<div class="app-sidebar__user">
						<div class="dropdown">
							<a class="nav-link p-0 leading-none d-flex" data-toggle="dropdown" href="#">
								<span class="avatar avatar-md brround" style="background-image: url(assets/images/faces/female/25.jpg)"></span>
								<span class="ml-2 "><span class="text-white app-sidebar__user-name font-weight-semibold">Sindy Scribner</span><br>
									<span class="text-muted app-sidebar__user-name text-sm"> Ui Designer</span>
								</span>
							</a>
							<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
								<a class="dropdown-item" href="#"><i class="dropdown-icon mdi mdi-account-outline"></i> Profile</a>
								<a class="dropdown-item" href="#"><i class="dropdown-icon mdi mdi-settings"></i> Settings</a>
								<a class="dropdown-item" href="#"><span class="float-right"><span class="badge badge-primary">6</span></span> <i class="dropdown-icon mdi mdi-message-outline"></i> Inbox</a>
								<a class="dropdown-item" href="#"><i class="dropdown-icon mdi mdi-comment-check-outline"></i> Message</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="#"><i class="dropdown-icon mdi mdi-compass-outline"></i> Need help?</a>
								<a class="dropdown-item" href="login.html"><i class="dropdown-icon mdi mdi-logout-variant"></i> Sign out</a>
							</div>
						</div>
					</div>
					<ul class="side-menu">
            <li>
							<a class="side-menu__item" href="index-2.html"><i class="side-menu__icon fas fa-home"></i><span class="side-menu__label">Dashboard</span></a>
						</li>
						<li>
							<a class="side-menu__item" href="cust_support.php"><i class="side-menu__icon fas fa-window-restore"></i><span class="side-menu__label">Admin Support</span></a>
						</li>
            <li class="slide">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fas fa-newspaper"></i><span class="side-menu__label">Affiliate</span><i class="angle fas fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="affl_check.php" class="slide-item">Affiliate Search</a>
								</li>
								<li>
									<a href="affl_sum.php" class="slide-item">Affiliate List</a>
								</li>
								<li>
									<a href="affl_acct.php" class="slide-item">Affiliate Accounting on</a>
								</li>
								<li>
									<a href="affl_send_logs.php" class="slide-item">Affiliate ROBOT SEND LOGS</a>
								</li>
							</ul>
						</li>
							</ul>
						</li>
							</ul>
						</li>

					</ul>
				</aside>

				<div class="my-3 my-md-5 app-content">
					<div class="side-app">
						<div class="page-header">
							<h4 class="page-title">Affiliate Search</h4>
							<ol class="breadcrumb">

                <li class="breadcrumb-item"><a href="aff_sum.php">Home</a></li>

								<li class="breadcrumb-item active" aria-current="page">Affiliate Search</li>
							</ol>
						</div>

          <div class="col-lg-12">
            <form  method="post" class="card">
              <div class="card-header">
                <h3 class="card-title">Search For</h3>
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-md-6 col-lg-6">
                    <div class="form-group" >
                      <form name="fnamesearch" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
                      <label class="form-label">Username</label>
                      <div class="row gutters-xs">
                        <div class="col">
                          <input type="text" class="form-control" placeholder="Search for..." value="<?php echo $uid; ?>">
                        </div>
                        <span class="col-auto">
                          <button class="btn btn-primary" type="button"><i class="fe fe-search"></i></button>
                        </span></form>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-6 col-lg-6">
                    <div class="form-group">
                      <form name="fnamesearch" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
                      <label class="form-label">Email</label>
                      <div class="row gutters-xs">
                        <div class="col">
                          <input type="text" class="form-control" placeholder="Search for..." value="<?php echo $uemail; ?>">
                        </div>
                        <span class="col-auto">
                          <button class="btn btn-primary" type="button"><i class="fe fe-search"></i></button>
                        </span></form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>


          <div class="col-lg-12">
            <div class="card">
              <div class="card-header">
                <div class="card-title">Affiliate Info </div>
              </div>
              <div class="card-body">
                <p class="col-lg-12"></p>

              </div>
            </div>
          </div>

          <div class="col-lg-12">
            <div class="card">
              <div class="card-header">
                <div class="card-title">Sales </div>
              </div>
              <div class="card-body">
                <div class="col-lg-12"></div>

              </div>
            </div>
          </div>
					<!--footer-->
					<footer class="footer">
						<div class="container">
							<div class="row align-items-center flex-row-reverse">
								<div class="col-lg-12 col-sm-12 mt-3 mt-lg-0 text-center">
									Copyright © 2018 <a href="#">Ren</a>. Designed by <a href="#">Spruko</a> All rights reserved.
								</div>
							</div>
						</div>
					</footer>
					<!-- End Footer-->
				</div>
			</div>
		</div>

		<!-- Back to top -->
		<a href="#top" id="back-to-top" style="display: inline;"><i class="fas fa-angle-up"></i></a>
		<!-- Dashboard Css -->
		<script src="assets/js/vendors/jquery-3.2.1.min.js"></script>
		<script src="assets/js/vendors/bootstrap.bundle.min.js"></script>
		<script src="assets/js/vendors/jquery.sparkline.min.js"></script>
		<script src="assets/js/vendors/selectize.min.js"></script>
		<script src="assets/js/vendors/jquery.tablesorter.min.js"></script>
		<script src="assets/js/vendors/circle-progress.min.js"></script>
		<script src="assets/plugins/rating/jquery.rating-stars.js"></script>
		<!-- Side menu js -->
		<script src="assets/plugins/toggle-sidebar/js/sidemenu.js"></script>

		<!-- Custom scroll bar Js-->
		<script src="assets/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js"></script>

		<!--Select2 js -->
		<script src="assets/plugins/select2/select2.full.min.js"></script>

		<!-- Timepicker js -->
		<script src="assets/plugins/time-picker/jquery.timepicker.js"></script>
		<script src="assets/plugins/time-picker/toggles.min.js"></script>

		<!-- Datepicker js -->
		<script src="assets/plugins/date-picker/spectrum.js"></script>
		<script src="assets/plugins/date-picker/jquery-ui.js"></script>
		<script src="assets/plugins/input-mask/jquery.maskedinput.js"></script>

		<!-- 3Dlines-animation -->
        <script src="assets/plugins/3Dlines-animation/three.min.js"></script>
        <script src="assets/plugins/3Dlines-animation/projector.js"></script>
        <script src="assets/plugins/3Dlines-animation/canvas-renderer.js"></script>
        <script src="assets/plugins/3Dlines-animation/3d-lines-animation.js"></script>
        <script src="assets/plugins/3Dlines-animation/color.js"></script>

		<!-- Inline js -->
		<script src="assets/js/select2.js"></script>

		<!--Counters -->
		<script src="assets/plugins/counters/counterup.min.js"></script>
		<script src="assets/plugins/counters/waypoints.min.js"></script>

		<!-- Custom js -->
		<script src="assets/js/custom.js"></script>

	</body>

<!-- Mirrored from spruko.com/demo/ren/Blue-animated/LeftMenu/form-elements.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 20 Sep 2018 05:54:43 GMT -->
</html>
