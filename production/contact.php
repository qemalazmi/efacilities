<?php
	include 'inc.hdr.php';
?>
<h1 class="highlight txt_center">Hubungi Saya</h1>

<p>Hubungi saya untuk sebarang pertanyaan dengan menggunakan sistem tiket bantuan di bawah. Ini untuk memudahkan saya melayan sebarang pertanyaan secara teratur dan memastikan setiap pertanyaan dapat dijawab dengan cepat.</p>

<p>Perhatian yang lebih akan diberi kepada kes-kes yang melibatkan kehilangan kata-laluan atau password, tidak menerima email daripada kami dan lain-lain lagi.</p>

<p>Bagi mereka yang telah melakukan deposit melalui Maybank, sila periksa semula email yang anda telah terima dan ikuti arahan sepenuhnya. Sila periksa spam folder untuk memastikan e-mail saya mungkin termasuk sana kadang-kadang.</p>

<p>Bagi yang sudah membeli, pastikan anda masukkan email asal anda semasa membuat pembelian. Saya akan berikan lebih perhatian kepada masalah-masalah anda.</p>
<!--
<p>SPAM RELATED COMPLAINTS :</p>

<p>FOR ANY SPAM COMPLAINTS, PLEASE USE THE BELOW FORM AND PROVIDE PROOF OF SUCH A VIOLATION OF MY NO SPAM POLICY. I WILL START TO INVESTIGATE YOUR CLAIMS AS SOON AS POSSIBLE.</p>-->

<br>
<p align="center"><a href="web_contact.php" class="btn" style="text-decoration:none;"><strong>Klik Sini Untuk Hubungi Saya</strong></a></p>

<br><br>


<?php include 'inc.ftr.php';?>
