<?php
  session_start();

  require_once('inc.config.php');
	require_once('inc.functions.php');
	require_once('inc.nufunctions.php');
	// require_once('xbackend/inc/inc.hdr.php');
?>
<!doctype html>
<html lang="en" dir="ltr">

<!-- Mirrored from spruko.com/demo/ren/Blue-animated/LeftMenu/tables.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 20 Sep 2018 05:53:41 GMT -->
<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<meta name="msapplication-TileColor" content="#0061da">
		<meta name="theme-color" content="#1643a3">
		<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="mobile-web-app-capable" content="yes">
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<link rel="icon" href="favicon.ico" type="image/x-icon"/>
		<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />

		<!-- Title -->
		<title>Ren - a responsive, flat and full featured admin template</title>

        <!--Font Awesome-->
		<link href="assets/plugins/fontawesome-free/css/all.css" rel="stylesheet">

		<!-- Font Family -->
		<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500" rel="stylesheet">

		<!-- Dashboard Css -->
		<link href="assets/css/dashboard.css" rel="stylesheet" />

		<!-- Custom scroll bar css-->
		<link href="assets/plugins/scroll-bar/jquery.mCustomScrollbar.css" rel="stylesheet" />

		<!-- Sidemenu Css -->
		<link href="assets/plugins/toggle-sidebar/css/sidemenu.css" rel="stylesheet">

		<!-- c3.js Charts Plugin -->
		<link href="assets/plugins/charts-c3/c3-chart.css" rel="stylesheet" />

		<!---Font icons-->
		<link href="assets/plugins/iconfonts/plugin.css" rel="stylesheet" />


	</head>

	<body class="app sidebar-mini rtl">
		<div id="global-loader" ><div class="showbox"><div class="lds-ring"><div></div><div></div><div></div><div></div></div></div></div>
		<div class="page">
			<div class="page-main">
				<!-- Navbar-->
				<header class="app-header header">

					<!-- Header Background Animation-->
					<div id="canvas" class="gradient"></div>

					<!-- Navbar Right Menu-->
					<div class="container-fluid">
						<div class="d-flex">
							<a class="header-brand" href="index-2.html">
								<img alt="ren logo" class="header-brand-img" src="assets/images/brand/logo.png">
							</a>
							<a aria-label="Hide Sidebar" class="app-sidebar__toggle" data-toggle="sidebar" href="#"></a>
							<div class="d-flex order-lg-2 ml-auto">
								<div class="">
									<form class="input-icon  mr-2">
										<input class="form-control header-search" placeholder="Search&hellip;" tabindex="1" type="search">
										<div class="input-icon-addon">
											<i class="fe fe-search"></i>
										</div>
									</form>
								</div>
								<div class="dropdown d-none d-md-flex">
									<a class="nav-link icon" data-toggle="dropdown">
										<i class="fas fa-user"></i>
										<span class="nav-unread bg-green"></span>
									</a>
									<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
										<a class="dropdown-item d-flex pb-3" href="#">
											<span class="avatar brround mr-3 align-self-center" style="background-image: url(assets/images/faces/male/4.jpg)"></span>
											<div>
												<strong>Madeleine Scott</strong> Sent you add request
												<div class="small text-muted">
													view profile
												</div>
											</div>
										</a>
										<a class="dropdown-item d-flex pb-3" href="#">
											<span class="avatar brround mr-3 align-self-center" style="background-image: url(assets/images/faces/female/14.jpg)"></span>
											<div>
												<strong>rebica</strong> Suggestions for you
												<div class="small text-muted">
													view profile
												</div>
											</div>
										</a>
										<a class="dropdown-item d-flex pb-3" href="#">
											<span class="avatar brround mr-3 align-self-center" style="background-image: url(assets/images/faces/male/1.jpg)"></span>
											<div>
												<strong>Devid robott</strong> sent you add request
												<div class="small text-muted">
													view profile
												</div>
											</div>
										</a>
										<div class="dropdown-divider"></div><a class="dropdown-item text-center text-muted-dark" href="#">View all contact list</a>
									</div>
								</div>
								<div class="dropdown d-none d-md-flex">
									<a class="nav-link icon" data-toggle="dropdown">
										<i class="fas fa-bell"></i>
										<span class="nav-unread bg-danger"></span>
									</a>
									<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
										<a class="dropdown-item d-flex pb-3" href="#">
											<div class="notifyimg">
												<i class="fas fa-thumbs-up"></i>
											</div>
											<div>
												<strong>Someone likes our posts.</strong>
												<div class="small text-muted">
													3 hours ago
												</div>
											</div>
										</a>
										<a class="dropdown-item d-flex pb-3" href="#">
											<div class="notifyimg">
												<i class="fas fa-comment-alt"></i>
											</div>
											<div>
												<strong>3 New Comments</strong>
												<div class="small text-muted">
													5 hour ago
												</div>
											</div>
										</a>
										<a class="dropdown-item d-flex pb-3" href="#">
											<div class="notifyimg">
												<i class="fas fa-cogs"></i>
											</div>
											<div>
												<strong>Server Rebooted.</strong>
												<div class="small text-muted">
													45 mintues ago
												</div>
											</div>
										</a>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item text-center text-muted-dark" href="#">View all Notification</a>
									</div>
								</div>
								<div class="dropdown d-none d-md-flex">
									<a class="nav-link icon" data-toggle="dropdown"><i class="fas fa-envelope"></i> <span class="badge badge-info badge-pill">2</span></a>
									<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
										<a class="dropdown-item text-center text-dark" href="#">2 New Messages</a>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item d-flex pb-3" href="#">
											<span class="avatar brround mr-3 align-self-center" style="background-image: url(assets/images/faces/male/41.jpg)"></span>
											<div>
												<strong>Madeleine</strong> Hey! there I' am available....
												<div class="small text-muted">
													3 hours ago
												</div>
											</div>
										</a>
										<a class="dropdown-item d-flex pb-3" href="#"><span class="avatar brround mr-3 align-self-center" style="background-image: url(assets/images/faces/female/1.jpg)"></span>
											<div>
												<strong>Anthony</strong> New product Launching...
												<div class="small text-muted">
													5 hour ago
												</div>
											</div>
										</a>
										<a class="dropdown-item d-flex pb-3" href="#"><span class="avatar brround mr-3 align-self-center" style="background-image: url(assets/images/faces/female/18.jpg)"></span>
											<div>
												<strong>Olivia</strong> New Schedule Realease......
												<div class="small text-muted">
													45 mintues ago
												</div>
											</div>
										</a>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item text-center text-muted-dark" href="#">See all Messages</a>
									</div>
								</div>
								<div class="dropdown">
									<a class="nav-link pr-0 leading-none d-flex" data-toggle="dropdown" href="#">
										<span class="avatar avatar-md brround" style="background-image: url(assets/images/faces/female/25.jpg)"></span>
										<span class="ml-2 d-none d-lg-block">
											<span class="text-white">Sindy Scribner</span>
										</span>
									</a>
									<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
										<a class="dropdown-item" href="#"><i class="dropdown-icon mdi mdi-account-outline"></i> Profile</a>
										<a class="dropdown-item" href="#"><i class="dropdown-icon mdi mdi-settings"></i> Settings</a>
										<a class="dropdown-item" href="#"><span class="float-right"><span class="badge badge-primary">6</span></span> <i class="dropdown-icon mdi mdi-message-outline"></i> Inbox</a>
										<a class="dropdown-item" href="#"><i class="dropdown-icon mdi mdi-comment-check-outline"></i> Message</a>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item" href="#"><i class="dropdown-icon mdi mdi-compass-outline"></i> Need help?</a>
										<a class="dropdown-item" href="login.html"><i class="dropdown-icon mdi mdi-logout-variant"></i> Sign out</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</header>

				<!-- Sidebar menu-->
				<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
				<aside class="app-sidebar">
					<div class="app-sidebar__user">
						<div class="dropdown">
							<a class="nav-link p-0 leading-none d-flex" data-toggle="dropdown" href="#">
								<span class="avatar avatar-md brround" style="background-image: url(assets/images/faces/female/25.jpg)"></span>
								<span class="ml-2 "><span class="text-white app-sidebar__user-name font-weight-semibold">Sindy Scribner</span><br>
									<span class="text-muted app-sidebar__user-name text-sm"> Ui Designer</span>
								</span>
							</a>
							<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
								<a class="dropdown-item" href="#"><i class="dropdown-icon mdi mdi-account-outline"></i> Profile</a>
								<a class="dropdown-item" href="#"><i class="dropdown-icon mdi mdi-settings"></i> Settings</a>
								<a class="dropdown-item" href="#"><span class="float-right"><span class="badge badge-primary">6</span></span> <i class="dropdown-icon mdi mdi-message-outline"></i> Inbox</a>
								<a class="dropdown-item" href="#"><i class="dropdown-icon mdi mdi-comment-check-outline"></i> Message</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="#"><i class="dropdown-icon mdi mdi-compass-outline"></i> Need help?</a>
								<a class="dropdown-item" href="login.html"><i class="dropdown-icon mdi mdi-logout-variant"></i> Sign out</a>
							</div>
						</div>
					</div>
					<ul class="side-menu">
						<!-- <li class="slide">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fas fa-home"></i><span class="side-menu__label">Dashboard</span><i class="angle fas fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li><a class="slide-item" href="index-2.html">Home 1</a></li>
								<li><a class="slide-item" href="index2.html">Home 2</a></li>
								<li><a class="slide-item" href="index3.html">Home 3</a></li>
								<li><a class="slide-item" href="index4.html">Home 4</a></li>
							</ul>
						</li> -->
						<li>
							<a class="side-menu__item" href="index-2.html"><i class="side-menu__icon fas fa-home"></i><span class="side-menu__label">Dashboard</span></a>
						</li>
						<li>
							<a class="side-menu__item" href="cust_support.php"><i class="side-menu__icon fas fa-window-restore"></i><span class="side-menu__label">Admin Support</span></a>
						</li>
            <li class="slide">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fas fa-newspaper"></i><span class="side-menu__label">Affiliate</span><i class="angle fas fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="affl_check.php" class="slide-item">Affiliate Search</a>
								</li>
								<li>
									<a href="affl_sum.php" class="slide-item">Affiliate List</a>
								</li>
								<li>
									<a href="affl_acct.php" class="slide-item">Affiliate Accounting on</a>
								</li>
								<li>
									<a href="affl_send_logs.php" class="slide-item">Affiliate ROBOT SEND LOGS</a>
								</li>
							</ul>
						</li>
						<!-- <li class="slide">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fas fa-chart-bar"></i><span class="side-menu__label">Chart Types</span><i class="angle fas fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="chart-flot.html" class="slide-item">Chart flot</a>
								</li>
								<li>
									<a href="chartjs.html" class="slide-item">Chart js</a>
								</li>
								<li>
									<a href="charts-peity.html" class="slide-item">Charts Peity</a>
								</li>
								<li>
									<a href="chart-morris.html" class="slide-item">Chart Morris</a>
								</li>
								<li>
									<a href="chart-ricksaw.html" class="slide-item">Chart Ricksaw</a>
								</li>
								<li>
									<a href="chart-chartist.html" class="slide-item">Chart Chartist</a>
								</li>
								<li>
									<a href="charts.html" class="slide-item">Bar Charts</a>
								</li>
								<li>
									<a href="chart-line.html" class="slide-item">Line Charts</a>
								</li>
								<li>
									<a href="chart-donut.html" class="slide-item">Donut Charts</a>
								</li>
								<li>
									<a href="chart-pie.html" class="slide-item">Pie charts</a>
								</li>
							</ul>
						</li> -->
						<!-- <li>
							<a class="side-menu__item" href="maps.html"><i class="side-menu__icon fas fa-map-marker"></i><span class="side-menu__label">Maps</span></a>
						</li>
						<li>
							<a class="side-menu__item" href="gallery.html"><i class="side-menu__icon fas fa-images"></i><span class="side-menu__label">Gallery</span></a>
						</li>
						<li class="slide">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fas fa-calendar"></i><span class="side-menu__label">Calendar</span><i class="angle fas fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="calendar.html" class="slide-item">Default calendar</a>
								</li>
								<li>
									<a href="calendar2.html" class="slide-item">Full calendar</a>
								</li>
							</ul>
						</li> -->
						<!-- <li class="slide">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fas fa-snowflake"></i><span class="side-menu__label">Ui Design</span><i class="angle fas fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="cards.html" class="slide-item">Cards design</a>
								</li>
								<li>
									<a href="chat.html" class="slide-item">Default Chat</a>
								</li>
								<li>
									<a href="notify.html" class="slide-item">Notifications</a>
								</li>
								<li>
									<a href="sweetalert.html" class="slide-item">Sweet alerts</a>
								</li>
								<li>
									<a href="rangeslider.html" class="slide-item">Range slider</a>
								</li>
								<li>
									<a href="scroll.html" class="slide-item">Content Scroll bar</a>
								</li>
								<li>
									<a href="counters.html" class="slide-item">Counters</a>
								</li>
								<li>
									<a href="loaders.html" class="slide-item">Loaders</a>
								</li>
								<li>
									<a href="rating.html" class="slide-item">Rating</a>
								</li>
								<li>
									<a href="time-line.html" class="slide-item">Time Line</a>
								</li>
							</ul>
						</li> -->

						<!-- <li class="slide">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fas fa-newspaper"></i><span class="side-menu__label">Pages</span><i class="angle fas fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="profile.html" class="slide-item">Profile</a>
								</li>
								<li>
									<a href="editprofile.html" class="slide-item">Edit Profile</a>
								</li>
								<li>
									<a href="email.html" class="slide-item">Email</a>
								</li>
								<li>
									<a href="emailservices.html" class="slide-item">Email Inbox</a>
								</li>
								<li>
									<a href="login.html" class="slide-item">Login</a>
								</li>
								<li>
									<a href="register.html" class="slide-item">Register</a>
								</li>
								<li>
									<a href="forgot-password.html" class="slide-item">Forgot password</a>
								</li>
								<li>
									<a href="lockscreen.html" class="slide-item">Lock screen</a>
								</li>
								<li>
									<a href="empty.html" class="slide-item">Empty Page</a>
								</li>
								<li>
									<a href="construction.html" class="slide-item">Under Construction</a>
								</li>
								<li>
									<a href="about.html" class="slide-item">About Company</a>
								</li>
								<li>
									<a href="company-history.html" class="slide-item">Company History</a>
								</li>
								<li>
									<a href="services.html" class="slide-item">Services</a>
								</li>
								<li>
									<a href="faq.html" class="slide-item">FAQS</a>
								</li>
								<li>
									<a href="terms.html" class="slide-item">Terms and Conditions</a>
								</li>
								<li>
									<a href="pricing.html" class="slide-item">Pricing Tables</a>
								</li>
								<li>
									<a href="blog.html" class="slide-item">Blog</a>
								</li>
								<li>
									<a href="invoice.html" class="slide-item">Invoice</a>
								</li>
							</ul>
						</li> -->
						<!-- <li class="slide">
							<a class="side-menu__item active" data-toggle="slide" href="#"><i class="side-menu__icon fas fa-table"></i><span class="side-menu__label">Tables</span><i class="angle fas fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="tables.html" class="slide-item">Default table</a>
								</li>
								<li>
									<a href="datatable.html" class="slide-item">Data Table</a>
								</li>
							</ul>
						</li> -->
						<!-- <li class="slide">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fas fa-shopping-cart"></i><span class="side-menu__label">E-commerce</span><i class="angle fas fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="shop.html" class="slide-item">Products</a>
								</li>
								<li>
									<a href="shop-des.html" class="slide-item">Product Details</a>
								</li>
								<li>
									<a href="cart.html" class="slide-item">product-Search page</a>
								</li>
							</ul>
						</li> -->
						<!-- <li class="slide">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fas fa-adjust"></i><span class="side-menu__label">Icons</span><i class="angle fas fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="icons.html" class="slide-item">Font Awesome</a>
								</li>
								<li>
									<a href="icons2.html" class="slide-item">Material Design Icons</a>
								</li>
								<li>
									<a href="icons3.html" class="slide-item">Simple Line Iocns</a>
								</li>
								<li>
									<a href="icons4.html" class="slide-item">Feather Icons</a>
								</li>
								<li>
									<a href="icons5.html" class="slide-item">Ionic Icons</a>
								</li>
								<li>
									<a href="icons6.html" class="slide-item">Flags Icons</a>
								</li>
							</ul>
						</li>
						<li class="slide">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fas fa-file-alt"></i><span class="side-menu__label">Forms</span><i class="angle fas fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="form-elements.html" class="slide-item">Form Elements</a>
								</li>
								<li>
									<a href="form-wizard.html" class="slide-item">Form-wizard Elements</a>
								</li>
								<li>
									<a href="wysiwyag.html" class="slide-item">Text Editor</a>
								</li>
							</ul>
						</li>
						<li class="slide">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fas fa-cubes"></i><span class="side-menu__label">Components</span><i class="angle fas fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="modal.html" class="slide-item">Modal</a>
								</li>
								<li>
									<a href="tooltipandpopover.html" class="slide-item">Tooltip and popover</a>
								</li>
								<li>
									<a href="progress.html" class="slide-item">Progress</a>
								</li>
								<li>
									<a href="chart.html" class="slide-item">Charts</a>
								</li>
								<li>
									<a href="carousel.html" class="slide-item">Carousels</a>
								</li>
								<li>
									<a href="accordion.html" class="slide-item">Accordions</a>
								</li>
								<li>
									<a href="tabs.html" class="slide-item">Tabs</a>
								</li>
								<li>
									<a href="headers.html" class="slide-item">Headers</a>
								</li>
								<li>
									<a href="footers.html" class="slide-item">Footers</a>
								</li>
								<li>
									<a href="crypto-currencies.html" class="slide-item">Crypto-currencies</a>
								</li>
								<li>
									<a href="users-list.html" class="slide-item">User List</a>
								</li>
								<li>
									<a href="search.html" class="slide-item">Search page</a>
								</li>
							</ul>
						</li>
						<li class="slide">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fas fa-pen-square"></i><span class="side-menu__label">Basic Elements</span><i class="angle fas fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="alerts.html" class="slide-item">Alerts</a>
								</li>
								<li>
									<a href="buttons.html" class="slide-item">Buttons</a>
								</li>
								<li>
									<a href="colors.html" class="slide-item">Colors</a>
								</li>
								<li>
									<a href="avatars.html" class="slide-item">Avatar-Square</a>
								</li>
								<li>
									<a href="avatar-round.html" class="slide-item">Avatar-Rounded</a>
								</li>
								<li>
									<a href="avatar-radius.html" class="slide-item">Avatar-Radius</a>
								</li>
								<li>
									<a href="dropdown.html" class="slide-item">Drop downs</a>
								</li>
								<li>
									<a href="thumbnails.html" class="slide-item">Thumbnails</a>
								</li>
								<li>
									<a href="mediaobject.html" class="slide-item">Media Object</a>
								</li>
								<li>
									<a href="list.html" class="slide-item">List</a>
								</li>
								<li>
									<a href="tags.html" class="slide-item">Tags</a>
								</li>
								<li>
									<a href="pagination.html" class="slide-item">Pagination</a>
								</li>
								<li>
									<a href="navigation.html" class="slide-item">Navigation</a>
								</li>
								<li>
									<a href="typography.html" class="slide-item">Typography</a>
								</li>
								<li>
									<a href="breadcrumbs.html" class="slide-item">Breadcrumbs</a>
								</li>
								<li>
									<a href="badge.html" class="slide-item">Badges</a>
								</li>
								<li>
									<a href="jumbotron.html" class="slide-item">Jumbotron</a>
								</li>
								<li>
									<a href="panels.html" class="slide-item">Panels</a>
								</li>
							</ul>
						</li>

						<li class="slide">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fas fa-exclamation-circle"></i><span class="side-menu__label">Error pages</span><i class="angle fas fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="400.html" class="slide-item">400 Error</a>
								</li>
								<li>
									<a href="401.html" class="slide-item">401 Error</a>
								</li>
								<li>
									<a href="403.html" class="slide-item">403 Error</a>
								</li>
								<li>
									<a href="404.html" class="slide-item">404 Error</a>
								</li>
								<li>
									<a href="500.html" class="slide-item">500 Error</a>
								</li>
								<li>
									<a href="503.html" class="slide-item">503 Error</a>
								</li>
							</ul>
						</li>
						<li>
							<a class="side-menu__item" href="https://themeforest.net/user/sprukotechnologies/portfolio"><i class="side-menu__icon fas fa-question-circle"></i><span class="side-menu__label">Help & Support</span></a>
						</li> -->
					</ul>
				</aside>
				<?php
				$php_remote_user= $_SERVER['REMOTE_USER'];
				echo "Logged in : ".$php_remote_user."<br>";

				// if aidi
				switch ($php_remote_user) {
				    case admin_phyton:
				        $show_category="AND category=1";  // TECHNICAL HELPDESK
				        break;
				    case admin_ami:
				        $show_category="AND category=2";  // NON TECHNICAL HELPDESK
				        break;
				    case admin_technical:
				        $show_category="AND category=3";  // ONLY SHOW ESCALATED
				        break;
				     // Show all categories
				    case admin_lion:					  // SHOW ALL
				        $show_category="";
				        break;
				}

				mysql_connect($db_host, $db_user, $db_pass)
			    or die ("Database CONNECT Error");


				//new search by email
				if (isset($_POST[submit_search]))

					{

						$search_q="SELECT * FROM messages WHERE email='".$_POST["find_email"]."' order by id DESC limit 0,1";
						//echo $search_q;
						$search_result = mysql_db_query($db, $search_q)
									or die ("Database INSERT Error");

						 if (mysql_num_rows($search_result) > 0)
									{
										while ($search_qry = mysql_fetch_array($search_result))
											{
												$mytarget="cust_support_reply.php?email=".urlencode($search_qry[email])."&uid=".$search_qry[id];
												aff_redirect($mytarget,0);
											}
									}
					}
				  //end new search by email

					$action= $_GET['action'];
					$uid= $_GET['uid'];

					if ($action!="")
					{

						if ($action=="reply") $ustat=1;
						if ($action=="hold") $ustat=2;
						if ($action=="thrash") $ustat=9;
						if ($action=="escalate")
						{
						//assign to admin
						//change to unread


						//check if escalation from non-technical (ami) to technical (id)

						$ustat=0;
						// Escalate from Non Tech to Tech - Tech to Admin
						if ($php_remote_user=="admin_ami")
							{
								$mycategory="1";
							}else{
								$mycategory="3";
							}



						$sq="UPDATE messages SET is_read=".$ustat.", category=".$mycategory." WHERE id=".$uid." LIMIT 1";
						//echo $sq;


						}else{



						$sq="UPDATE messages SET is_read=".$ustat." WHERE id=".$uid." LIMIT 1";
						}

						//echo $sq;
						$sresult = mysql_db_query($db, $sq)
							or die ("Database UPDATE Error");
					}

					$q="select * from messages WHERE datetime <= (NOW() - INTERVAL 4 HOUR) AND (is_read=0 OR is_read=2) ".$show_category." ORDER BY datetime ASC";





				  $result = mysql_db_query($db, $q)
				    or die ("Database INSERT Error");


				  print mysql_num_rows($result);
				?>


				<!-- <center>
				<form method=post action=<?php echo $_SERVER[PHP_SELF];?>
				Find messages by Email : <input type=text name=find_email value=>
				<input type=submit name=submit_search value=OK>
				</form>
				</center> -->

				<div class="app-content my-3 my-md-5">
					<div class="side-app">
						<div class="page-header">
							<h4 class="page-title">Admin Support</h4>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="index-2.html">Home</a></li>
								<li class="breadcrumb-item active" aria-current="page">Admin Support</li>
							</ol>
						</div>
						<div class="row">
							<div class="col-md-12 col-lg-12">
								<div class="card">
									<div class="card-header">
										<h3 class="card-title">Admin Support</h3>
									</div>
									<div class="table-responsive">
										<table class="table card-table table-vcenter text-nowrap">
											<thead >
												<tr>
													<th  style="width:40%">User Detail</th>
													<th style="width:20%">Message</th>
													<th  style="width:40%">Action</th>
												</tr>
												</thead>
												<?php

												  if (mysql_num_rows($result))
												  {
													 $cnt=0;
													 $tr_col="#EBEBEB";

												    while ($qry = mysql_fetch_array($result))
												    {
													  if ($qry["is_read"]==0 || $qry["is_read"]==2)
													  {
															$cnt=$cnt+1;
															if ($cnt%2==0)
															{$tr_col="#EBEBEB";
															} else {
															$tr_col="#FFFFFF";
															}
															print "<tr valign=top bgcolor=".$tr_col.">";
															print "<td valign=top><span class=\"style3\">";
															print $qry[name];
															print "</br>";
															print pt_buyer($qry[email]).pt_agent($qry[email]);
															print "</br>";
															print $qry[ipaddress];
															print "</td><td style=\"wrap\"><span class=\"style3\">";
												$link=db_connect();
												//print check_purchaser($qry[email]);
												//if (check_affiliate($qry[email])==true) print " <font color=red><b>*</b></font>";
												//color
												if ($qry[category]==1)
												{
													$myfont="blue";
												}

												if ($qry[category]==2)
												{
													$myfont="brown";
												}

												if ($qry[category]==3)
												{
													$myfont="black";
												}


												print "<strong><font color=".$myfont.">$qry[subject]</font></strong><br><br>";
												if ($qry["is_read"]==2) {
												// $mark="bgcolor=\"#FF0000\"";
												$mark_font="<font color=\"#FF0000\">";
												$mark_font_close="</font>";
												}else{
												$mark="";
												$mark_font="";
												$mark_font_close="</font>";
												}
													print $mark_font.$qry[komen].$mark_font_close;
													print "</br>";
												$msian_time=strtotime($qry[datetime])+(3600*21);

													//$msian_time=strtotime($qry[datetime]);
													print gmdate("d-m-Y h:i:s A",$msian_time)."<br>";

															//print $qry[email];
															print "</td><td><span class=\"style3\">";
                              if ($qry["is_read"]==0) print "<a href=cust_support_reply.php?email=".urlencode($qry[email])."&uid=".$qry[id].">Reply</a> | <a href=cust_support.php?action=hold&uid=".$qry[id].">Hold</a> | <font color=red>NEW</font> | <a href=cus_support.php?action=thrash&uid=".$qry[id].">Thrash</a> | <a href=cus_support.php?action=escalate&uid=".$qry[id].">Escalate</a>";
                              if ($qry["is_read"]==2) print "<a href=cust_support_reply.php?email=".urlencode($qry[email])."&uid=".$qry[id].">Reply</a> | <font color=red>HOLD</font> | <a href=cus_support.php?action=thrash&uid=".$qry[id].">Thrash</a> | | <a href=cus_support.php?action=escalate&uid=".$qry[id].">Escalate</a>";
                            print "</td>";

															// print "<td><span class=\"style3\">";
															// if ($qry["is_read"]==0) print "<a href=cus_support_reply.php?email=".urlencode($qry[email])."&uid=".$qry[id].">Reply</a> | <a href=cus_support.php?action=hold&uid=".$qry[id].">Hold</a> | <font color=red>NEW</font> | <a href=cus_support.php?action=thrash&uid=".$qry[id].">Thrash</a> | <a href=cus_support.php?action=escalate&uid=".$qry[id].">Escalate</a>";
															// if ($qry["is_read"]==2) print "<a href=cus_support_reply.php?email=".urlencode($qry[email])."&uid=".$qry[id].">Reply</a> | <font color=red>HOLD</font> | <a href=cus_support.php?action=thrash&uid=".$qry[id].">Thrash</a> | | <a href=cus_support.php?action=escalate&uid=".$qry[id].">Escalate</a>";
															// print "</td>";





															print "</td><td ".$mark."><span class=\"style3\">";


															print "</td><td><span class=\"style3\">";
												//			print $qry[datetime];

															print "</td>";

															print "</tr>";
													  }
												    }
												  }
												?>

										</table>
									</div>
									<!-- table-responsive -->
								</div>
							</div>
						</div>
						<!-- <div class="row">
							<div class="col-md-12 col-lg-12">
								<div class="card">
									<div class="card-header">
										<h3 class="card-title">Full color variations</h3>
									</div>
									<div class="table-responsive">
										<table class="table card-table table-vcenter text-nowrap table-primary" >
											<thead  class="bg-primary text-white">
												<tr>
													<th class="text-white">ID</th>
													<th class="text-white">Name</th>
													<th class="text-white">Position</th>
													<th class="text-white">Salary</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<th scope="row">1</th>
													<td>Joan Powell</td>
													<td>Associate Developer</td>
													<td>$450,870</td>
												</tr>
												<tr>
													<th scope="row">2</th>
													<td>Gavin Gibson</td>
													<td>Account manager</td>
													<td>$230,540</td>
												</tr>
												<tr>
													<th scope="row">3</th>
													<td>Julian Kerr</td>
													<td>Senior Javascript Developer</td>
													<td>$55,300</td>
												</tr>
												<tr>
													<th scope="row">4</th>
													<td>Cedric Kelly</td>
													<td>Accountant</td>
													<td>$234,100</td>
												</tr>
												<tr>
													<th scope="row">5</th>
													<td>Samantha May</td>
													<td>Junior Technical Author</td>
													<td>$43,198</td>
												</tr>
											</tbody>
										</table>
									</div> -->
									<!-- table-responsive -->
								<!-- </div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 col-lg-12">
								<div class="card">
									<div class="card-header">
										<h3 class="card-title">Full color variations2</h3>
									</div>
									<div class="table-responsive">
										<table class="table card-table table-vcenter text-nowrap table-warning" >
											<thead  class="bg-warning text-white">
												<tr>
													<th>ID</th>
													<th>Name</th>
													<th>Position</th>
													<th>Salary</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<th scope="row">1</th>
													<td>Joan Powell</td>
													<td>Associate Developer</td>
													<td>$450,870</td>
												</tr>
												<tr>
													<th scope="row">2</th>
													<td>Gavin Gibson</td>
													<td>Account manager</td>
													<td>$230,540</td>
												</tr>
												<tr>
													<th scope="row">3</th>
													<td>Julian Kerr</td>
													<td>Senior Javascript Developer</td>
													<td>$55,300</td>
												</tr>
												<tr>
													<th scope="row">4</th>
													<td>Cedric Kelly</td>
													<td>Accountant</td>
													<td>$234,100</td>
												</tr>
												<tr>
													<th scope="row">5</th>
													<td>Samantha May</td>
													<td>Junior Technical Author</td>
													<td>$43,198</td>
												</tr>
											</tbody>
										</table>
									</div> -->
									<!-- table-responsive -->
								<!-- </div>
							</div>
						</div>
					</div> -->
          <div align="center">
					<ul class="pagination pagination-sm"><li class="disabled"><a href="?limit=25&page=0">&laquo;</a></li><li class="active"><a href="?limit=25&page=1">1</a></li><li class="disabled"><a href="?limit=25&page=2">&raquo;</a></li></ul>				</div>

				<p>&nbsp;</p>
					<!--footer-->
					<footer class="footer">
						<div class="container">
							<div class="row align-items-center flex-row-reverse">
								<div class="col-lg-12 col-sm-12 mt-3 mt-lg-0 text-center">
									Copyright © 2018 <a href="http://jutawanapp.my/">Jutawan APP</a>. Designed by <a href="#">JAPP</a> All rights reserved.
								</div>
							</div>
						</div>
					</footer>
					<!-- End Footer-->
				</div>
			</div>
		</div>

		<!-- Back to top -->
		<a href="#top" id="back-to-top" style="display: inline;"><i class="fas fa-angle-up"></i></a>

		<!-- Dashboard js -->
		<script src="assets/js/vendors/jquery-3.2.1.min.js"></script>
		<script src="assets/js/vendors/bootstrap.bundle.min.js"></script>
		<script src="assets/js/vendors/jquery.sparkline.min.js"></script>
		<script src="assets/js/vendors/selectize.min.js"></script>
		<script src="assets/js/vendors/jquery.tablesorter.min.js"></script>
		<script src="assets/js/vendors/circle-progress.min.js"></script>
		<script src="assets/plugins/rating/jquery.rating-stars.js"></script>
		<!-- Side menu js -->
		<script src="assets/plugins/toggle-sidebar/js/sidemenu.js"></script>

		<!-- 3Dlines-animation -->
        <script src="assets/plugins/3Dlines-animation/three.min.js"></script>
        <script src="assets/plugins/3Dlines-animation/projector.js"></script>
        <script src="assets/plugins/3Dlines-animation/canvas-renderer.js"></script>
        <script src="assets/plugins/3Dlines-animation/3d-lines-animation.js"></script>
        <script src="assets/plugins/3Dlines-animation/color.js"></script>

		<!-- Custom scroll bar Js-->
		<script src="assets/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js"></script>

		<!--Counters -->
		<script src="assets/plugins/counters/counterup.min.js"></script>
		<script src="assets/plugins/counters/waypoints.min.js"></script>

		<!-- custom js -->
		<script src="assets/js/custom.js"></script>

	</body>

<!-- Mirrored from spruko.com/demo/ren/Blue-animated/LeftMenu/tables.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 20 Sep 2018 05:53:41 GMT -->
</html>
