<?php
error_reporting(1);
require('class.phpmailer.php');

require('class.smtp.php');

//require('../class.writetolog.php');

//require('../mail.cfg.php');

require_once('inc.config.php');
	require_once('inc.functions.php');
	require_once('inc.nufunctions.php');
	require_once('xbackend/inc/inc.hdr.php');



global $CFG_SYS;



?>
<!doctype html>
<html lang="en" dir="ltr">

<!-- Mirrored from spruko.com/demo/ren/Blue-animated/LeftMenu/profile.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 20 Sep 2018 05:04:18 GMT -->
<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="msapplication-TileColor" content="#0061da">
		<meta name="theme-color" content="#1643a3">
		<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="mobile-web-app-capable" content="yes">
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<link rel="icon" href="favicon.ico" type="image/x-icon"/>
		<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />

		<!-- Title -->
		<title>Ren - a responsive, flat and full featured admin template</title>

        <!--Font Awesome-->
		<link href="assets/plugins/fontawesome-free/css/all.css" rel="stylesheet">

		<!-- Font-family -->
		<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500" rel="stylesheet">

		<!-- Dashboard Css -->
		<link href="assets/css/dashboard.css" rel="stylesheet" />

		<!-- Custom scroll bar css-->
		<link href="assets/plugins/scroll-bar/jquery.mCustomScrollbar.css" rel="stylesheet" />

		<!-- Sidemenu Css -->
		<link href="assets/plugins/toggle-sidebar/css/sidemenu.css" rel="stylesheet">

		<!-- c3.js Charts Plugin -->
		<link href="assets/plugins/charts-c3/c3-chart.css" rel="stylesheet" />

		<!---Font icons-->
		<link href="assets/plugins/iconfonts/plugin.css" rel="stylesheet" />

		<script type="text/javascript">
		function PopupCenterDual(url, title, w, h) {
		// Fixes dual-screen position Most browsers Firefox
		var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
		var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;
		width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
		height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

		var left = ((width / 2) - (w / 2)) + dualScreenLeft;
		var top = ((height / 2) - (h / 2)) + dualScreenTop;
		var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

		// Puts focus on the newWindow
		if (window.focus) {
		newWindow.focus();
		}
		}
		</script>

	</head>
	<body class="app sidebar-mini rtl">
		<div id="global-loader" ><div class="showbox"><div class="lds-ring"><div></div><div></div><div></div><div></div></div></div></div>
		<div class="page">
			<div class="page-main">
				<!-- Navbar-->
				<header class="app-header header">

					<!-- Header Background Animation-->
					<div id="canvas" class="gradient"></div>

					<!-- Navbar Right Menu-->
					<div class="container-fluid">
						<div class="d-flex">
							<a class="header-brand" href="index-2.html">
								<img alt="ren logo" class="header-brand-img" src="assets/images/brand/logo.png">
							</a>
							<a aria-label="Hide Sidebar" class="app-sidebar__toggle" data-toggle="sidebar" href="#"></a>
							<div class="d-flex order-lg-2 ml-auto">
								<div class="">
									<form class="input-icon  mr-2">
										<input class="form-control header-search" placeholder="Search&hellip;" tabindex="1" type="search">
										<div class="input-icon-addon">
											<i class="fe fe-search"></i>
										</div>
									</form>
								</div>
								<div class="dropdown d-none d-md-flex">
									<a class="nav-link icon" data-toggle="dropdown">
										<i class="fas fa-user"></i>
										<span class="nav-unread bg-green"></span>
									</a>
									<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
										<a class="dropdown-item d-flex pb-3" href="#">
											<span class="avatar brround mr-3 align-self-center" style="background-image: url(assets/images/faces/male/4.jpg)"></span>
											<div>
												<strong>Madeleine Scott</strong> Sent you add request
												<div class="small text-muted">
													view profile
												</div>
											</div>
										</a>
										<a class="dropdown-item d-flex pb-3" href="#">
											<span class="avatar brround mr-3 align-self-center" style="background-image: url(assets/images/faces/female/14.jpg)"></span>
											<div>
												<strong>rebica</strong> Suggestions for you
												<div class="small text-muted">
													view profile
												</div>
											</div>
										</a>
										<a class="dropdown-item d-flex pb-3" href="#">
											<span class="avatar brround mr-3 align-self-center" style="background-image: url(assets/images/faces/male/1.jpg)"></span>
											<div>
												<strong>Devid robott</strong> sent you add request
												<div class="small text-muted">
													view profile
												</div>
											</div>
										</a>
										<div class="dropdown-divider"></div><a class="dropdown-item text-center text-muted-dark" href="#">View all contact list</a>
									</div>
								</div>
								<div class="dropdown d-none d-md-flex">
									<a class="nav-link icon" data-toggle="dropdown">
										<i class="fas fa-bell"></i>
										<span class="nav-unread bg-danger"></span>
									</a>
									<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
										<a class="dropdown-item d-flex pb-3" href="#">
											<div class="notifyimg">
												<i class="fas fa-thumbs-up"></i>
											</div>
											<div>
												<strong>Someone likes our posts.</strong>
												<div class="small text-muted">
													3 hours ago
												</div>
											</div>
										</a>
										<a class="dropdown-item d-flex pb-3" href="#">
											<div class="notifyimg">
												<i class="fas fa-comment-alt"></i>
											</div>
											<div>
												<strong>3 New Comments</strong>
												<div class="small text-muted">
													5 hour ago
												</div>
											</div>
										</a>
										<a class="dropdown-item d-flex pb-3" href="#">
											<div class="notifyimg">
												<i class="fas fa-cogs"></i>
											</div>
											<div>
												<strong>Server Rebooted.</strong>
												<div class="small text-muted">
													45 mintues ago
												</div>
											</div>
										</a>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item text-center text-muted-dark" href="#">View all Notification</a>
									</div>
								</div>
								<div class="dropdown d-none d-md-flex">
									<a class="nav-link icon" data-toggle="dropdown"><i class="fas fa-envelope"></i> <span class="badge badge-info badge-pill">2</span></a>
									<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
										<a class="dropdown-item text-center text-dark" href="#">2 New Messages</a>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item d-flex pb-3" href="#">
											<span class="avatar brround mr-3 align-self-center" style="background-image: url(assets/images/faces/male/41.jpg)"></span>
											<div>
												<strong>Madeleine</strong> Hey! there I' am available....
												<div class="small text-muted">
													3 hours ago
												</div>
											</div>
										</a>
										<a class="dropdown-item d-flex pb-3" href="#"><span class="avatar brround mr-3 align-self-center" style="background-image: url(assets/images/faces/female/1.jpg)"></span>
											<div>
												<strong>Anthony</strong> New product Launching...
												<div class="small text-muted">
													5 hour ago
												</div>
											</div>
										</a>
										<a class="dropdown-item d-flex pb-3" href="#"><span class="avatar brround mr-3 align-self-center" style="background-image: url(assets/images/faces/female/18.jpg)"></span>
											<div>
												<strong>Olivia</strong> New Schedule Realease......
												<div class="small text-muted">
													45 mintues ago
												</div>
											</div>
										</a>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item text-center text-muted-dark" href="#">See all Messages</a>
									</div>
								</div>
								<div class="dropdown">
									<a class="nav-link pr-0 leading-none d-flex" data-toggle="dropdown" href="#">
										<span class="avatar avatar-md brround" style="background-image: url(assets/images/faces/female/25.jpg)"></span>
										<span class="ml-2 d-none d-lg-block">
											<span class="text-white">Sindy Scribner</span>
										</span>
									</a>
									<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
										<a class="dropdown-item" href="#"><i class="dropdown-icon mdi mdi-account-outline"></i> Profile</a>
										<a class="dropdown-item" href="#"><i class="dropdown-icon mdi mdi-settings"></i> Settings</a>
										<a class="dropdown-item" href="#"><span class="float-right"><span class="badge badge-primary">6</span></span> <i class="dropdown-icon mdi mdi-message-outline"></i> Inbox</a>
										<a class="dropdown-item" href="#"><i class="dropdown-icon mdi mdi-comment-check-outline"></i> Message</a>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item" href="#"><i class="dropdown-icon mdi mdi-compass-outline"></i> Need help?</a>
										<a class="dropdown-item" href="login.html"><i class="dropdown-icon mdi mdi-logout-variant"></i> Sign out</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</header>

				<!-- Sidebar menu-->
				<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
				<aside class="app-sidebar">
					<div class="app-sidebar__user">
						<div class="dropdown">
							<a class="nav-link p-0 leading-none d-flex" data-toggle="dropdown" href="#">
								<span class="avatar avatar-md brround" style="background-image: url(assets/images/faces/female/25.jpg)"></span>
								<span class="ml-2 "><span class="text-white app-sidebar__user-name font-weight-semibold">Sindy Scribner</span><br>
									<span class="text-muted app-sidebar__user-name text-sm"> Ui Designer</span>
								</span>
							</a>
							<div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
								<a class="dropdown-item" href="#"><i class="dropdown-icon mdi mdi-account-outline"></i> Profile</a>
								<a class="dropdown-item" href="#"><i class="dropdown-icon mdi mdi-settings"></i> Settings</a>
								<a class="dropdown-item" href="#"><span class="float-right"><span class="badge badge-primary">6</span></span> <i class="dropdown-icon mdi mdi-message-outline"></i> Inbox</a>
								<a class="dropdown-item" href="#"><i class="dropdown-icon mdi mdi-comment-check-outline"></i> Message</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="#"><i class="dropdown-icon mdi mdi-compass-outline"></i> Need help?</a>
								<a class="dropdown-item" href="login.html"><i class="dropdown-icon mdi mdi-logout-variant"></i> Sign out</a>
							</div>
						</div>
					</div>
					<ul class="side-menu">
						<!-- <li class="slide">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fas fa-home"></i><span class="side-menu__label">Dashboard</span><i class="angle fas fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li><a class="slide-item" href="index-2.html">Home 1</a></li>
								<li><a class="slide-item" href="index2.html">Home 2</a></li>
								<li><a class="slide-item" href="index3.html">Home 3</a></li>
								<li><a class="slide-item" href="index4.html">Home 4</a></li>
							</ul>
						</li> -->
						<li>
							<a class="side-menu__item" href="index-2.html"><i class="side-menu__icon fas fa-home"></i><span class="side-menu__label">Dashboard</span></a>
						</li>
						<li>
							<a class="side-menu__item" href="cust_support.php"><i class="side-menu__icon fas fa-window-restore"></i><span class="side-menu__label">Admin Support</span></a>
						</li>
						<!-- <li class="slide">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fas fa-chart-bar"></i><span class="side-menu__label">Chart Types</span><i class="angle fas fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="chart-flot.html" class="slide-item">Chart flot</a>
								</li>
								<li>
									<a href="chartjs.html" class="slide-item">Chart js</a>
								</li>
								<li>
									<a href="charts-peity.html" class="slide-item">Charts Peity</a>
								</li>
								<li>
									<a href="chart-morris.html" class="slide-item">Chart Morris</a>
								</li>
								<li>
									<a href="chart-ricksaw.html" class="slide-item">Chart Ricksaw</a>
								</li>
								<li>
									<a href="chart-chartist.html" class="slide-item">Chart Chartist</a>
								</li>
								<li>
									<a href="charts.html" class="slide-item">Bar Charts</a>
								</li>
								<li>
									<a href="chart-line.html" class="slide-item">Line Charts</a>
								</li>
								<li>
									<a href="chart-donut.html" class="slide-item">Donut Charts</a>
								</li>
								<li>
									<a href="chart-pie.html" class="slide-item">Pie charts</a>
								</li>
							</ul>
						</li> -->
						<!-- <li>
							<a class="side-menu__item" href="maps.html"><i class="side-menu__icon fas fa-map-marker"></i><span class="side-menu__label">Maps</span></a>
						</li>
						<li>
							<a class="side-menu__item" href="gallery.html"><i class="side-menu__icon fas fa-images"></i><span class="side-menu__label">Gallery</span></a>
						</li>
						<li class="slide">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fas fa-calendar"></i><span class="side-menu__label">Calendar</span><i class="angle fas fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="calendar.html" class="slide-item">Default calendar</a>
								</li>
								<li>
									<a href="calendar2.html" class="slide-item">Full calendar</a>
								</li>
							</ul>
						</li> -->
						<!-- <li class="slide">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fas fa-snowflake"></i><span class="side-menu__label">Ui Design</span><i class="angle fas fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="cards.html" class="slide-item">Cards design</a>
								</li>
								<li>
									<a href="chat.html" class="slide-item">Default Chat</a>
								</li>
								<li>
									<a href="notify.html" class="slide-item">Notifications</a>
								</li>
								<li>
									<a href="sweetalert.html" class="slide-item">Sweet alerts</a>
								</li>
								<li>
									<a href="rangeslider.html" class="slide-item">Range slider</a>
								</li>
								<li>
									<a href="scroll.html" class="slide-item">Content Scroll bar</a>
								</li>
								<li>
									<a href="counters.html" class="slide-item">Counters</a>
								</li>
								<li>
									<a href="loaders.html" class="slide-item">Loaders</a>
								</li>
								<li>
									<a href="rating.html" class="slide-item">Rating</a>
								</li>
								<li>
									<a href="time-line.html" class="slide-item">Time Line</a>
								</li>
							</ul>
						</li> -->

						<!-- <li class="slide">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fas fa-newspaper"></i><span class="side-menu__label">Pages</span><i class="angle fas fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="profile.html" class="slide-item">Profile</a>
								</li>
								<li>
									<a href="editprofile.html" class="slide-item">Edit Profile</a>
								</li>
								<li>
									<a href="email.html" class="slide-item">Email</a>
								</li>
								<li>
									<a href="emailservices.html" class="slide-item">Email Inbox</a>
								</li>
								<li>
									<a href="login.html" class="slide-item">Login</a>
								</li>
								<li>
									<a href="register.html" class="slide-item">Register</a>
								</li>
								<li>
									<a href="forgot-password.html" class="slide-item">Forgot password</a>
								</li>
								<li>
									<a href="lockscreen.html" class="slide-item">Lock screen</a>
								</li>
								<li>
									<a href="empty.html" class="slide-item">Empty Page</a>
								</li>
								<li>
									<a href="construction.html" class="slide-item">Under Construction</a>
								</li>
								<li>
									<a href="about.html" class="slide-item">About Company</a>
								</li>
								<li>
									<a href="company-history.html" class="slide-item">Company History</a>
								</li>
								<li>
									<a href="services.html" class="slide-item">Services</a>
								</li>
								<li>
									<a href="faq.html" class="slide-item">FAQS</a>
								</li>
								<li>
									<a href="terms.html" class="slide-item">Terms and Conditions</a>
								</li>
								<li>
									<a href="pricing.html" class="slide-item">Pricing Tables</a>
								</li>
								<li>
									<a href="blog.html" class="slide-item">Blog</a>
								</li>
								<li>
									<a href="invoice.html" class="slide-item">Invoice</a>
								</li>
							</ul>
						</li> -->
						<!-- <li class="slide">
							<a class="side-menu__item active" data-toggle="slide" href="#"><i class="side-menu__icon fas fa-table"></i><span class="side-menu__label">Tables</span><i class="angle fas fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="tables.html" class="slide-item">Default table</a>
								</li>
								<li>
									<a href="datatable.html" class="slide-item">Data Table</a>
								</li>
							</ul>
						</li> -->
						<!-- <li class="slide">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fas fa-shopping-cart"></i><span class="side-menu__label">E-commerce</span><i class="angle fas fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="shop.html" class="slide-item">Products</a>
								</li>
								<li>
									<a href="shop-des.html" class="slide-item">Product Details</a>
								</li>
								<li>
									<a href="cart.html" class="slide-item">product-Search page</a>
								</li>
							</ul>
						</li> -->
						<!-- <li class="slide">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fas fa-adjust"></i><span class="side-menu__label">Icons</span><i class="angle fas fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="icons.html" class="slide-item">Font Awesome</a>
								</li>
								<li>
									<a href="icons2.html" class="slide-item">Material Design Icons</a>
								</li>
								<li>
									<a href="icons3.html" class="slide-item">Simple Line Iocns</a>
								</li>
								<li>
									<a href="icons4.html" class="slide-item">Feather Icons</a>
								</li>
								<li>
									<a href="icons5.html" class="slide-item">Ionic Icons</a>
								</li>
								<li>
									<a href="icons6.html" class="slide-item">Flags Icons</a>
								</li>
							</ul>
						</li>
						<li class="slide">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fas fa-file-alt"></i><span class="side-menu__label">Forms</span><i class="angle fas fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="form-elements.html" class="slide-item">Form Elements</a>
								</li>
								<li>
									<a href="form-wizard.html" class="slide-item">Form-wizard Elements</a>
								</li>
								<li>
									<a href="wysiwyag.html" class="slide-item">Text Editor</a>
								</li>
							</ul>
						</li>
						<li class="slide">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fas fa-cubes"></i><span class="side-menu__label">Components</span><i class="angle fas fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="modal.html" class="slide-item">Modal</a>
								</li>
								<li>
									<a href="tooltipandpopover.html" class="slide-item">Tooltip and popover</a>
								</li>
								<li>
									<a href="progress.html" class="slide-item">Progress</a>
								</li>
								<li>
									<a href="chart.html" class="slide-item">Charts</a>
								</li>
								<li>
									<a href="carousel.html" class="slide-item">Carousels</a>
								</li>
								<li>
									<a href="accordion.html" class="slide-item">Accordions</a>
								</li>
								<li>
									<a href="tabs.html" class="slide-item">Tabs</a>
								</li>
								<li>
									<a href="headers.html" class="slide-item">Headers</a>
								</li>
								<li>
									<a href="footers.html" class="slide-item">Footers</a>
								</li>
								<li>
									<a href="crypto-currencies.html" class="slide-item">Crypto-currencies</a>
								</li>
								<li>
									<a href="users-list.html" class="slide-item">User List</a>
								</li>
								<li>
									<a href="search.html" class="slide-item">Search page</a>
								</li>
							</ul>
						</li>
						<li class="slide">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fas fa-pen-square"></i><span class="side-menu__label">Basic Elements</span><i class="angle fas fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="alerts.html" class="slide-item">Alerts</a>
								</li>
								<li>
									<a href="buttons.html" class="slide-item">Buttons</a>
								</li>
								<li>
									<a href="colors.html" class="slide-item">Colors</a>
								</li>
								<li>
									<a href="avatars.html" class="slide-item">Avatar-Square</a>
								</li>
								<li>
									<a href="avatar-round.html" class="slide-item">Avatar-Rounded</a>
								</li>
								<li>
									<a href="avatar-radius.html" class="slide-item">Avatar-Radius</a>
								</li>
								<li>
									<a href="dropdown.html" class="slide-item">Drop downs</a>
								</li>
								<li>
									<a href="thumbnails.html" class="slide-item">Thumbnails</a>
								</li>
								<li>
									<a href="mediaobject.html" class="slide-item">Media Object</a>
								</li>
								<li>
									<a href="list.html" class="slide-item">List</a>
								</li>
								<li>
									<a href="tags.html" class="slide-item">Tags</a>
								</li>
								<li>
									<a href="pagination.html" class="slide-item">Pagination</a>
								</li>
								<li>
									<a href="navigation.html" class="slide-item">Navigation</a>
								</li>
								<li>
									<a href="typography.html" class="slide-item">Typography</a>
								</li>
								<li>
									<a href="breadcrumbs.html" class="slide-item">Breadcrumbs</a>
								</li>
								<li>
									<a href="badge.html" class="slide-item">Badges</a>
								</li>
								<li>
									<a href="jumbotron.html" class="slide-item">Jumbotron</a>
								</li>
								<li>
									<a href="panels.html" class="slide-item">Panels</a>
								</li>
							</ul>
						</li>

						<li class="slide">
							<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fas fa-exclamation-circle"></i><span class="side-menu__label">Error pages</span><i class="angle fas fa-angle-right"></i></a>
							<ul class="slide-menu">
								<li>
									<a href="400.html" class="slide-item">400 Error</a>
								</li>
								<li>
									<a href="401.html" class="slide-item">401 Error</a>
								</li>
								<li>
									<a href="403.html" class="slide-item">403 Error</a>
								</li>
								<li>
									<a href="404.html" class="slide-item">404 Error</a>
								</li>
								<li>
									<a href="500.html" class="slide-item">500 Error</a>
								</li>
								<li>
									<a href="503.html" class="slide-item">503 Error</a>
								</li>
							</ul>
						</li>
						<li>
							<a class="side-menu__item" href="https://themeforest.net/user/sprukotechnologies/portfolio"><i class="side-menu__icon fas fa-question-circle"></i><span class="side-menu__label">Help & Support</span></a>
						</li> -->
					</ul>
				</aside>
				<div class="app-content my-3 my-md-5">
					<div class="side-app">
						<div class="page-header">
							<h4 class="page-title">Customer Reply</h4>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="index2.html">Home</a></li>
								<li class="breadcrumb-item active" aria-current="page">Customer Reply</li>
							</ol>
						</div>
						<div class="row">
							<!-- <div class="col-md-12">
								<div class="card card-profile "  style="background-image: url(assets/images/photos/1.jpg); background-size:cover;">
									<div class="card-body text-center">
										<img class="card-profile-img" src="assets/images/faces/male/16.jpg" alt="img">
										<h3 class="mb-3 text-white">George Mestayer</h3>
										<p class="mb-4 text-white">Ren Administrator</p>
										<button class="btn btn-primary btn-sm">
											<span class="fab fa-twitter"></span> Follow
										</button>
										<a href="editprofile.html" class="btn btn-success btn-sm"><i class="fas fa-pencil-alt" aria-hidden="true"></i> Edit profile</a>
									</div>
								</div>
							</div> -->
							<div class="col-lg-4">
								<div class="card p-5 ">
									<div class="card-status bg-orange br-tr-3 br-tl-3"></div>
									<div class="card-title">
										Customer Support Response
									</div>



									<div class="media-list">
										<?php

										$uid= $_GET['uid'];
										//echo $uid;

										$uemail= $_GET['email'];
										//echo $uemail;

										$usubmitted= $_POST['submitted'];

										$uresponse=addslashes($_POST['txtresponse']);

										$bcc = $_POST['bcc'];


										mysql_connect($db_host, $db_user, $db_pass)

											or die ("Database CONNECT Error");



											$q3="select * from affiliates where email=\"".$uemail."\" LIMIT 0,1";
											$result3 = mysql_db_query($db, $q3)

													or die ("Database INSERT Error");

											$qry3 = mysql_fetch_array($result3);


											$q5="select * from user_pass where user=\"".$uemail."\" LIMIT 0,1";
											$result5 = mysql_db_query($db, $q5)

													or die ("Database INSERT Error");

											$qry5 = mysql_fetch_array($result5);


											$q="select * from messages WHERE id=\"".$uid."\" ";

											$result = mysql_db_query($db, $q)

													or die ("Database INSERT Error");



											$qry = mysql_fetch_array($result);







											if ($usubmitted==1)

												{

													/**

													* EMAILS

													*/

													//New - Now send just send notification email with PIN to retrieve mail_body_html

													$notify_response="Salam Tuan/Puan

													Saya telah membalas pertanyaan anda. Untuk membaca balasan saya, sila klik atau buka laman web di bawah

													http://bit.ly/2xEXhSZ

													GUNAKAN NOMBOR PIN 892".$uid." UNTUK BUKA MESEJ.";





													$mail_info['mail_subject']['2'] = "Sistem Bantuan Robot JA: ".$qry[subject];

													$mail_info['mail_body_txt']['2'] = "

													".$notify_response."
										--------
										INI ADALAH BALASAN SISTEM ROBOT. SILA JANGAN BALAS EMAIL INI!!
										Untuk membalas, sila isi borang pertanyaan di http://bit.ly/2wnqjW5
										--------


													Yang berkhidmat,

													Afiq
													JutawanApp";



													$mail_info['mail_body_html']['2'] = "

													<html>



													<body>

													<p>

													".nl2br($notify_response)."

													<br>

													<br>

													</p>
										<p>

										--------<br>
										<b>INI ADALAH BALASAN SISTEM ROBOT. SILA JANGAN BALAS EMAIL INI!!</b><br>
										<br>
										Untuk membalas, sila isi borang pertanyaan di http://bit.ly/2wnqjW5
										</p>
													<p>Yang berkhidmat,<br><br>

													Afiq<br>
													JutawanApp
													</p>



													</body>

													</html>

																";

													//$log = new writelog($CFG_SYS);



													// pass customer information for email

													$mail_subject = $mail_info['mail_subject'][2];

													$mail_html_msg = $mail_info['mail_body_html'][2];

													$mail_txt_msg = $mail_info['mail_body_txt'][2];



													$mail = new PHPMailer();
													$mail->IsSMTP();   																		// set mailer to use SMTP



													$mail->Host = "216.172.182.38";
													$mail->SMTPAuth = true;
													$mail->Username = "afiq";
													$mail->Password = "pkh101z2";
													$mail->Port = 587;
													$mail->AddReplyTo("afiq@jutawanapp.com","Afiq - Jutawan App");
													$mail->From = "afiq@jutawanapp.pw";
													$mail->FromName = "Sistem Bantuan JA";
													$mail->AddAddress($uemail);     									//This is target recipient

													//chekc if need to send BCC
													if($bcc!="")$mail->AddBCC($bcc);



													$mail->WordWrap = 50;                                 // set word wrap to 50 characters



													if ($attach_ebook=='attach')

														$mail->AddAttachment($product_attachment);           // add attachments





													$mail->IsHTML(true);                                  // set email format to HTML



													$mail->Subject = $mail_subject;

													$mail->Body    = $mail_html_msg;

													$mail->AltBody = $mail_txt_msg;



														//echo $mail->Subject.$mail->Body.$mail->From.$mail->FromName;



													if(!$mail->Send())

													{

														echo "<font color=red>Mailer Failure</font>" . $mail->ErrorInfo;



														exit;

													}else{

														echo "<font color=green size=5>Mailer Success</font>";



														$sq3="UPDATE messages SET response=\"".$uresponse."\", is_read=1 WHERE id=".$uid." LIMIT 1";



														$sresult = mysql_db_query($db, $sq3)

															or die ("Database UPDATE Error");





													}

														//echo $mail->Subject.$mail->Body.$mail->From.$mail->FromName;



												//		echo "Mailer Success";

												}

										?>
										<?php




										print "<b>From :</b> ";

										print $qry[name];

										print "  ";

										//print $qry[email];

							$link=db_connect();

							//print check_purchaser($qry[email]);

							//if (check_affiliate($qry[email])) print " <font color=red><b>*</b></font>";
							print pt_buyer($qry[email]).pt_agent($qry[email]);






										print " <br>";

										echo "<br><b>
										<a onclick=\"PopupCenterDual('manual_email.php?email=".urlencode($qry[email])."&subject=".urlencode($qry[subject])."&body=".urlencode(nl2br($qry[komen]))."','myWindow','600','450'); \" href=\"javascript:void(0);\">CLICK TO SEND MANUAL EMAIL</a>
										<br></b>";

										print "<br><b>Affiliate Link :</b> http://www.jutawanapp.com/?id=".$qry3[refid]."<br><b>Username : </b>".$qry[email]."<br><b>Password : </b>".$qry5[pass]."<br><b>Join Date : </b>".$qry3[date]."<br><b>Bank : </b>".$qry3[bank_name]." ".$qry3[bank_account];


										//Get sales and comm history


											    $resultC = mysql_db_query($db, "select count(*) as cnt from deposits where email = '".$uemail."'")
												or die ("Database  Error".mysql_error());
												while ($qryC = mysql_fetch_array($resultC))
												{
														$notification_count=$qryC[cnt];
												}
											    echo "<br><b>Notification Form Submitted Count:</b> ".$notification_count;


										if ($qry3[refid]!="")
										{

												$resultA = mysql_db_query($db, "select count(*) as cnt from sales_bank where refid = '".$qry3[refid]."'")
												or die ("Database  Error".mysql_error());
												while ($qryA = mysql_fetch_array($resultA))
												{
														$bank_sales_count=$qryA[cnt];
												}
										        $total_count=$bank_sales_count;
												echo "<br><b>Total Commission :</b> RM ".number_format(($total_count * $comm_rate),2);
												//echo "<br><br>";
												$total_sales=$total_count * $comm_rate;

												$resultB = mysql_db_query($db, "select * from payment_history where refid = '".$qry3[refid]."'")
												or die ("Database  Error".mysql_error());
												$total_amt=0;
												while ($qryB = mysql_fetch_array($resultB))
												{
												//echo "<tr bgcolor=white><td class=tree>".$q[datetime]."</td><td class=tree>RM ".number_format($q[amount],2)."</td></tr>";
												$total_amt=$total_amt + $qryB[amount];
												}
												//echo "</table>";
												$balance=$total_sales-$total_amt;
												echo "<br><b>Paid :</b> RM ".number_format($total_amt,2)."";
												echo "<br><b>Not Yet Paid :</b> RM ".number_format($balance,2)."<br>";
										        // end sales

										}


										print "<br><br> <b>Subject :</b> ";

										//print $qry3[refid];



										print $qry[subject];



										print "<br><br> <b>Message <font color=blue>".$qry[id]."</font> :</b> ";

										//print $qry[komen];

										//detect attachment from user upload
										$preg = '/(\w+\.(png|jpg|gif|jpeg))/';
							            preg_match_all($preg, $qry[komen], $arr);
										$my_komen=str_replace($arr[0][0],"<a href=../U4l04d/".$arr[0][0]." target=_blank>Open Attachment</a>",$qry[komen]);

										print nl2br($my_komen);





										print "<br><br> <b>Date/Time :</b> ";

							//			print $qry[datetime];

										$msian_time=strtotime($qry[datetime])+(3600*21);

										print gmdate("d-m-Y h:i:s A",$msian_time)."<br>";

										print "<br><br> <b>Status :</b> ";

										if ($qry[is_read]==0) print "<font color=red>NEW</font> | <a href=cus_support.php?action=thrash&uid=".$qry[id].">Thrash</a> | <a href=cus_support.php?action=escalate&uid=".$qry[id].">Escalate</a>";

										if ($qry[is_read]==1) print "Replied";

										if ($qry[is_read]==2) print "<font color=red>HOLD</font> | <a href=cus_support.php?action=thrash&uid=".$qry[id].">Thrash</a> | <a href=cus_support.php?action=escalate&uid=".$qry[id].">Escalate</a>";

										if ($qry[is_read]==9) print "<font color=red>THRASH</font>";

							?>


									</div>
									<!-- media-list -->
								</div>

								<!-- <div class="card"> -->
									<!-- <div class="card-status bg-primary br-tr-3 br-tl-3"></div>
									<div class="card-header pt-5 pb-5">
										<div class="input-group">
											<input type="text" class="form-control" placeholder="Message">
											<div class="input-group-append">
												<button type="button" class="btn btn-primary">
													<i class="fas fa-search" aria-hidden="true"></i>
												</button>
											</div>
										</div>
									</div> -->
									<!-- <ul class="list-group card-list-group">
										<li class="list-group-item py-5">
											<div class="media m-0">
												<div class="media-object avatar brround avatar-md mr-4" style="background-image: url(assets/images/faces/male/16.jpg)"></div>
												<div class="media-body">
													<div class="media-heading">
														<small class="float-right text-muted">4 min</small>
														<h5><strong>George Mestayer</strong></h5>
													</div>
													<div>
														Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet
													</div>
													<ul class="media-list">
														<li class="media mt-4">
															<div class="media-object brround avatar mr-4" style="background-image: url(assets/images/faces/female/17.jpg)"></div>
															<div class="media-body">
																<strong>Holley Corby: </strong>
																I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you
															</div>
														</li>

													</ul>
												</div>
											</div>
										</li>
										<li class="list-group-item py-5">
											<div class="media m-0">
												<div class="media-object avatar brround avatar-md mr-4" style="background-image: url(assets/images/faces/male/16.jpg)"></div>
												<div class="media-body">
													<div class="media-heading">
														<small class="float-right text-muted">34 min</small>
														<h5><strong>George Mestayer</strong></h5>
													</div>
													<div>
														 Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus</div>
													<ul class="media-list">
														<li class="media mt-4">
															<div class="media-object brround avatar mr-4" style="background-image: url(assets/images/faces/male/32.jpg)"></div>
															<div class="media-body">
																<strong>Art Langner: </strong>
																master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure
															</div>
														</li>
													</ul>
												</div>
											</div>
										</li>
									</ul> -->
								<!-- </div> -->

								<!-- <div class="card"> -->
									<!-- <div class="card-status bg-success br-tr-3 br-tl-3"></div> -->
									<!-- <div class="card-body">
										<div class="card-box tilebox-one">
											<i class="icon-layers float-right text-muted"><i class="fas fa-cubes text-success" aria-hidden="true"></i></i>
											<h6 class="text-drak text-uppercase mt-0">Projects</h6>
											<h2 class="m-b-20">678</h2>
											<span class="badge badge-success"> +78% </span> <span class="text-muted">From previous period</span>
										</div>
									</div> -->
								<!-- </div> -->
								<!-- <div class="card">
									<div class="card-status bg-primary br-tr-3 br-tl-3"></div>
									<div class="card-body">
										<div class="card-box tilebox-one">
											<i class="icon-layers float-right text-muted"><i class="fas fa-chart-bar text-secondary" aria-hidden="true"></i></i>
											<h6 class="text-drak text-uppercase mt-0">Profits</h6>
											<h2 class="m-b-20">7,908</h2>
											<span class="badge badge-secondary"> +66% </span> <span class="text-muted">Last year</span>
										</div>
									</div>
								</div> -->
							</div>
							<div class="col-lg-8">
								<div class="card">
									<div class="card-status bg-success br-tr-3 br-tl-3"></div>
									<div class="card-body">
										<div class=" " id="profile-log-switch">
											<div class="fade show active " >
												<div class="table-responsive border ">
													<!-- <table class="table row table-borderless w-100 m-0 ">
														<tbody class="col-lg-6 p-0">
															<tr>
																<td><strong>Full Name :</strong> George Mestayer</td>
															</tr>
															<tr>
																<td><strong>Location :</strong> USA</td>
															</tr>
															<tr>
																<td><strong>Languages :</strong> English, German, Spanish.</td>
															</tr>
														</tbody>
														<tbody class="col-lg-6 p-0">
															<tr>
																<td><strong>Website :</strong> ren.com</td>
															</tr>
															<tr>
																<td><strong>Email :</strong> georgemestayer@ren.com</td>
															</tr>
															<tr>
																<td><strong>Phone :</strong> +125 254 3562 </td>
															</tr>
														</tbody>
													</table> -->
												</div>
												<div class="row mt-5 profie-img">
													<div class="col-md-7">
														<div class="media-heading">
														<!-- <h5><strong>Biography</strong></h5> -->
														<?php



														  $q="select * from messages WHERE email=\"".$uemail."\" ORDER BY id DESC";

														  $result = mysql_db_query($db, $q)

														    or die ("Database INSERT Error");



														  print "<b>Total Messages :</b> ".mysql_num_rows($result)."<br><br>";

														// Continue usual Code, display data

														  if (mysql_num_rows($result))

														  {

														    while ($qry = mysql_fetch_array($result))

														    {

																	// print "<table width=100%  border=0 cellspacing=4 cellpadding=4 bgcolor=#EBEBEB><tr><td>";

																	print "<b>From :</b> ";

																	print $qry[name];

																	print " < ";

																	print $qry[email];

																	print " ><br> <b>Subject :</b> ";

																	print $qry[subject];

																	print "<br> <b>Message <font color=blue>".$qry[id]."</font> :</b> ";

																	//print nl2br($qry[komen]);
																	//detect attachment from user upload
																	$preg = '/(\w+\.(png|jpg|gif|jpeg))/';
														            //$preg='/\.(jpg|jpeg|png|gif)(?:[\?\#].*)?$/i';

																	preg_match_all($preg, $qry[komen], $arr);
																	$my_komen=str_replace($arr[0][0],"<a href=../U4l04d/".$arr[0][0]." target=_blank>Open Attachment</a>",$qry[komen]);
																	print nl2br($my_komen);

																	print "<br> <b>Response :</b> ";

																	print nl2br($qry[response]);

																	print "<br> <b>Date/Time :</b> ";

														//			print $qry[datetime];

																	$msian_time=strtotime($qry[datetime])+(3600*21);

																	print gmdate("d-m-Y h:i:s A",$msian_time)."<br>";

																	print "<br> <b>Status :</b> ";

																	if ($qry[is_read]==0) print "<font color=red>NEW</font>";

																	if ($qry[is_read]==1) print "Replied";

																	if ($qry[is_read]==2) print "<font color=red>HOLD</font>";

																	if ($qry[is_read]==9) print "<font color=red>THRASH</font>";

																	print "</td></tr></table><br>";

															  }

														  }

														?>
													</div>
													<!-- <p>
														 Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus</p>
													<p >because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure.</p> -->
													</div>
													<!-- <img class="img-fluid rounded w-25 h-25 m-2" src="assets/images/photos/8.jpg" alt="banner image">
													<img class="img-fluid rounded w-25 h-25 m-2" src="assets/images/photos/10.jpg" alt="banner image ">
													<img class="img-fluid rounded w-25 h-25 m-2" src="assets/images/photos/11.jpg" alt="banner image "> -->
												</div>
											</div>
										</div>
									</div>
								</div>

								<!-- <div class="card"> -->
									<!-- <div class="card-status bg-blue br-tr-3 br-tl-3"></div>
									<div class="card-header">
										<h3 class="card-title">Recent Projects</h3>
									</div> -->
									<!-- <div class="card-body"> -->
										<!-- <div class="table-responsive">
										<table class="table card-table table-vcenter border text-nowrap">
											<thead>
												<tr>
													<th>Project Name</th>
													<th>Date</th>
													<th>Status</th>
													<th>Price</th>
												</tr>
											</thead>
											<tbody>

												<tr>
													<td><a href="store.html" class="text-inherit">Untrammelled prevents </a></td>
													<td>28 May 2018</td>
													<td><span class="status-icon bg-success"></span> Completed</td>
													<td>$56,908</td>
												</tr>
												<tr>
													<td><a href="store.html" class="text-inherit">Untrammelled prevents</a></td>
													<td>12 June 2018</td>
													<td><span class="status-icon bg-danger"></span> On going</td>
													<td>$45,087</td>
												</tr>
												<tr>
													<td><a href="store.html" class="text-inherit">Untrammelled prevents</a></td>
													<td>12 July 2018</td>
													<td><span class="status-icon bg-warning"></span> Pending</td>
													<td>$60,123</td>
												</tr>
												<tr>
													<td><a href="store.html" class="text-inherit">Untrammelled prevents</a></td>
													<td>14 June 2018</td>
													<td><span class="status-icon bg-warning"></span> Pending</td>
													<td>$70,435</td>
												</tr>
												<tr>
													<td><a href="store.html" class="text-inherit">Untrammelled prevents</a></td>
													<td>25 June 2018</td>
													<td><span class="status-icon bg-success"></span> Completed</td>
													<td>$15,987</td>
												</tr>
											</tbody>
										</table>
									</div> -->
									<!-- </div> -->
								<!-- </div> -->
							</div>
						</div>
					</div>
					<!--footer-->
					<footer class="footer">
						<div class="container">
							<div class="row align-items-center flex-row-reverse">
								<div class="col-lg-12 col-sm-12 mt-3 mt-lg-0 text-center">
									Copyright © 2018 <a href="#">Ren</a>. Designed by <a href="#">Spruko</a> All rights reserved.
								</div>
							</div>
						</div>
					</footer>
					<!-- End Footer-->
				</div>
			</div>
		</div>

		<!-- Back to top -->
		<a href="#top" id="back-to-top" style="display: inline;"><i class="fas fa-angle-up"></i></a>

		<!-- Dashboard js -->
		<script src="assets/js/vendors/jquery-3.2.1.min.js"></script>
		<script src="assets/js/vendors/bootstrap.bundle.min.js"></script>
		<script src="assets/js/vendors/jquery.sparkline.min.js"></script>
		<script src="assets/js/vendors/selectize.min.js"></script>
		<script src="assets/js/vendors/jquery.tablesorter.min.js"></script>
		<script src="assets/js/vendors/circle-progress.min.js"></script>
		<script src="assets/plugins/rating/jquery.rating-stars.js"></script>
		<!-- Side menu js -->
		<script src="assets/plugins/toggle-sidebar/js/sidemenu.js"></script>

		<!-- Custom scroll bar Js-->
		<script src="assets/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js"></script>

		<!-- 3Dlines-animation -->
        <script src="assets/plugins/3Dlines-animation/three.min.js"></script>
        <script src="assets/plugins/3Dlines-animation/projector.js"></script>
        <script src="assets/plugins/3Dlines-animation/canvas-renderer.js"></script>
        <script src="assets/plugins/3Dlines-animation/3d-lines-animation.js"></script>
        <script src="assets/plugins/3Dlines-animation/color.js"></script>

		<!--Counters -->
		<script src="assets/plugins/counters/counterup.min.js"></script>
		<script src="assets/plugins/counters/waypoints.min.js"></script>

		<!-- Custom js -->
		<script src="assets/js/custom.js"></script>

	</body>

<!-- Mirrored from spruko.com/demo/ren/Blue-animated/LeftMenu/profile.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 20 Sep 2018 05:04:18 GMT -->
</html>
