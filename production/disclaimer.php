<?php include 'inc.hdr.php';?>
<h1 class="highlight txt_center"><i>Disclaimer</i> Pendapatan</h1>

<p>SEBELUM ANDA TERUS MEMBACA APA-APA DI LAMAN WEB INI DAN BERSETUJU UNTUK MELAKUKAN SEBARANG PEMBELIAN PRODUK DI LAMAN WEB INI, SILA PASTIKAN ANDA TELAH MEMBACA KESELURUHAN TERMA DAN SYARAT DI BAWAH. ANDA BERTANGGUNGJAWAB SEPENUHNYA UNTUK MEMAHAMI SEGALA TERMA SYARAT SEBELUM MELAKUKAN SEBARANG PEMBELIAN BARANG.</p>

<p>JUTAWANAPP ADALAH SEBUAH APLIKASI MARKETING TOOL YANG BOLEH BERJALAN DI TELEFON ANDROID ANDA. SILA PASTIKAN TELEFON ANDA MENGGUNAKAN OS ANDROID UNTUK MENGGUNAKAN APLIKASI INI.</p>

<p>JUTAWANAPP BUKAN SEBUAH APA-APA PROGRAM, PROGRAM SKIM CEPAT KAYA, SKIM PELABURAN ATAU MLM YANG PERLU DI SERTAI UNTUK MENJANA PENDAPATAN. IA ADALAH PENJUALAN SEBUAH PRODUK BERUPA APLIKASI YANG BOLEH DI PASANG DI TELEFON BIMBIT ANDROID YANG BERFUNGSI UNTUK MENJADI SEBUAH MARKETING TOOL UNTUK PEMBELI.</p>

<p>JUTAWANAPP.COM MEMBUAT SEGALA USAHA UNTUK MEMASTIKAN IA MENERANGKAN DENGAN JELAS MENGENAI PRODUK DENGAN TEPAT DAN SEGALA POTENSI PENDAPATAN MEREKA. ANGGARAN PENDAPATAN DAN KEUNTUNGAN YANG DIBUAT OLEH JUTAWANAPP.COM DAN PELANGGANNYA ADALAH ANGGARAN TENTANG APA YANG JUTAWANAPP.COM FIKIR ANDA MUNGKIN BOLEH DAPAT DAN JUGA BERDASARKAN PENDAPATAN YANG DIPEROLEHI SENDIRI OLEH TUAN PUNYA LAMAN INI. TIADA SEBARANG JAMINAN BAHAWA ANDA AKAN DAPAT MEMBUAT TAHAP PENDAPATAN INI DAN ANDA TERIMA BAHAWA PENDAPATAN DAN KEUNTUNGAN DARI PENGGUNAAN APLIKASI ADALAH BERBEZA BAGI SETIAP INDIVIDU.</p>

<p>SEPERTI MANA-MANA PERNIAGAAN, HASIL ANDA MUNGKIN BERBEZA-BEZA, DAN AKAN BERDASARKAN KEUPAYAAN ANDA, PENGALAMAN PERNIAGAAN, KEPAKARAN DAN TAHAP KEINGINAN MASING-MASING. TIDAK ADA SEBARANG JAMINAN MENGENAI TAHAP KEJAYAAN YANG MUNGKIN DI CAPAI. CONTOH YANG DIGUNAKAN ADALAH BERDASARKAN HASIL YANG LUAR BIASA, YANG TIDAK BERLAKU UNTUK PEMBELI RATA-RATA, DAN TIDAK DIMAKSUDKAN UNTUK MEWAKILI ATAU MENJAMIN BAHAWA SESIAPA PUN AKAN MENCAPAI HASIL YANG SAMA ATAU SERUPA. KEJAYAAN SETIAP INDIVIDU BERGANTUNG PADA LATAR BELAKANG, DEDIKASI, KEINGINAN DAN MOTIVASI MEREKA.</p>

<p>TIDAK ADA JAMINAN BAHAWA CONTOH PENDAPATAN MASA LALU BOLEH DIDUPLIKASI UNTUK MASA AKAN DATANG. JUTAWANAPP.COM TIDAK DAPAT MENJAMIN HASIL DAN / ATAU KEJAYAAN MASA DEPAN ANDA. TERDAPAT BEBERAPA RISIKO YANG TIDAK DIKETAHUI DALAM PERNIAGAAN DAN DALAM PERNIAGAAN INTERNET YANG JUTAWANAPP.COM TIDAK DAPAT JANGKA YANG DAPAT MENGURANGKAN HASIL DARI APA YANG DI PAPARKAN DI LAMAN INI. JUTAWANAPP.COM TIDAK BERTANGGUNGJAWAB TERHADAP SETIAP TINDAKAN ANDA.</p>

<p>PENGGUNAAN MAKLUMAT, PRODUK DAN PERKHIDMATAN JUTAWANAPP.COM HARUS BERDASARKAN KETEKUNAN ANDA SENDIRI DAN ANDA BERSETUJU BAHAWA JUTAWANAPP.COM TIDAK BERTANGGUNGJAWAB TERHADAP KEJAYAAN ATAU KEGAGALAN PERNIAGAAN ANDA YANG SECARA LANGSUNG ATAU TIDAK LANGSUNG BERKAITAN DENGAN PEMBELIAN DAN PENGGUNAAN JUTAWANAPP MAKLUMAT, PRODUK DAN PERKHIDMATAN.</p>

<!--<p>SEBELUM ANDA TERUS MEMBACA APA-APA DI LAMAN WEB INI DAN BERSETUJU UNTUK MELAKUKAN SEBARANG PEMBELIAN PRODUK DI LAMAN WEB INI, SILA PASTIKAN ANDA TELAH MEMBACA KESELURUHAN TERMA DAN SYARAT DI BAWAH. ANDA BERTANGGUNGJAWAB SEPENUHNYA UNTUK MEMAHAMI SEGALA TERMA SYARAT SEBELUM MELAKUKAN SEBARANG PEMBELIAN BARANG.</p>

<p>JutawanApp.com make every effort to ensure that it accurately represent these products and services and their potential for income. Earning and Income statements made by JutawanApp.com and its customers are estimates of what JutawanApp.com think you can possibly earn. There is no guarantee that you will make these levels of income and you accept the risk that the earnings and income statements differ by individual.</p>

<p>As with any business, your results may vary, and will be based on your individual capacity, business experience, expertise, and level of desire. There are no guarantees concerning the level of success you may experience. The testimonials and examples used are exceptional results, which do not apply to the average purchaser, and are not intended to represent or guarantee that anyone will achieve the same or similar results. Each individual's success depends on his or her background, dedication, desire and motivation.</p>

<p>There is no assurance that examples of past earnings can be duplicated in the future. JutawanApp.com cannot guarantee your future results and/or success. There are some unknown risks in business and on the internet that JutawanApp.com cannot foresee which can reduce results. JutawanApp.com is not responsible for your actions.</p>

<p>The use of JutawanApp.com information, products and services should be based on your own due diligence and you agree that JutawanApp.com is not liable for any success or failure of your business that is directly or indirectly related to the purchase and use of JutawanApp.com information, products and services.</p>

<p>While every attempt has been made to verify the information on this web site and the product being sold, neither the product owners, distributors, agents or publishers assume any responsibility for any error, inaccuracies, and or omissions or assume any responsibility or liability whatsoever on behalf of any purchaser or reader of these materials. Any slight on people, organisations, companies or products are unintentional. The income statements and examples on this website are not intended to represent or guarantee that everyone will achieve the same results. Each individual&rsquo;s success will be determined by his or her desire, dedication, marketing background, effort and motivation to work and follow the program. There is no guarantee or duplicate results stated here. You recognise any business endeavor has inherent risk for loss of capital.</p>-->

<?php include 'inc.ftr.php';?>
