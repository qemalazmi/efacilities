<?php
error_reporting(0);
	session_start();
	include('inc.sanitize.php');
	include 'inc.config.php';
	include 'inc.functions.php';	
	include('class.captcha.php');
	
	// mod by mr. K on 7/1/08
	// 1. add if submitted process here
	// don't forget to sanitize
	// addded extra comments on 15/1/08 coz the coding is getting longer and harder to track! damn...

//----------------------------------------------------------------------
// first time enter via link outside either from mail or from html
// decode, sanitize information and verify entered from correct link and contain correct information
	
	$securecode=$_GET['enc'];
	$error_msg="";
	
	if ($securecode)
	{
	
		$unencoded_string=base64_decode($securecode);
		$arr_unencoded=explode("|",$unencoded_string);
		
		//code to clear session      		              
		$_SESSION['x'] = '';
		unset($_SESSION['x']);

		$_SESSION['x']=$securecode;

		$_SESSION['customer_email'] = '';
		unset($_SESSION['customer_email']);
	
		$_SESSION['customer_referrer'] = '';
		unset($_SESSION['customer_referrer']);
		
		$_SESSION['customer_rnd_amt'] = '';
		unset($_SESSION['customer_rnd_amt']);
		
		$_SESSION['customer_ref'] = '';
		unset($_SESSION['customer_ref']);

		// check if actual link is used to get to notification page, sanitize embedded email
		$session_name=sanitize($arr_unencoded[0],HTML, UTF8, SQL);
		$session_email=sanitize($arr_unencoded[1],HTML, UTF8, SQL);
		//$session_referrer=sanitize($arr_unencoded[2],PARANOID);
		$session_referrer=$arr_unencoded[2];
		$session_rnd_amt=sanitize($arr_unencoded[3],PARANOID);
		$session_type=sanitize($arr_unencoded[4],PARANOID);
		$session_bank=sanitize($arr_unencoded[5],PARANOID);
		$session_ref=sanitize($arr_unencoded[6],PARANOID);

		if ($session_referrer=="") $session_referrer="admin";
		if ($session_rnd_amt=="") $session_rnd_amt=0;
		
		// added by K, to show value of purchase 3/12/07
		if (preg_match("/^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/", $session_email)) {
			$_SESSION['customer_email']=$session_email;

		}else{
			// if email incorrect format
			echo "Sila masuk halaman ini melalui link di dalam email anda. <br>Klik <a href=index.php>di sini</a></p>";
			echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=5;URL=index.php>";
			exit();	
		}
		// sanitize embedded referrer
		$_SESSION['customer_referrer']=$session_referrer;
		
	}else{
		echo "Sila masuk halaman ini melalui link di dalam email anda. <br>Klik <a href=index.php>di sini</a></p>";
		echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=5;URL=index.php>";
		exit();	
	}

//----------------------------------------------------------------------
// will be processed if user comes from outside link or submit the form
// grab information from dbase and used to fill information on form
// make sure user exists in clients table, if not kick to index

	mysql_connect($db_host, $db_user, $db_pass) 
		or die ("Database CONNECT Error"); 
	$query="SELECT id, name, email FROM clients WHERE email='$session_email'";
	$result = mysql_db_query($db, $query) 
		or die ("Database INSERT Error : 002"); 

	$exist=mysql_num_rows($result);
	
	if ($exist > 0)
	{
		$myq=mysql_fetch_array($result);
	} else {
		echo "Sila masuk halaman ini melalui link di dalam email anda. <br>Klik <a href=index.php>di sini</a></p>";
		echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=5;URL=index.php>";
		exit();	
	}

//----------------------------------------------------------------------
// user submitted the form
// decode and sanitize information from form
// verify that user entered from correct link 
// verify that form submitted contain correct information

	$process=$_POST['sudah'];
	$payment=$_POST['bayaran'];
	//$payment_type=$myq['payment'];
	$pay_amt=$g_prod_price.".".sprintf("%02d", $session_rnd_amt);
	$paid_amt=$_POST['a1'];

	if ($process=="1")
	{
		// initialize info
		$c_time = date("Y-m-d H:i:s",(time()+(3600*13)));
		$c_refid=$session_referrer;
		$c_name=$session_name;
		$c_email=$session_email;
		
		// check captcha	
		$s_code = sanitize($_POST['captchacode'], PARANOID);
	
		$img = new Securimage();
		$valid = $img->check($s_code);
		
		if($valid == true) {
		//echo "<center>Thanks, you entered the correct code.</center>";
		} else {
			$error_msg.="Sila masukkan kod captcha yang betul.<br>";
		}

		// check date, month, year
		$d_date_dd=sanitize($_POST['hari'],INT);
		$d_date_mm=sanitize($_POST['bulan'],INT);
		$d_date_yy=sanitize($_POST['tahun'],INT);
		
		if (eregi ("[0-9]", $d_date_dd))
		{
			if ($d_date_dd>31 || $d_date_dd<1) 
			{
				$d_error=1;
			}
		}else{  
			$d_error=2;	
		}   
		
		if (eregi ("[0-9]", $d_date_mm))
		{
			if ($d_date_mm>12 || $d_date_mm<1) 
			{
				$d_error=3;
			}
		}else{   
			$d_error=4;	
		}   

		if (eregi ("[0-9]", $d_date_yy))
		{
			if ($d_date_yy<1) 
			{
				$d_error=36;
			}
		}else{   
			$d_error=46;	
		} 
		
		if ($d_date_dd==1 && $d_date_mm==1 && $d_date_yy==2008)
		{ 
			$d_error=47;	
		}
		
		if ($d_error>0)
		{ 
			$error_msg.="Sila masukkan tarikh yang betul.<BR>";
		}
		
		// check hour, minute
		$t_date_hr=sanitize($_POST['jam'],INT);
		$t_date_mn=sanitize($_POST['minit'],INT);

		if (eregi ("[0-9]", $t_date_hr))
		{
			if ($t_date_hr>23 || $t_date_hr< 0) 
			{ 
				//$t_error=7;
			}
		}else{ 
			//$t_error=8; 
			}   
		
		if (eregi ("[0-9]", $t_date_mn))
		{
			if ($t_date_mn>59 || $t_date_mn< 0) 
			{ 
				//$t_error=9;
			}
		}else{
			//$t_error=10; 
		}  
		
		if ($t_date_hr==0 && $t_date_mn==0)
		{ 
			//$t_error=11;	
		}
		
		if ($t_error>0)
		{ 
			$error_msg.="Sila masukkan masa yang betul.<BR>";
		}

		if ($payment=='NONE')
		{ 
			$error_msg.="Sila pilih cara bayaran yang betul.<BR>";
		}
		
		// UPLOAD IMAGE & PDF	- 1162017
		$image_uploaded_info="";
		if ( isset( $_FILES['pictures'] ) ) {
			//echo '2.<pre>';
			//var_dump($_FILES['pictures']);
			//echo '</pre><br>';
			
			$imageFileType = pathinfo($_FILES['pictures']['name'],PATHINFO_EXTENSION);
			
			//echo '3.filetype'.$imageFileType.'<br>';
			
			if ($imageFileType == "pdf") {
			
				$oldFileName=$_FILES["pictures"]["name"];		

				$allowedExts = array("pdf");
				$foo=explode(".", $oldFileName);
				$extension = end($foo);
				//echo $extension;

				if ((($_FILES["pictures"]["type"] == "application/pdf")) && ($_FILES["pictures"]["size"] < 5242880) && in_array($extension, $allowedExts)) {

					if ($_FILES["pictures"]["error"] > 0) {
						//echo "Return Code: " . $_FILES["pictures"]["error"] . "<br>";
						$error_msg.="PDF bermasalah untuk diupload.<br>";
					} else {
						//echo "4.you have successfully upload your note" . "<br>";
						
						do {
							  $newFileName=date('YmdHis-'). substr( sha1(uniqid (rand())), 0, 10).'.'.$imageFileType;
						} while(file_exists("C4Mad3y/" . $newFileName));

						if (file_exists("C4Mad3y/" . $newFileName)) {
							//echo $_FILES["pictures"]["name"] . " the file you are trying to upload is already exists. ";
							$error_msg.="PDF bermasalah untuk diupload.<br>";
						} else {
							move_uploaded_file($_FILES["pictures"]["tmp_name"], "C4Mad3y/" . $newFileName);
							$image_uploaded_info="C4Mad3y/" . $newFileName;
							//echo "5.you have succesfully upload your note! ".$newFileName;
						}
					}
				}
			} else {	
				// UPLOAD pictures
				
				require_once  "bulletproof.php";

				$pictures = new Bulletproof\Image($_FILES);
				
				$oldFileName=$pictures->getName();
				
				$pictures->setName(date('YmdHis-'). substr( sha1(uniqid (rand())), 0, 10))
					  ->setSize(10240, 5242880)
					  ->setMime(array('jpeg', 'jpg', 'png', 'gif'))
					  ->setLocation("C4Mad3y");

				if($pictures["pictures"]){
					
					if ($pictures['error']==1) {
						
						$error_msg.="Gambar bermasalah untuk diupload.<br>";
						
					} else {
					
						$upload = $pictures->upload(); 
						
						if($upload){
							//echo $upload->getFullPath(); // bulletproof/cat.gif
							
							//$myFile = "xlog/uploads.log";
							//file_put_contents($myFile, date('Y-m-d H:i:s')."\r\n Email:".$session_email.
							//", Path: ".$upload->getFullPath()."\r\n\r\n", FILE_APPEND);	
							
							$image_uploaded_info=$upload->getFullPath();

						}else{
							//echo $pictures["error"]; 
							//$myFile = "xlog/uploads.log";
							//file_put_contents($myFile, date('Y-m-d H:i:s')."\r\n Email:".$session_email.
							//", Error: ".$pictures["error"]."\r\n\r\n",FILE_APPEND);	
							$error_msg.="Gambar bermasalah untuk diupload.<br>";
						}
					}
				}
				
				// END UPLOAD
			}// endif file is pictures
		}//UPLOAD IMAGE & PDF	- 1162017
		
/*		
		// UPLOAD IMAGE
		$image_uploaded_info="";
		require_once  "bulletproof.php";

		$image = new Bulletproof\Image($_FILES);
		
		$oldFileName=$image->getName();
		
		$image->setName(substr( sha1(uniqid (rand())), 0, 10))
			  ->setSize(10240, 5242880)
			  ->setMime(array('jpeg', 'jpg', 'png', 'gif'))
			  ->setLocation("C4Mad3y");

		if($image["pictures"]){
			
			$upload = $image->upload(); 
			
			if($upload){
				//echo $upload->getFullPath(); // bulletproof/cat.gif
				
				$myFile = "xlog/uploads.log";
				file_put_contents($myFile, date('Y-m-d H:i:s')."\r\n Email:".$session_email.
				", Path: ".$upload->getFullPath()."\r\n\r\n", FILE_APPEND);	
				
				$image_uploaded_info=$upload->getFullPath();

			}else{
				//echo $image["error"]; 
				$myFile = "xlog/uploads.log";
				file_put_contents($myFile, date('Y-m-d H:i:s')."\r\n Email:".$session_email.
				", Error: ".$image["error"]."\r\n\r\n",FILE_APPEND);	
				$error_msg.="Gambar bermasalah untuk diupload.<br>";
			}
		}
		
		// END UPLOAD
*/
		
		// process method payment
		// NOTE: no checking of input
		
		if ($session_type=='mbb' || $session_type=='bank') 
		{ 
			$s_payment_type=$session_type;
			
			$s_bankname=$_POST['ibbank'];
			
			if ($payment=="Interbank" && $s_bankname=="NONE") 
			{ 
				$error_msg.="Sila pilih bank pembayar.<br>";
			}
			
			$s_name=sanitize($_POST['penama'],HTML, UTF8, SQL);
			//$s_acct=sanitize($_POST['noakaun'],PARANOID);
			$s_branch=sanitize($_POST['kauntercaw'],PARANOID);

			$c_comment=$bank_co_name[$session_bank].', '.$bank_co_mbb_acct[$session_bank].' | '.$s_bankname." | ".$payment." | ".$s_name." | ".$s_branch." | ".$paid_amt." / ".$pay_amt." | ".$session_ref.' | <a href="../'.$image_uploaded_info.'" target="_blank">Gambar Bukti Bayaran</a>';		
		}
		
		if ($session_type=='pos' ) 
		{ 
			$s_payment_type=$session_type;
			
			$s_serial=sanitize($_POST['postalsiri'],HTML, UTF8, SQL);

			$c_comment=$payment." | ".$s_serial;				
		}

		$c_method=$s_payment_type;
		



		//----------------------------------------------------------------------	
		// if all okay
		if ($error_msg=="")
		{
			$date_deposit=sprintf("%04d",$d_date_yy)."-".sprintf("%02d",$d_date_mm)."-".sprintf("%02d",$d_date_dd);
			$time_deposit=sprintf("%02d",$t_date_hr).":".sprintf("%02d",$t_date_mn).":01";

			// query to make sure no redundancy or multiple try, if email is in deposits table, exit
			$q="SELECT email FROM deposits WHERE email='$c_email' AND is_read<9";
			$result=mysql_db_query($db, $q)
				or die (mysql_error());
			$d_exist=mysql_num_rows($result);
					
			if ($d_exist > 0)
			{ 
				include 'inc.hdr.php';

				?>
				<h1 class="highlight txt_center">Permohonan Anda Masih Diproses</h1>
				<p>Butiran pembayaran anda yang dihantar sebelum ini masih di dalam proses verifikasi. Jika terdapat kesilapan semasa mengisi borang notifikasi ini, sila hubungi saya melalui sistem bantuan. Anda akan dapat membetulkan kesilapan di dalam borang notifikasi selepas itu.</p>
				<p>Email saya mungkin masuk ke SPAM atau BULK folder anda, jadi pastikan anda memeriksa folder-folder tersebut. Jika tiada sebarang email yang sampai, <a href="contact.php">sila hubungi saya</a>.</p>
<?php
				include 'inc.ftr.php';
				exit();
			
			}
							
			if ($d_exist==0)
			{
				$sql="INSERT INTO deposits (ipaddress,email,name,refid,method,date_deposit,
				time_deposit,komen,datetime) VALUES ('$clientip','$c_email','$c_name','$c_refid','$c_method',
				'$date_deposit','$time_deposit','$c_comment','$c_time')";
				mysql_db_query($db, $sql)
				or die (mysql_error());

				include 'inc.hdr.php';

				?>
				<h1 class="highlight txt_center">Terima Kasih...Permohonan Anda Sedang Diproses</h1>
<p>Sila berikan saya masa antara sehari ataupun tiga hari jika hari cuti untuk melakukan verifikasi pembayaran anda.</p>
<p>Butiran untuk download akan dihantar melalui email. Email saya mungkin masuk ke SPAM atau BULK folder anda, jadi pastikan anda memeriksa folder-folder tersebut. Jika tiada sebarang email yang sampai, terdapat masalah seperti cara bayaran tidak sama, nilai sen tidak sama dan sebagainya, <a href="contact.php">sila hubungi saya</a>.</p>
<?php

				include ('inc.mailout.php');
				$mailsuccess=mailnotificationconfirm($c_email);

				include 'inc.ftr.php';
				exit();
			
			}
		}
		//----------------------------------------------------------------------	
		// end if all okay

	}

//----------------------------------------------------------------------

	$nu_securecode=base64_encode($session_email."|".$session_referrer."|".$session_rnd_amt);	
	$pay_amt=$g_prod_price.".".sprintf("%02d", $session_rnd_amt);
	
	include 'inc.hdr.php';
	
?>
<h1 class="highlight txt_center">Borang Notifikasi</h1>
<p>Tolong jangan isi borang ini jika anda belum lakukan pembayaran ataupun baru bercadang melakukan pembayaran. Sila bayar dahulu baru isi. Pastikan semua maklumat betul. </p>
<p>Anda akan dihubungi melalui email di bawah ini untuk penghantaran produk. Jika tiada sebarang email yang sampai, terdapat masalah seperti cara bayaran tidak sama, nilai sen tidak sama dan sebagainya, <a href="contact.php">sila hubungi saya</a>.</p>

<FORM id="proses" name="proses" method="post" enctype="multipart/form-data" action="notify.php?enc=<?php echo $securecode;?>" class="container">
	<TABLE width="100%" border="0" cellpadding="10" cellspacing="10">
		
		<TR>
			<TD valign="top">
			<?php if ($error_msg!="") echo "<p><font color=red><b>".$error_msg."</b></font></p>"; ?>
			
			<strong>Nama Pembeli</strong><br />
				<input name="a1" type="text" value="<?php echo $myq['name'];?>" size="65" / disabled></TD>
		</TR>
		
		<TR>
			<TD valign="top"><strong>Email Pembeli</strong><br />
				<input name="a2" type="text" value="<?php echo $myq['email'];?>" size="65" / disabled></TD>
		</TR>
		
		<TR>
			<TD valign="top">			<strong>Bayaran Ke</strong><br />
				<input name="a3" type="text" value="<?php
				//echo $session_type;
				//echo $bank_co_name[$session_bank].', Maybank('.$bank_co_mbb_acct[$session_bank].')';
				//echo $bank_co_name[$session_bank].', '.$bank_co_mbb_acct[$session_bank];
				
			if ($session_type=='mbb')
			{ 
				echo $bank_co_name[$session_bank].', Maybank('.$bank_co_mbb_acct[$session_bank].')';
			} else if ($session_type=='cimb')
			{ 
				echo $bank_co_name[$session_bank].', '.$bank_co_mbb_acct[$session_bank];
			} else if ($session_type=='pos')
			{
				echo 'Pos';
			} else {
				echo $bank_co_name[$session_bank].', '.$bank_co_mbb_acct[$session_bank];
			}
			?>" size="65" / disabled></TD>
		</TR>
		<TR>
			<TD valign="top"><strong>Cara Pembayaran Dibuat</strong><br />
				<SELECT name="bayaran" id="select">
				<OPTION value="NONE" >PILIH CARA BAYARAN</OPTION>
<?php
			if ($session_type=='mbb' || $session_type=='cimb' || $session_type=='bank')
			{
?>
				<OPTION value="CDM" >Mesin Deposit Tunai</OPTION>			
				<OPTION value="ATM" >Mesin ATM</OPTION>
				<OPTION value="Kaunter" >Kaunter</OPTION>
				<OPTION value="Interbank" >Interbank</OPTION>	                
<?php
				//if ($session_type=='cimb')
				//{
?>
				<OPTION value="Maybank2U" >Maybank2U</OPTION>
				<OPTION value="CIMBClicks" >CIMB Clicks</OPTION>
				<!--<OPTION value="CDM" >CDM</OPTION>-->
<?php
				//} else {
?>
				<!--<OPTION value="Maybank2U" >Maybank2U</OPTION>-->
<?php
				//}
			}
			if ($session_type=='pos')
			{
?>
				<OPTION value="Kiriman Wang" >Kiriman Wang</OPTION>
				<OPTION value="Wang Pos" >Wang Pos</OPTION>
				<OPTION value="Tunai" >Tunai</OPTION>
<?php
}
?>		
			</SELECT></TD>
		</TR>
		<TR>
			<TD valign="top"><strong>Amaun Dibayar</strong><br />
				<input name="a1" type="text" value="<?php echo $pay_amt;?>" size="40" style="background-color:#ffffff;"/>
				<span class="txt_smaller_75">Masukkan amaun sebenar yang anda bayar sekiranya amaun tersebut berbeza dari di atas.</span>	
			</TD>
		</TR>
<?php if ($session_type=='mbb' || $session_type=='cimb' || $session_type=='bank') { ?>
		<tr>
			<td>
				<strong>Bank Pembayar</strong><br />
				<select name="ibbank" id="ibbank">
					<option value="NONE">PILIH BANK ANDA</option>
					<option value="MBB" selected>MAYBANK</option>
					<option value="CIMB">CIMB BANK</option>
					<option value="PUB">PUBLIC BANK</option>
					<option value="BI">BANK ISLAM MALAYSIA</option>
					<option value="BR">BANK RAKYAT</option>
					<option value="RHB">RHB BANK</option>
					<option  value="AFFIN">AFFIN BANK BERHAD / AFFIN ISLAMIC BANK</option>
					<option  value="ALLIANCE">ALLIANCE BANK MALAYSIA BERHAD</option>
					<option  value="AMBANK">AmBANK BERHAD</option>
					<option  value="MUAMALAT">BANK MUAMALAT</option>
					<option  value="BSN">BANK SIMPANAN NASIONAL BERHAD</option>
					<option  value="CITIBANK">CITIBANK BERHAD</option>
					<option  value="DEUTSCHE">DEUTSCHE BANK (MSIA) BERHAD</option>
					<option  value="EON">EON BANK / EONCAP ISLAMIC BANK</option>
					<option  value="HLBANK">HONG LEONG BANK</option>
					<option  value="HSBC">HSBC BANK MALAYSIA BERHAD</option>
					<option  value="OCBC">OCBC BANK(MALAYSIA) BHD</option>
					<option  value="STANCHART">STANDARD CHARTERED BANK MALAYSIA BERHAD</option>
					<option  value="UOB">UNITED OVERSEAS BANK BERHAD</option>
				</select>			</td>
		</tr>
		<TR>
			<TD valign="top"><strong>Penama Akaun</strong><br />
				<input name="penama" type="text" id="nama" value="<?php echo $myq['name'];?>" size="65" maxlength="50"  style="background-color:#ffffff;"/>
			
			<span class="txt_smaller_75">Nama <strong>Anda</strong> ATAU <strong>Pemegang Akaun Pembayar</strong> 
			(cth: Azhar Idrus)</span></TD>
		</TR>
<?php
		//if ($session_type=='mbb')
		if ($session_type!='')
		{
?>
		<TR>
			<TD valign="top"><strong>Cawangan Pembayaran Dibuat</strong><br />
				<input name="kauntercaw" type="text" id="kauntercaw" value="<?php echo $_POST['kauntercaw'];?>" size="65" maxlength="50"  style="background-color:#ffffff;"/>
			
			<span class="txt_smaller_75">Jika bayaran dilakukan di mesin deposit tunai, mesin ATM atau kaunter. Jangan lupa hantar snapshot resit yang mempunyai tulisan email anda.</span></TD>
		</TR>

<?php 		}
	} 
	if ($session_type=='pos') { ?>
		<TR>
			<TD valign="top"><strong>No Siri</strong><br />
				<input name="postalsiri" type="text" id="textfield11" value="<?php echo $_POST['postalsiri'];?>" size="65" maxlength="20" /> 
				<br />
			(bermula dengan huruf A - Z di <strong>BAHAGIAN ATAS PENJURU KANAN</strong>)</TD>
		</TR>
<?php } ?>		

		<TR>
			<TD valign="top">
			
				<table width="90%" border="0">
					<tr>
						<td><strong>Tarikh Bayaran</strong></td>
						<td><strong>Masa Bayaran</strong></td>
					</tr>
					<tr>
						<td>						
							<table border="0">
								<tr>
									<td><span class="edit">Hari</span></td>
									<td><span class="edit">Bulan</span></td>
									<td><span class="edit">Tahun</span></td>
								</tr>
								<tr>
									<td><input name="hari" type="text" id="hari" value="<?php echo date('d');?>" size="3" maxlength="2"  style="background-color:#ffffff;"/></td>
									<td><input name="bulan" type="text" id="bulan" value="<?php echo date('m');?>" size="3" maxlength="2"  style="background-color:#ffffff;"/></td>
									<td><input name="tahun" type="text" id="tahun" value="<?php echo date('Y');?>" size="5" maxlength="4"  style="background-color:#ffffff;"/></td>
								</tr>
							</table>
						</td>
						<td>
							<table border="0">
								<tr>
									<td><span class="edit">Jam</span></td>
									<td><span class="edit">Minit</span></td>
								</tr>
								<tr>
									<td><input name="jam" type="text" id="jam" value="00" size="3" maxlength="2"  style="background-color:#ffffff;"/></td>
									<td><input name="minit" type="text" id="minit" value="00" size="3" maxlength="2"  style="background-color:#ffffff;"/></td>
								</tr>
							</table>					
						
						</td>
					</tr>
				</table>
			</TD>
		</TR>
		
		<tr valign="top">
			<td valign="top"><b>Kod Penaja Anda</b><br>
			<input id="test" type="text" size="20" maxlength="10" value="<?php echo $session_referrer; ?>" disabled=disabled/><br>
			<span class="txt_smaller_75">Sekiranya anda diperkenalkan oleh penaja, sila pastikan kod penaja anda betul seperti di atas.</span></td>
		</tr>
		
		<tr valign="top">
			<td valign="top"><b>Upload File Bukti Bayaran Anda Di Sini</b><br>
			<input type="file" name="pictures"  style="background-color:#ffffff;"/><br>
			<span class="txt_smaller_75">Gambar resit ATM, CDM, ScreenShot Transfer, dan lain-lain (hanya PDF, JPG, PNG atau GIF diterima). Jika bayaran dilakukan di mesin deposit tunai, mesin ATM atau kaunter, jangan lupa hantar snapshot resit yang mempunyai tulisan email anda.</span></td>
		</tr>
		
		<tr valign="top">
			<td valign="top"><img src="captcha.php?sid=<?php echo md5(uniqid(time())); ?>" /><br />
			Masukkan kod captcha di atas<br />
			<input name="captchacode" type="text" size="20" maxlength="10"  style="background-color:#ffffff;"/></td>
		</tr>
		
		<TR>
			<TD valign="top">
			<INPUT type="hidden" name="sudah" value="1" />
			<INPUT type="hidden" name="reference" value="<?php echo $session_ref;?>" />
			<INPUT type="submit" name="Submit" id="button" value="Submit"  class="btn"/>
			<!--<INPUT type="reset" name="reset" id="reset" value="Reset"  class="btn"/>--></TD>
		</TR>
		<TR>
			<TD>&nbsp;</TD>
		</TR>
	</TABLE>
</FORM>
<?php
include 'inc.ftr.php';
?>