<?php 

	session_start();
	include('inc.sanitize.php');
	include 'inc.config.php';
	include 'inc.functions.php';
	//include('class.captcha.php');
	
	// mod by mr. K on 7/1/08
	// 1. add if submitted process here
	// don't forget to sanitize
	
	$securecode=$_GET['enc'];
	$error_msg="";
	
	if ($securecode)
	{
	
		$unencoded_string=base64_decode($securecode);
		$arr_unencoded=explode("|",$unencoded_string);
		//echo $unencoded_string;
		//code to clear session      		              
		//thurdegree@gmail.com|admin|46|7
		$_SESSION['x'] = '';
		unset($_SESSION['x']);

		$_SESSION['x']=$securecode;

		$_SESSION['customer_email'] = '';
		unset($_SESSION['customer_email']);
	
		$_SESSION['customer_referrer'] = '';
		unset($_SESSION['customer_referrer']);
		
		$_SESSION['customer_rnd_amt'] = '';
		unset($_SESSION['customer_rnd_amt']);
		
		$_SESSION['customer_rnd_bank'] = '';
		unset($_SESSION['customer_rnd_bank']);
		
		$_SESSION['customer_ref'] = '';
		unset($_SESSION['customer_ref']);

		// check if actual link is used to get to notification page, sanitize embedded email
		$session_email=sanitize($arr_unencoded[0],HTML, UTF8);
		$session_referrer=sanitize($arr_unencoded[1],PARANOID);
		//$session_referrer=$arr_unencoded[1];

		if ($session_referrer=="") $session_referrer="admin";
		
		$session_rnd_amt=sanitize($arr_unencoded[2],HTML, UTF8);

		//if ($session_rnd_amt=="" || $session_rnd_amt<5) $session_rnd_amt=4;
		
		$session_bank=sanitize($arr_unencoded[3],INT);
		
		$bank_no = substr($arr_unencoded[3], 0, 1);
		$rand_no = substr($arr_unencoded[3], 1, 4);

		// echo $session_email.$session_referrer.$session_rnd_amt;
		
		// added by K, to show value of purchase 3/12/07
		if (preg_match("/^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/", $session_email)) {
			$_SESSION['customer_email']=$session_email;
			
			
		}else{
			// if email incorrect format
			echo "Sila masuk halaman ini melalui link di dalam email anda. <br>Klik <a href=index.php>di sini</a></p>";
			echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=5;URL=index.php>";
			exit();	
		}
		// sanitize embedded referrer
		$_SESSION['customer_referrer']=$session_referrer;
		
	}else{
		echo "Sila masuk halaman ini melalui link di dalam email anda. <br>Klik <a href=index.php>di sini</a></p>";
		echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=5;URL=index.php>";
		exit();	
	}

	$link = mysql_connect($db_host, $db_user, $db_pass) 
		or die ("Database CONNECT Error"); 

	if($link == false){
		//try to reconnect
	}
	
	//mysql_connect($db_host, $db_user, $db_pass) 
	//	or die ("Database CONNECT Error"); 
	$query="SELECT id, name, email FROM clients WHERE email='$session_email'";
	$result = mysql_db_query($db, $query) 
		or die ("Database INSERT Error : 002"); 

	$exist=mysql_num_rows($result);
	
	if ($exist > 0)
	{
		$myq=mysql_fetch_array($result);
	} else {
		echo "Sila masuk halaman ini melalui link di dalam email anda. <br>Klik <a href=index.php>di sini</a></p>";
		echo "<META HTTP-EQUIV=\"Refresh\" CONTENT=5;URL=index.php>";
		exit();	
	}

	// change reference number here
	
	//$session_ref='JA'.$bank_no.sprintf("%02d",$session_rnd_amt).substr(date('yn'),1,2).
	//sprintf("%04d",$g_prod_price+$session_bank+$session_rnd_amt);
	
	$session_ref='JA'.substr(date('yn'),1,2).sprintf("%02d",$session_rnd_amt).$bank_no.sprintf("%03d",$rand_no);
	
	// end
	

	$pp_securecode=base64_encode($myq['email']."|".$session_referrer);

	$nu_securecode_mbb=base64_encode($myq['name']."|".$myq['email']."|".$session_referrer."|".
	$session_rnd_amt."|bank|".$bank_no."|".$session_ref);
	
	
	
	
	$nu_securecode_cimb=base64_encode($myq['name']."|".$myq['email']."|".$session_referrer."|".$session_rnd_amt."|cimb|".$session_ref);
	$nu_securecode_pos=base64_encode($myq['name']."|".$myq['email']."|".$session_referrer."|0|pos|".$session_ref);
	
	include 'inc.hdr.php';
	
?>

<h1 class="highlight txt_center">Cara Melakukan Pembayaran</h1>


<p><b><?php echo "SALAM ".strtoupper($myq['name']);?></b></p>

<p>Anda boleh buat bayaran melalui Maybank2U, CIMBClicks, Interbank Transfer (IBG), Instant Transfer (IBFT), Mesin Deposit Tunai (CDM), Mesin ATM, MEPS atau kaunter bank.</p>

<!--<p>Untuk pembayaran melalui mesin deposit tunai, mesin ATM ataupun kaunter, anda perlu menghantar scan/emel salinan resit ke alamat email berikut: <a href="mailto:bayaran@jutawanapp.com">bayaran@jutawanapp.com</a></p>

<p>Anda boleh mengambil gambar dengan telefon bimbit anda dan hantar terus dari sana ke alamat e-mail di atas.</p>

<p>Untuk bayaran CIMBClicks / Maybank2U. Sila hantar juga "screen capture" ke bayaran@jutawanapp.com jika anda menggunakan Internet Banking.</p>-->

<p>Pastikan anda menghantar gambar bukti bayaran, <i>contohnya snapshot resit / skrin bayaran yang TELAH dibuat dengan kamera telefon bimbit</i>, semasa mengisi borang <b>Notifikasi Bayaran</b>. Jika pembayaran dibuat melalui <b>Mesin Deposit Tunai, Mesin ATM</b> ataupun <b>Kaunter</b>, <b><u>anda perlu tuliskan email anda pada resit</u></b> sebelum snapshot dan menghantar gambar bukti bayaran.</p>

<p>Jika anda ingin melakukan bayaran melalui bank lain sila gunakan khidmat INSTANT interbank transfer bagi mengelakkan kelewatan menerima produk.</p>

<p>Pastikan anda telah bersetuju dengan segala <a href="terms.php"><strong>terma & syarat</strong></a> sebelum melakukan pembayaran.</p>

<p>Butiran akaun BANK syarikat saya adalah seperti berikut:</p>

<hr>

<p>Nama : <b><?php echo $bank_co_name[$bank_no];?></b></p>

<p>Nombor Akaun: <b><?php echo $bank_co_mbb_acct[$bank_no];?></b></p>

<!--<p>Nombor Akaun CIMB : <b><?php echo $bank_co_cimb_acct[$bank_no];?></b></p>-->

<!--<p>Alamat E-mail : <b>bayaran@jutawanapp.com</b></p>-->

<p>Kod Penaja Anda : <b><?php echo $session_referrer;?></b></p>

<p>Reference : <b><?php echo $session_ref;?></b> <?php //echo $session_bank.'|'.$bank_no.'|'.$rand_no;?></p>

<p><img src="images/notif-form.png"></p>

<p>Jumlah PERLU dibayar: <b>RM<?php echo $g_prod_price;?>.<?=sprintf("%02d",$session_rnd_amt);?></b></p>

<p>(Amaun <b><?=sprintf("%02d",$session_rnd_amt);?> sen</b> ini adalah untuk memudahkan saya mengenali transaksi anda)</p>

<p class="txt_smaller_75">* Jika bayaran melalui deposit tunai CDM, hanya bayar RM<?php echo $g_prod_price;?>, <b><u>jangan lupa tuliskan email anda pada resit, ambil gambar dan hantarkan kepada saya</u></b> melalui borang Notifikasi Bayaran</p><!-- dan email slip deposit kepada bayaran@jutawanapp.com</p>-->

<hr>

<p>Selepas anda membuat bayaran, anda WAJIB mengisi <b>Borang Notifikasi</b> di bawah secepat mungkin.</p>

<!--<h5 id="notify" class="txt_bigger_150"><a href="notify.php?enc=<?=$nu_securecode_mbb;?>"><strong>Klik Sini Untuk Mengisi Borang Notifikasi Saya&gt;&gt;</strong></a></h5>-->
<br>

<p><center><a href="notify.php?enc=<?=$nu_securecode_mbb;?>" class="btn" style="text-decoration:none;"><strong>Klik Sini Untuk Mengisi Borang Notifikasi</strong></a></center></p>

<br>

<p>Kelewatan mengisi borang notifikasi selepas membuat bayaran akan melambatkan proses penghantaran produk saya kepada anda.</p>


<br><br>
<?php
include 'inc.ftr.php';
?>