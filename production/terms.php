<?php
	include 'inc.hdr.php';
?>
<h1 class="highlight txt_center">Terma & Syarat</h1>


<p>SEBELUM ANDA TERUS MEMBACA APA-APA DI LAMAN WEB INI
DAN BERSETUJU UNTUK MELAKUKAN SEBARANG PEMBELIAN
PRODUK DI LAMAN WEB INI, SILA PASTIKAN ANDA TELAH
MEMBACA KESELURUHAN TERMA DAN SYARAT DI BAWAH. ANDA
BERTANGGUNGJAWAB SEPENUHNYA UNTUK MEMAHAMI
SEGALA TERMA SYARAT SEBELUM MELAKUKAN SEBARANG
PEMBELIAN BARANG.
</p>
<b>1. MAKLUMAT UNDANG-UNDANG: TERMA DAN SYARAT</b>
<p>
Laman ini dimiliki dan dikendalikan oleh JutawanApp.com. Akses anda,
penggunaan laman dan penggunaan bahan dalam laman adalah
tertakluk kepada terma dan syarat (&quot;Terma dan Syarat&quot;) pada halaman
ini, dan semua undang-undang yang berkenaan. Dengan mengakses dan
melayari laman ini, anda menerima, tanpa batasan atau kelayakan,
Terma dan Syarat ini. Jika anda tidak bersetuju dengan mana-mana
Terma dan Syarat di bawah, jangan gunakan Laman ini dan jangan
membeli produk-produk kami. JutawanApp.com berhak, mengikut budi
bicara mutlaknya, untuk mengubahsuai, mengubah atau
memperbaharui Terma dan Syarat ini pada bila-bila masa dan dengan
menggunakan laman ini, anda bersetuju untuk terikat dengan
pengubahsuaian, perubahan atau kemas kini sedemikian.
</p>

<b>2. HAK CIPTA DAN TRADEMARKS</b>
<p>
Dokumen dan maklumat di Laman ini adalah bahan hak cipta
JutawanApp.com. Maklumat hak cipta yang terkandung dalam domain
ini tidak boleh diterbitkan, diedarkan atau disalin secara terbuka dalam
apa jua cara, termasuk Internet, e-mel, media sosial, atau dicetak
semula. Sesiapa yang melanggar ini akan dikenakan denda maksimum

dan penalti yang dikenakan oleh undang-undang. Pembeli produk kami
diberi lesen untuk menggunakan bahan yang terkandung di sini untuk
kegunaan peribadi mereka sendiri sahaja. Tiada
tuntutan hak cipta dibuat pada mana-mana perisian pihak ketiga,
laman web, imej atau teks yang boleh dirujuk dalam bahan kami.
Dengan melihat dan / atau membeli bahan-bahan di laman web kami,
anda bersetuju untuk terikat oleh terma hak cipta ini.</p>

<b>3. BATASAN LIABILITI</b>
<p>
Maklumat dan apa-apa perisian yang boleh dimuat turun di laman web
ini disediakan &quot;Seperti Yang Di Lihat&quot; dan akses ke app dan sistem JutawanApp disediakan berdasarkan &quot;usaha terbaik.&quot;(Best Effort) tanpa jaminan apa-apa jenis,
sama ada dinyatakan atau tersirat.
Walaupun kami berusaha untuk ketepatan, maklumat di laman ini
mungkin mengandungi ketidaktepatan teknikal yang tidak sengaja atau
kesilapan tipografi. Maklumat boleh diubah atau dikemaskini tanpa
notis. JutawanApp.com juga boleh membuat penambahbaikan dan /
atau perubahan dalam produk dan / atau program / tawaran yang
diterangkan dalam maklumat ini pada bila-bila masa tanpa notis.
JutawanApp.com tidak membuat apa-apa gambaran tentang mana-
mana laman web pihak ketiga yang boleh diakses melalui Laman atau
produk kami. Laman pihak ke-3 ini tidak dicipta atau dikendalikan oleh
JutawanApp.com. Mereka bebas daripada JutawanApp.com, dan
JutawanApp.com tidak mempunyai kawalan ke atas kandungan di laman

web tersebut. Lebih-lebih lagi, JutawanApp.com tidak menyokong atau
menerima apa-apa tanggungjawab untuk kandungan, atau penggunaan,
laman web tersebut. Walaupun kami tidak mempunyai maklumat
mengenai kehadiran bahaya seperti di laman web pihak ke-3, anda perlu
mengambil langkah berjaga-jaga yang diperlukan untuk melindungi diri
anda dan sistem anda daripada virus, worm, kuda Trojan, dan barangan
lain yang boleh merosakkan peralatan anda.
JutawanApp.com tidak akan bertanggungjawab kepada anda atau mana-
mana pihak ketiga lain untuk sebarang ganti rugi langsung, tidak
langsung, khas atau lain-lain untuk apa-apa penggunaan laman ini atau
produk kami, termasuk, tanpa batasan, apa-apa keuntungan yang hilang,
gangguan perniagaan, kehilangan program atau data lain mengenai
sistem pengendalian maklumat anda atau sebaliknya, walaupun kami
dinasihatkan dengan jelas mengenai kemungkinan kerosakan
sedemikian.</p>

<b>4. PENDAPATAN</b>
<p>
JUTAWANAPP ADALAH SEBUAH APLIKASI MARKETING TOOL
YANG BOLEH BERJALAN DI TELEFON ANDROID ANDA. SILA
PASTIKAN TELEFON ANDA MENGGUNAKAN OS ANDROID UNTUK
MENGGUNAKAN APLIKASI INI.
JUTAWANAPP BUKAN SEBUAH
SKIM CEPAT KAYA, SKIM PELABURAN ATAU MLM YANG PERLU DI
SERTAI UNTUK MENJANA PENDAPATAN. IA ADALAH PENJUALAN
SEBUAH PRODUK BERUPA APLIKASI YANG BOLEH DI PASANG DI
TELEFON BIMBIT ANDROID YANG BERFUNGSI UNTUK MENJADI
SEBUAH MARKETING TOOL UNTUK PEMBELI.
<br><br>
JUTAWANAPP.COM MEMBUAT SEGALA USAHA UNTUK
MEMASTIKAN IA MENERANGKAN DENGAN JELAS MENGENAI
PRODUK DENGAN TEPAT DAN SEGALA POTENSI PENDAPATAN
MEREKA. ANGGARAN PENDAPATAN DAN KEUNTUNGAN YANG
DIBUAT OLEH JUTAWANAPP.COM DAN PELANGGANNYA ADALAH
ANGGARAN TENTANG APA YANG JUTAWANAPP.COM FIKIR ANDA
MUNGKIN BOLEH DAPAT DAN JUGA BERDASARKAN
PENDAPATAN YANG DIPEROLEHI SENDIRI OLEH PIHAK JUTAWANAPP. 
<br><br>
TIADA SEBARANG JAMINAN BAHAWA ANDA AKAN
DAPAT MEMBUAT TAHAP PENDAPATAN INI DAN ANDA SETUJU
BAHAWA PENDAPATAN DAN KEUNTUNGAN DARI PENGGUNAAN
APLIKASI ADALAH BERBEZA BAGI SETIAP INDIVIDU.
SEPERTI MANA-MANA PERNIAGAAN, HASIL ANDA MUNGKIN
BERBEZA-BEZA, DAN AKAN BERDASARKAN KEUPAYAAN ANDA,
PENGALAMAN PERNIAGAAN, KEPAKARAN DAN TAHAP
KEINGINAN MASING-MASING. TIDAK ADA SEBARANG JAMINAN
MENGENAI TAHAP KEJAYAAN YANG MUNGKIN DI CAPAI.
CONTOH YANG DIGUNAKAN ADALAH BERDASARKAN TESTIMONI PEMBELI YANG
LUAR BIASA DAN TIDAK DIMAKSUDKAN UNTUK MEWAKILI ATAU
MENJAMIN BAHAWA SESIAPA PUN AKAN MENCAPAI HASIL YANG
SAMA ATAU SERUPA. KEJAYAAN SETIAP INDIVIDU BERGANTUNG
PADA LATAR BELAKANG, DEDIKASI, KEINGINAN DAN MOTIVASI
MEREKA.<br><br>
TIDAK ADA JAMINAN BAHAWA CONTOH PENDAPATAN MASA
LALU BOLEH DIDUPLIKASI UNTUK MASA AKAN DATANG.
JUTAWANAPP.COM TIDAK DAPAT MENJAMIN HASIL DAN / ATAU
KEJAYAAN MASA DEPAN ANDA. TERDAPAT BEBERAPA RISIKO
YANG TIDAK DIKETAHUI DALAM PERNIAGAAN DAN DI INTERNET
YANG JUTAWANAPP.COM TIDAK DAPAT JANGKA YANG DAPAT
MENGURANGKAN HASIL DARI APA YANG DI PAPARKAN DI
LAMAN INI. JUTAWANAPP.COM TIDAK BERTANGGUNGJAWAB
TERHADAP SETIAP TINDAKAN ANDA.
PENGGUNAAN MAKLUMAT, PRODUK DAN PERKHIDMATAN
JUTAWANAPP.COM HARUS BERDASARKAN KETEKUNAN ANDA
SENDIRI DAN ANDA BERSETUJU BAHAWA JUTAWANAPP.COM
TIDAK BERTANGGUNGJAWAB TERHADAP KEJAYAAN ATAU
KEGAGALAN PERNIAGAAN ANDA YANG SECARA LANGSUNG ATAU
TIDAK LANGSUNG BERKAITAN DENGAN PEMBELIAN DAN
PENGGUNAAN JUTAWANAPP MAKLUMAT, PRODUK DAN
PERKHIDMATAN.</p>

<b>5. Polisi Pemulangan Wang Kembali</b>
<p>
Pembelian ini dilengkapi dengan jaminan wang balik 60 hari dengan
terma dan syarat seperti berikut:</p>
<p>
(A) jaminan itu HANYA sah di mana pembelian telah dibuat oleh anda
sebagai bahan semata-mata bertujuan untuk anda seorang sahaja. Apa-
apa permintaan bayaran balik perlu memasukkan kenyataan /
pengesahan bahawa anda, sebagai pembeli, membeli bahan-bahan ini
dengan suci hati;</p>
<p>
(B) untuk mendapatkan bayaran balik, anda mesti terlebih dahulu
memulangkan bahan-bahan dan apa-apa cetakan kepada
JutawanApp.com, salinan atau sebahagian daripadanya serta semua

URL memuat turun dan semua akses kepadanya dalam keadaan asal
tanpa sebarang kerosakan apapun. Permintaan pengembalian dana anda
mesti mengandungi pengesahan kepada fakta dan jika pada masa kami
menemui sebarang bukti bahawa pengesahan anda salah (sama ada
secara sengaja atau tidak), permintaan pengembalian akan ditolak dan
anda akan kehilangan hak ke atas bayaran balik secara kekal;</p>
<p>
(C) bayaran balik ini HANYA sah jika anda membayar balik dalam
tempoh 60 hari dari penerimaan produk dan apabila JutawanApp.com
menerima kembali bahan dalam tempoh 60 hari dari penerimaan bahan
oleh anda;</p>
<p>
(D) anda perlu memberikan bukti yang JELAS dan tidak dapat disangkal
lagi bahawa pembayaran melalui deposit bank memang jelas dibuat
untuk pembelian bahan tersebut. Pengesahan deposit bank dari bank
anda menunjukkan bahawa wang telah dipindahkan ke akaun bank
JutawanApp.com harus disediakan dan anda adalah pemilik akaun bank
tersebut. Jika pembayaran dilakukan melalui mesin deposit tunai,
pastikan nama dan email anda tertera pada resit deposit tunai tersebut
secara terang. Juga pengesahan dari bank perlu ada untuk memastikan
bayaran tersebut telah benar-benar dilakukan oleh anda;</p>
<p>
(E) dengan meminta pengembalian wang dan mengesahkan perkara di
atas, anda bersetuju dengan liabiliti kerosakan yang tidak dapat
diperbaiki ke JutawanApp.com sekiranya didapati bahawa anda telah
menyimpan salinan atau bahagian bahan atau memberikannya kepada
pihak ketiga. Liabiliti ini berdiri walaupun tanpa permintaan
pengembalian dana, tentu saja, kerana ia adalah sebahagian daripada
hak cipta dan lesen produk maklumat;</p>
<p>

(F) mempunyai hak untuk mendapatkan bayaran balik, anda mesti
membuktikan bahawa anda telah mematuhi batasan hak cipta produk
maklumat ini dan tidak memberikan apa-apa salinan atau mana-mana
bahagian kepada SETIAP pihak ketiga;</p>
<p>
(G) bayaran balik HANYA boleh dilakukan untuk sebarang
KEROSAKAN PADA PRODUK seperti masalah teknikal yang tidak dapat
diselesaikan. Bayaran balik tidak boleh dilakukan untuk
apa-apa tujuan lain selain dari ini. Tidak mendapat pendapatan bukan sebab yang dibenarkan untuk mendapat bayaran balik. Disebabkan sifat produk digital aplikasi, kami tidak boleh memberikan bayaran
balik jika anda hanya mengubah fikiran anda selepas membelinya. Anda bertanggungjawab sepenuhnya
untuk memastikan anda mempunyai segala peralatan yang secukupnya
dan memenuhi kriteria seperti yang diceritakan di laman sebelum
melakukan pembelian produk;</p>
<p>
(H) hak bayaran balik ini tidak dimaksudkan sebagai peluang untuk
melihat dan menggunakan bahan-bahan dalam laman web secara
percuma. Jangan teruskan untuk mendapatkan dan melihat bahan-
bahan melainkan jika anda benar-benar berminat dengan subjeknya dan
berhasrat untuk menilai cara untuk mencapai kawalan yang lebih baik
terhadap kewangan anda dan mencapai kebebasan kewangan.Alasan bahawa produk tidak memenuhi apa yang di
tawarkan tidak boleh diterima. Segala maklumat tawaran adalah
berdasarkan apa yang di letakkan di laman web jutawanapp.com sahaja.
Sebarang tawaran lain atau janji lain tidak di ambil kira. Anda mesti
memberikan keterangan dan bukti lengkap (jawatan media sosial, sms, mesej e-
mel yang dihantar dan lain-lain) yang telah anda jalankan dan cuba
SEMUA kaedah yang diterangkan dalam ebook panduan. Tanda-tanda
permintaan bayaran balik adalah sangat khusus

dalam industri maklumat dan jika kami mendapati tanda-tanda yang
ada dalam permintaan anda, ia mungkin ditolak atas dasar yang anda
menyalahgunakan jaminan wang untuk mendapatkan bahan-bahan
secara percuma, dengan itu berniat memperoleh keuntungan dengan
penipuan;</p>
<p>
(I) pembelian asal anda mesti dibuat atas nama anda sendiri dan dalam
permintaan bayaran balik anda, anda mesti memasukkan tarikh
pembelian asal anda, ID keahlian nama dan laman web yang diberikan
oleh kami kepada anda;
DAN</p>
<p>
(J) semua yuran pemprosesan, kos pembungkusan, YURAN AFFILIATE,
kos penghantaran dan penghantaran dan semua perbelanjaan lain yang
berkaitan dengan proses penyerahan bahan kepada anda AKAN
DITOLAK dari bayaran balik kepada anda.</p>
<p>
Kami tidak berunding dengan pelanggan mengenai format permintaan
pengembalian yang betul selain membuat halaman ini tersedia.
Terpulang kepada anda untuk memastikan permintaan bayaran balik
anda mengesahkan perkara-perkara di atas dari permulaan dan jika
tidak, permintaan anda akan ditolak. Semua keputusan penolakan
adalah muktamad. Kami memohon maaf atas perlunya syarat-syarat
khusus ini untuk mendapatkan bayaran balik, meminta dan memproses
bayaran balik. Langkah-langkah ini malangnya perlu kerana sesetengah
individu telah menyalahgunakan dasar pembayaran balik kami untuk
mendapatkan produk percuma. Jaminan wang balik 60-hari bermaksud
apa yang dikatakan selagi anda membeli bahan tersebut dengan niat
penuh untuk menggunakannya dan, sekiranya anda benar-benar tidak

puas hati, hantarkan permintaan bayaran balik anda mengikut kaedah
yang di nyatakan di atas.</p>

<b>6. PROGRAM AFFILIATE</b><br>
<br>
(A) Tempoh Perjanjian
<p>
Terma perjanjian ini akan bermula apabila JutawanApp.com menerima
Afiliasi anda dan akan berakhir apabila ditamatkan oleh salah satu
pihak. Sama ada Affiliate atau JutawanApp.com boleh menamatkan
perjanjian ini, pada bila-bila masa, dan untuk apa-apa sebab dengan
memberikan notis bertulis yang lain kepada pihak yang satu lagi. Setelah
penamatan, Affiliate bersetuju dengan segera berhenti menggunakan,
dan menghapuskan dari promosi, laman web, semua bahan pautan,
tanda dagangan, logo, dan semua bahan lain yang JutawanApp.com
telah menyediakan Affiliate untuk digunakan berkaitan dengan program
ini.</p>
(B) Komisen Rujukan (Referral Commission)
<p>
JutawanApp.com bersetuju untuk membayar komisyen rujukan dalam
jumlah Ringgit Malaysia Dua Ratus Sahaja hanya atas jualan rujukan
yang disahkan sahaja. Jualan yang disahkan akan bermakna urus niaga
berjaya yang menyebabkan JutawanApp.com menerima urus niaga dari
pembeli produk dalam MASA YANG MUNASABAH. Sekiranya
JutawanApp.com mendapati jualan jualan tidak diterima dalam masa
yang munasabah, JutawanApp.com berhak untuk menahan pembayaran
tersebut sehingga transaksi itu telah disahkan oleh pembeli. Ini
termasuk tetapi tidak terhad kepada pembersihan cek, pesanan wang,
pesanan pos, transaksi Paypal dan banyak lagi. Sebarang pemulangan

atau bayaran balik oleh pembeli bahan akan menyebabkan komisen
ditolak oleh JutawanApp.com. Hak sepenuhnya samada untuk memberi
komisen kepada affiliate atau tidak adalah berada di tangan
JutawanApp.com sahaja dan sebarang keputusan adalah muktamad.</p>
(C) Terma Pembayaran

<p>Frekuensi Pembayaran - JutawanApp.com bersetuju untuk membayar
Affiliate dalam LIMA (5) hari selepas akhir setiap tempoh pembayaran
untuk semua komisen rujukan yang disahkan yang diperolehi dalam 2
minggu sebelum ini, kurang apa-apa JutawanApp.com menentukan
mengikut budi bicara mutlaknya, tidak sah diperoleh daripada
penggunaan program yang tidak betul. Komisen hendaklah dikira dari
0800 Isnin hingga 0759 Isnin dua minggu selepas itu.
Kaedah Pembayaran - JutawanApp.com bersetuju membayar Affiliate
melalui bank terus ke akaun bank affiliate. Affiliate bertanggungjawab
untuk memberikan butiran akaun bank JutawanApp.com melalui
penyerahan borang semasa pendaftaran afiliasi ataupun masuk sendiri
ke dalam portal affiliate dan melakukan kemaskini sendiri. Sebarang caj
bank akan ditolak daripada pembayaran keluar Affiliate. Affiliate
bertanggungjawab memaklumkan kepada JutawanApp.com mengenai
sebarang perubahan kepada butiran perbankan. Sekiranya Affiliate gagal
memberikan butiran ini, JutawanApp.com akan dengan menggunakan
dana tersebut sehingga kitaran pembayaran seterusnya.
JutawanApp.com juga berhak membayar Affiliate melalui apa-apa cara
lain mengikut budi bicara mutlaknya. Pihak JutawanApp.com akan
melakukan pembayaran ke akaun bank yang didaftarkan dan tertera di
dalam sistem portal affiliate pada masa pembayaran. Sebarang kesilapan

atau kecuaian oleh affiliate tidak akan dilayan oleh pihak
JutawanApp.com</p>

<b>7. PENGURUSAN DAN PEMILIHAN UNDANG-UNDANG</b>
<p>
JutawanApp.com tidak membuat sebarang representasi bahawa bahan-
bahan di laman ini sesuai atau tersedia untuk digunakan di lokasi lain.
Mereka yang memilih untuk mengakses laman web ini dari lokasi lain
melakukannya atas inisiatif mereka sendiri dan bertanggungjawab untuk
mematuhi undang-undang tempatan, jika dan setakat undang-undang
tempatan terpakai.
Terma dan Syarat ini akan ditadbir oleh, ditafsirkan dan dikuatkuasakan
mengikut undang-undang Malaysia, kerana ia digunakan untuk
perjanjian yang dibuat dan dilaksanakan sepenuhnya dalam bidang
kuasa sedemikian. Sebarang tindakan anda, mana-mana pihak ketiga
atau JutawanApp.com membawa untuk menguatkuasakan Terma dan
Syarat ini atau, berhubung dengan, apa-apa perkara yang berkaitan
dengan laman web ini akan dibawa hanya di mahkamah Kuala Lumpur,
dan anda secara nyata bersetuju dengan bidang kuasa mahkamah
tersebut.</p>

<b>8. POLISI TIDAK BOLEH SPAM</b>
<p>
JANGAN gunakan perkhidmatan kami untuk menghantar apa-apa
komunikasi spam! SPAM hendaklah termasuk, tetapi tidak terhad
kepada penghantaran sebarang mesej mel elektronik dengan tujuan
utamanya ialah iklan komersil atau promosi produk atau perkhidmatan
komersial (termasuk kandungan di laman web Internet yang
dikendalikan untuk tujuan komersil). Istilah ‘mesej elektronik’ tidak

termasuk mesej transaksional atau hubungan. JANGAN Sertakan SIAP
Rujukan kepada JutawanApp, atau JutawanApp.com, dalam sebarang
surat-menyurat tersebut.
Di samping itu, JANGAN menggunakan nama atau jenama JutawanApp
/ JutawanApp.com dalam sebarang jenis surat-menyurat kepada mailing
list pihak ketiga atau senarai &#39;memilih&#39;, senarai e-mel yang dituai,
safelists, kumpulan berita, forum, &quot;faxblast&quot;, atau sebarang bentuk lain
Komunikasi yang telah dilabel sebagai SPAM.
Mana-mana anggota / affiliate / pengguna JutawanApp.com / pengguna
yang menggunakan SPAM dalam apa-apa hubungan dengan laman web
JutawanApp / JutawanApp.com akan mempunyai keahlian mereka
ditamatkan dengan serta-merta, akan melupuskan mana-mana komisen
afiliasi dan akan dipertanggungjawabkan secara sah dan kewangan atas
tindakan mereka.
Apa-apa aduan dan pelanggaran SPAM mesti dilaporkan kepada
admin@JutawanApp.com untuk tindakan selanjutnya diambil.</p>

<b>9. PERJANJIAN KESELURUHANNYA</b>
<p>
Terma dan syarat ini merangkumi keseluruhan perjanjian antara pihak-
pihak di sini dan menggantikan semua rundingan, perwakilan, literatur
pemasaran, komunikasi dan perjanjian yang berkaitan dengan terma
dan syarat ini, sama ada tertulis atau lisan, kecuali untuk
memperluaskannya secara khusus yang diwujudkan atau dirujuk di sini .
Tiada perubahan, perubahan atau pengubahsuaian kepada terma dan
syarat ini akan berkuat kuasa melainkan jika secara bertulis. Kegagalan

oleh salah satu pihak untuk melaksanakan haknya di bawah terma dan
syarat ini tidak akan menjadi penepian terma dan syaratnya.</p>
<?php include 'inc.ftr.php';?>
