<?php
	include 'inc.hdr.php';
?>
<h1 class="highlight txt_center">Terma & Syarat</h1>

<!--<p>SEBELUM ANDA TERUS MEMBACA APA-APA DI LAMAN WEB INI DAN BERSETUJU UNTUK MELAKUKAN SEBARANG PEMBELIAN PRODUK DI LAMAN WEB INI, SILA PASTIKAN ANDA TELAH MEMBACA KESELURUHAN TERMA DAN SYARAT DI BAWAH. ANDA BERTANGGUNGJAWAB SEPENUHNYA UNTUK MEMAHAMI SEGALA TERMA SYARAT SEBELUM MELAKUKAN SEBARANG PEMBELIAN BARANG.</p>-->

<p>SEBELUM ANDA TERUS MEMBACA APA-APA DI LAMAN WEB INI DAN BERSETUJU UNTUK MELAKUKAN SEBARANG PEMBELIAN PRODUK DI LAMAN WEB INI, SILA PASTIKAN ANDA TELAH MEMBACA KESELURUHAN TERMA DAN SYARAT DI BAWAH. ANDA BERTANGGUNGJAWAB SEPENUHNYA UNTUK MEMAHAMI SEGALA TERMA SYARAT SEBELUM MELAKUKAN SEBARANG PEMBELIAN BARANG. TERMA SYARAT ADALAH DALAM BAHASA INGGERIS KERANA HANYA TERMA SYARAT INI SAHAJA YANG TERANG DAN DAPAT TERANGKAN DENGAN JELAS MENGENAI SYARAT TAWARAN SAYA.</p>

<p><strong>1. LEGAL INFORMATION: TERMS AND CONDITIONS</strong></p>

<p>This site (the "Site") is owned and operated by JutawanApp.com. Your access, use of the Site and use of Site&rsquo;s materials is subject to the following terms and conditions ("Terms and Conditions") on this page, and all applicable laws. By accessing and browsing the Site, you accept, without limitation or qualification, these Terms and Conditions. If you do not agree with any of the below Terms and Conditions, do not use the Site and do not purchase our materials. JutawanApp.com reserves the right, in its sole discretion, to modify, alter or otherwise update these Terms and Conditions at any time and by using our Site, you agree to be bound by such modifications, alterations or updates.</p>

<p><strong>2. COPYRIGHTS AND TRADEMARKS</strong></p>

<p>The documents and information on the Site are copyrighted materials of JutawanApp.com. Copyright information contained on this domain may not be reproduced, distributed or copied publicly in any way, including Internet, e-mail, newsgroups, or reprinting. Any violator will be subject to the maximum fine and penalty imposed by law. Purchasers of our materials are granted a license to use the material contained herein for their own personal use only. Any violators will be pursued and punished to the fullest extent of the law. No claim of copyright is made on any 3rd party software, ware, websites, images or text that may be referenced in our material(s). By viewing and/or purchasing the materials on our website, you agree to be bound by these copyright terms. </p>

<p><strong>3. LIMITATION OF LIABILITY</strong></p>

<p>Information and any software available for download on this site is provided "As is", without warranty of any kind, either expressed or implied, including, but not limited to, the implied warranties of merchantability, fitness for a particular purpose, or non-infringement. Our company's entire liability, and any customer's exclusive remedy, shall be a refund of the price paid.</p>

<p>Though we strive for accuracy, information on the Site may contain inadvertent technical inaccuracies or typographical errors. Information may be changed or updated without notice. JutawanApp.com may also make improvements and/or changes in the products and/or the programs/offers described in this information at any time without notice. </p>

<p>JutawanApp.com makes no representations whatsoever about any hyperlinked third-party sites which you may access through our Site or product. These 3rd party sites are not created or maintained by JutawanApp.com. They are independent from JutawanApp.com, and JutawanApp.com has no control over the content on those sites. Moreover, JutawanApp.com does not endorse or accept any responsibility for the content, or the use, of such sites. While we have no information about the presence of such dangers on 3rd party sites, you should take the necessary precautions to protect yourself and your systems from viruses, worms, Trojan horses, and other items of a destructive nature. </p>

<p>In no event will JutawanApp.com be liable to you or any other third party for any direct, indirect, special or other consequential damages for any use of this Site or our product(s), including, without limitation, any lost profits, business interruption, loss of programs or other data on your information handling system or otherwise, even if we are expressly advised of the possibility of such damages.</p>

<p><strong>4. EARNINGS</strong></p>

<p>JutawanApp.com make every effort to ensure that it accurately represent these products and services and their potential for income. Earning and Income statements made by JutawanApp.com and its customers are estimates of what JutawanApp.com think you can possibly earn. There is no guarantee that you will make these levels of income and you accept the risk that the earnings and income statements differ by individual.</p>

<p>As with any business, your results may vary, and will be based on your individual capacity, business experience, expertise, and level of desire. There are no guarantees concerning the level of success you may experience. The testimonials and examples used are exceptional results, which do not apply to the average purchaser, and are not intended to represent or guarantee that anyone will achieve the same or similar results. Each individual's success depends on his or her background, dedication, desire and motivation.</p>

<p>There is no assurance that examples of past earnings can be duplicated in the future. JutawanApp.com cannot guarantee your future results and/or success. There are some unknown risks in business and on the internet that JutawanApp.com cannot foresee which can reduce results. JutawanApp.com is not responsible for your actions.</p>

<p>The use of JutawanApp.com information, products and services should be based on your own due diligence and you agree that JutawanApp.com is not liable for any success or failure of your business that is directly or indirectly related to the purchase and use of JutawanApp.com information, products and services.</p>

<p>While every attempt has been made to verify the information on this web site and the product being sold, neither the product owners, distributors, agents or publishers assume any responsibility for any error, inaccuracies, and or omissions or assume any responsibility or liability whatsoever on behalf of any purchaser or reader of these materials. Any slight on people, organisations, companies or products are unintentional. The income statements and examples on this website are not intended to represent or guarantee that everyone will achieve the same results. Each individual&rsquo;s success will be determined by his or her desire, dedication, marketing background, effort and motivation to work and follow the program. There is no guarantee or duplicate results stated here. You recognise any business endeavor has inherent risk for loss of capital.</p>

<p><strong>5. REFUND POLICY</strong></p>

<p>The purchase comes with a full 60-days money-back guarantee with the terms and conditions as follows:</p>

<p>(a) 	the guarantee is valid ONLY where the purchase has been made by you as the material is solely intended for that person alone. Any refund requests needs to include a statement/attestation that you, as the purchaser, bought these materials in good faith;</p>

<p>(b) 	to obtain a refund you must first return the materials and any printouts to JutawanApp.com, copies or parts of it as well as all the downloading URLs and all access to it in it&rsquo;s original condition without any damage whatsoever. Your refund request must include an attestation to the fact and if at the time we find ANY evidence that your attestation is false (whether intentionally or not) the refund request will be declined and you will have permanently forfeited your right to a refund;</p>

<p>(c) 	this refund is ONLY valid if you refund within 60-days of receipt of materials and when  JutawanApp.com receives back the materials within 60-days of receipt of the material by you;</p>

<p>(d)	you will need to provide proof beyond doubt that a bank deposit was indeed clearly made for the purchase of the materials. Bank deposit confirmation from your bank showing that money has been transferred into JutawanApp.com bank account must be provided and that you are the owner of the said bank account;</p>

<p>(e)	by requesting a refund and attesting to the above you agree to a liability of irreparable damage toward JutawanApp.com should it later be found that you've kept copies or parts of the materials or given these to third parties. This liability stands even without a refund request, of course, as it is part of the copyright and license of the information product;</p>

<p>(f)	to have a right for a refund you must attest that you've adhered to the copyright limitations of this information product and not given any copies or any parts to ANY third parties;</p>

<p>(g)	this refund right is not intended as an opportunity to view and use the materials in the website for free. Do NOT proceed to obtain and view the materials unless you are genuinely interested in its subject matter and intend to evaluate ways to achieve better control over your finances and achieve financial freedom. Due to the nature of digital products, we cannot grant a refund if you simply change your mind after purchasing the app. You must provide evidence and proof (social media posts, sms,email messages sent etc)that you have executed and tried ALL methods described in the ebook manual. The signs of frivolent refund requests are very specific within the information industry and if we find those signs present in your request it may be refused on the basis that you misused the money-back guarantee to obtain the materials for free, thus intending to acquire benefit with deceipt aforethought;</p>

<p>(h)	your original purchase must have been made in your own name and in your refund request you must include the date of your original purchase, the name and website membership ID given by us to you;</p>

<p>and</p>

<p>(i)	all processing fees, packaging costs, affiliate fees, posting and delivery costs and all other related expenses incurred in the process of delivering the materials to you will be deducted from the refund to you.</p>

<p>We do not consult clients on the correct format of refund requests other than make this page available. It is up to you to ensure your refund request confirms with these above points from the onset and if it doesn't, your request will be rejected. All decisions of rejection are final. We apologise for the need for these specific terms for refund right, request and processing of refunds. These measures are unfortunately necessary because some rare individuals have misused our refund policies in order to acquire free materials. The 60-day 100% money-back guarantee means what it says as long as you purchase the materials with full intention of using it and, in case you are truly dissatisfied, file your refund request according to the simple points above.</p>

<p><strong>6. AFFILIATE PROGRAM</strong></p>

<p>(a) Term Of Agreement</p>

<p>The term of this Agreement will begin upon the JutawanApp.com&rsquo;s acceptance of the Affiliate and will end when terminated by either party. Either the Affiliate or the JutawanApp.com may terminate this Agreement, at any time, and for any reason by giving the other party written Notice of Termination. Upon termination, the Affiliate agrees to immediately cease using, and remove from their Promotions, Website, all link materials, trademarks, logos, and all other materials that JutawanApp.com may have provided the Affiliate to use in connection with the Program.</p>

<p>(b)  Referral Commissions </p>

<p>JutawanApp.com agrees to pay referral commissions in the sum of Ringgit Malaysia Two Hundred Only on confirmed referral sales only. Confirmed sales shall mean successful transaction which results to JutawanApp.com receiving the transaction from the purchaser of the materials within a reasonable time. If JutawanApp.com finds that the transacting sale is not received within reasonable time, JutawanApp.com has the right to withhold those payments until that transaction has been cleared by the purchaser. This includes but not limited to clearing of cheques, money orders, postal orders, Paypal transactions and more. Any refunds by the purchaser of the materials will result to commissions being rejected by JutawanApp.com.</p>

<p>(c)  Payment Terms </p>

<p>Payment Frequency - JutawanApp.com agrees to pay the Affiliate within FIVE (5) days after the end of each payment period for all confirmed referral commissions earned during the prior 2 weeks, less any amount JutawanApp.com determines in its sole discretion, was not validly earned from improper use of the Program. Commission shall be calculated from 0800 Monday to 0759 Monday two weeks after that.</p>

<p>Payment Method - JutawanApp.com agrees to pay the Affiliate via direct bank-in to the Affiliate's bank account. The Affiliate is responsible to provide JutawanApp.com his/her bank account details via form submission during affiliate registration. Any bank charges will be deducted from the Affiliate's outgoing payments. The Affiliate is responsible in informing JutawanApp.com of any changes to the banking details. In the event that the Affiliate fails to provide these details, JutawanApp.com will with-hold those funds until the next payment cycle. JutawanApp.com also reserves the right to pay the Affiliate via any other means at its sole discretion.</p>

<p><strong>7. JURISDICTION AND CHOICE OF LAW</strong></p>

<p>JutawanApp.com does not make any representation that materials in the Site are appropriate or available for use in other locations. Those who choose to access this site from other locations do so on their own initiative and are responsible for compliance with local laws, if and to the extent local laws are applicable. </p> 
<p> These Terms and Conditions shall be governed by, construed and enforced in accordance with the laws of Malaysia, as it is applied to agreements entered into and to be performed entirely within such jurisdiction. Any action you, any third party or JutawanApp.com brings to enforce these Terms and Conditions or, in connection with, any matters related to this site shall be brought only in Kuala Lumpur courts, and you expressly consent to the jurisdiction of said courts. </p>

<p><strong>8. NO SPAM POLICY</strong></p>

<p>DO NOT use our services to send any sort of spam communications! SPAM shall include, but is not limited to, the sending of any electronic mail message with the primary purpose of which is the commercial advertisement or promotion of a commercial product or service (including content on an Internet website operated for a commercial purpose).  The term `commercial electronic mail message' does not include a transactional or relationship message. DO NOT include ANY reference to JutawanApp, or JutawanApp.com, in any such correspondence. </p>

<p>In addition, DO NOT use the JutawanApp / JutawanApp.com name or brand in any type of correspondence to third party mailing or 'opt in' lists, harvested email lists, safelists, newsgroups, forums, "faxblasts", or any other form of communication that has been labeled as SPAM. </p>

<p>Any JutawanApp / JutawanApp.com member/affiliate/user that uses SPAM in any relation with the JutawanApp / JutawanApp.com website shall have their memberships terminated immediately, will forfeit any affiliate commissions and will be held both legally and financially responsible for their actions. <br /> <br /> Any SPAM complaints and abuses must be reported to <strong>admin@JutawanApp.com</strong> for further action to be taken.</p>

<p><strong>9. ENTIRE AGREEMENT</strong></p>

<p>These terms and conditions constitutes the entire agreement between the parties hereto and supersedes all prior negotiations, representations, marketing literatures, communications and agreements related to these terms and conditions, either written or oral, except to the extend they are specifically embodied or referred to herein. No changes, alterations or modifications to these terms and conditions shall be effective unless in writing. Failure by either party to implement its rights under these terms and conditions shall not constitute a waiver of the terms and conditions thereof.</p>

<?php include 'inc.ftr.php';?>
