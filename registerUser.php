<!doctype html>
<html lang="en" dir="ltr">
<?php
    
    include('header.php');
?>
        <?php
            // Connection database
            //include("connection.php");
            // Check, for session 'user_session'
            include("session.php");

            // Set Default Time Zone for Asia/Kuala_Lumpur
            date_default_timezone_set("Asia/Kuala_Lumpur");

            // Check, if username session is NOT set then this page will jump to login page
            if (!isset($_SESSION['session']) && !isset($_SESSION['job'])) {
                header('Location: login.php');
                session_destroy();
            }

            $job = $_SESSION["job"];
            $now = date("Y-m-d");
        ?>
				<div class="my-3 my-md-5 app-content">
					<div class="side-app">
						<div class="page-header">
							<ol class="breadcrumb1">
											<li class="breadcrumb-item1 active">User Management</li>
											<li class="breadcrumb-item1 active">Register User </li>
							</ol>
							
						</div>
                        
							<div class="col-lg-12">
								<form class="card" method="POST" action="insertUserAdd.php"  enctype="multipart/form-data">
									<div class="card-header">
										<h3 class="card-title">Register User</h3>
									</div>
									<div class="card-body">
										<div class="row">
                                            <div class="col-sm-6 col-md-12">
												<div class="form-group">
													<label class="form-label">Full Name</label>
													<input type="text" class="form-control" name="name" id="name" >
												</div>
											</div>
											<div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">NRIC / Passport</label>
													<input type="text" class="form-control" name="nric" id="nric" >
                                                    <input type="text" class="form-control" name="password" id="password" hidden>
												</div>
											</div>
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Contact Number</label>
													<input type="text" class="form-control" name="phone" id="phone" >
												</div>
											</div>
                                             <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Email</label>
													<input type="text" class="form-control" name="email" id="email" >
												</div>
											</div>
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">No. Unit House</label>
													<select class="form-control select2 custom-select" data-placeholder="Choose one" name="NoHouse" id="NoHouse">
                                                            <option value="">-- Choose --</option>
                                                            <?php 
                                                                  ## Fetch countries
                                                                  $stmt = $conn->prepare("SELECT * FROM house");
                                                                  $stmt->execute();
                                                                  $houseList = $stmt->fetchAll();

                                                                  foreach($houseList as $house){
                                                                     echo "<option value='".$house['house_ID']."'>".$house['NoHouse']."</option>";
                                                                  }
                                                                  ?>
                                                     </select>
                                   
												</div>
											</div>
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Position</label>
													<select class="form-control select2 custom-select" data-placeholder="Choose one" name="position" id="position">
                                                            <option label="Choose one"></option>
                                                            <option value="Admin">Admin</option>
                                                            <option value="Employee">Employee</option>
                                                            <option value="Resident">Resident</option>
                                                     </select>
												</div>
											</div>
                                            
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Status</label>
													<select class="form-control select2 custom-select" data-placeholder="Choose one" name="status" id="status">
                                                            <option label="Choose one"></option>
                                                            <option value="Single">Single</option>
                                                            <option value="Marry">Marry</option>
                                                     </select>
												</div>
											</div>
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Gender</label>
													<select class="form-control select2 custom-select" data-placeholder="Choose one" name="gender" id="gender">
                                                            <option label="Choose one"></option>
                                                            <option value="Male">Male</option>
                                                            <option value="Female">Female</option>
                                                     </select>
												</div>
											</div>
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">State</label>
													<select class="form-control select2 custom-select" data-placeholder="Choose one" name="state" id="state">
                                                            <option label="Choose one"></option>
                                                            <option value="Johor">Johor</option>
                                                            <option value="Kedah">Kedah</option>
                                                            <option value="Kelantan">Kelantan</option>
                                                            <option value="Perak">Perak</option>
                                                            <option value="Selangor">Selangor</option>
                                                            <option value="Wilayah Persekutuan">Wilayah Persekutuan</option>
                                                            <option value="Melaka">Melaka</option>
                                                            <option value="Negeri Sembilan">Negeri Sembilan</option>
                                                            <option value="Pahang">Pahang</option>
                                                            <option value="Perlis">Perlis</option>
                                                            <option value="Pulau Penang">Pulau Penang</option>
                                                            <option value="Sabah">Sabah</option>
                                                            <option value="Serawak">Serawak</option>
                                                            <option value="Terengganu">Terengganu</option>
                                                     </select>
												</div>
											</div>
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Region</label>
													<select class="form-control select2 custom-select" data-placeholder="Choose one" name="region" id="region">
                                                            <option label="Choose one"></option>
                                                            <option value="Islam">Islam</option>
                                                            <option value="Hindu">Hindu</option>
                                                            <option value="Buddha">Buddha</option>
                                                            <option value="Kristian">Kristian</option>
                                                     </select>
												</div>
											</div>
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Descendant</label>
													<select class="form-control select2 custom-select" data-placeholder="Choose one" name="descendant" id="descendant">
                                                            <option label="Choose one"></option>
                                                            <option value="Melayu">Melayu</option>
                                                            <option value="India">India</option>
                                                            <option value="Cina">Cina</option>
                                                            <option value="Other">Other</option>
                                                     </select>
												</div>
											</div> 
                                            <div class="col-sm-6 col-md-12">
												<div class="form-group">
													<label class="form-label">Address</label>
													<input type="text" class="form-control" name="address" id="address" >
												</div>
											</div> 
											<div class="col-md-5">
												<div class="form-group">
													<label class="form-label">Postal Code</label>
													<input type="text" class="form-control"  placeholder="e.g : 86400" name="postal" id="postal"  >
												</div>
											</div>
											<div class="col-sm-6 col-md-3">
												<div class="form-group">
													<label class="form-label">City</label>
													<input type="text" class="form-control" placeholder="e.g : Parit Raja" name="city" id="city" >
												</div>
											</div>
											<div class="col-sm-6 col-md-4">
												<div class="form-group">
													<label class="form-label">Country</label>
													<input type="text" class="form-control" placeholder="" name="country" id="country">
												</div>
											</div>						
										</div>
									</div>
									<div class="card-footer text-right">
										<button type="submit" class="btn btn-primary" >Register User</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					
<?php
    
    include('footer.php');
?>

