<!doctype html>
<html lang="en" dir="ltr">
<?php
    
    include('header.php');
?>
				<div class="my-3 my-md-5 app-content">
					<div class="side-app">
						<div class="page-header">
							
							 <ol class="breadcrumb1">
											<li class="breadcrumb-item1 active">Resident Management</li>
											<li class="breadcrumb-item1 active">Register Resident</li>
							</ol>
						</div>
							<div class="col-lg-12">
								<form class="card" method="POST" action="insertoccupant.php" enctype="multipart/form-data">
									<div class="card-header">
										<h3 class="card-title">Register Resident</h3>
									</div>
									<div class="card-body">
										<div class="row">
                                             <div class="col-sm-6 col-md-12">
                                            <div class="expanel-footer">Resident Information</div>
                                            </div>
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Name</label>
													<input type="text" name="name" id="name" class="form-control" required >
												</div>
											</div>
											<div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">NRIC</label>
													<input type="text" name="nric" id="nric" class="form-control" required >
												</div>
											</div>                                             
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Contact Number</label>
													<input type="text" name="phone" id="phone" class="form-control" required >
												</div>
											</div>
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Email</label>
													<input type="email" name="email" id="email" class="form-control" required >
												</div>
											</div>
											<div class="col-sm-12 col-md-12">
												<div class="form-group">
													<label class="form-label">Address</label>
													<input type="text" name="address" id="address" class="form-control" required >
												</div>
											</div>
                                             <div class="col-sm-6 col-md-6">
                                                <label for="recipient-name" class="form-control-label">Status:</label>
                                                <div class="form-group">
                                               <select class="form-control select2 custom-select" data-placeholder="Choose one" name="status" id="status">
                                                            <option label="Choose one"></option>
                                                            <option value="Owner">Owner</option>
                                                            <option value="Rental">Rental</option>
                                                </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">No Unit House</label>
													<input type="text" name="unit_house" id="unit_house" class="form-control" required >
												</div>
											</div>
                                            
                                           
                                            <!-- // form penyewa

                                            <div class="col-sm-6 col-md-12">
                                            <div class="expanel-footer">Maklumat Penyewa</div>
                                            </div>
                                            <div>
                                            &nbsp;
                                            </div>
                                            <div class="col-md-12">
												<div class="form-group">
                                                    <form name="add_name" id="add_name">  
                                                          <div class="table-responsive">  
                                                               <table class="table table-bordered" id="dynamic_field">  
                                                                    <tr>  
                                                                         <td><input type="text" name="name_penyewa[]" placeholder="Enter your Name" class="form-control name_list" /></td>
                                                                        
                                                                        <td><input type="text" name="nric_penyewa[]" id="nric_penyewa[]" class="form-control"  placeholder="nric penyewa" ></td>
                                                                        
                                                                        <td><input type="text" name="relay[]" id="relay[]" class="form-control"  placeholder="Hubungan" ></td>
                                                                        
                                                                         <td><button type="button" name="add" id="add" class="btn btn-success">Add More</button></td>  
                                                                    </tr>  
                                                               </table>  
                                                               <input type="button" name="submit" id="submit" class="btn btn-info" value="Submit" />  
                                                          </div>  
                                                     </form>
                                                </div>
                                            </div>
-->
                                           <!--                         
                                            <div class="col-md-5">
												<div class="form-group">
													<label class="form-label">Nama Penyewa</label>
													<input type="text" name="penyewa" id="penyewa" class="form-control"  placeholder="" >
												</div>
											</div>
											<div class="col-sm-6 col-md-3">
												<div class="form-group">
													<label class="form-label">NRIC</label>
													<input type="text" name="nric_penyewa" id="nric_penyewa" class="form-control" placeholder="" >
												</div>
											</div>
											<div class="col-sm-6 col-md-4">
												<div class="form-group">
													<label class="form-label">Hubungan</label>
													<input type="email" name="relay" id="relay" class="form-control" placeholder="">
												</div>
											</div>	
                                            -->
                                           
                                            			
										</div>
									</div>
									<div class="card-footer text-right">
										<button type="submit" class="btn btn-primary" >Register Resident</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					
<?php
    
    include('footer.php');
?>

