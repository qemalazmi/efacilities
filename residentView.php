<!doctype html>
<html lang="en" dir="ltr">
<?php
    
    include('header.php');
?>
        <?php
            // Connection database
            //include("connection.php");
            // Check, for session 'user_session'
            include("session.php");

            // Set Default Time Zone for Asia/Kuala_Lumpur
            date_default_timezone_set("Asia/Kuala_Lumpur");

            // Check, if username session is NOT set then this page will jump to login page
            if (!isset($_SESSION['session']) && !isset($_SESSION['job'])) {
                header('Location: login.php');
                session_destroy();
            }

            $job = $_SESSION["job"];
            $now = date("Y-m-d");
        ?>
				<div class="my-3 my-md-5 app-content">
					<div class="side-app">
						<div class="page-header">
							<ol class="breadcrumb1">
											<li class="breadcrumb-item1 active">User Management</li>
											<li class="breadcrumb-item1 active">Update User </li>
							</ol>
							
						</div>
            
                        <?php
			 //--------------code get data ----------------
                if(isset($_GET["user_id"]))
                {
                    $user_id = $_GET["user_id"];                    
                    $sql = 
                        "SELECT * FROM user where user_id = :user_id" ; 
                         
                    $stmt = $conn->prepare($sql);
                    $stmt->bindParam(":user_id", $user_id);
                    $stmt->execute();
                    
                    if($dt = $stmt->fetch(PDO::FETCH_ASSOC))
                    {
                        $user_id = $dt["user_id"];
                        $name = $dt["name"];						
                        $nric = $dt["nric"];
                        $password = $dt["password"];
                        $phone = $dt["phone"];
                        $position = $dt["position"];
                        $address = $dt["address"];
                        $status = $dt["status"];
                        $gender = $dt["gender"];
                        $state = $dt["state"];
                        $region = $dt["region"];
						$NoHouse = $dt["NoHouse"];
						$descendant = $dt["descendant"];
						$postal = $dt["postal"];
						$city = $dt["city"];
						$country = $dt["country"];
						$email = $dt["email"];
                       
                    }
                }
                else
                {
                    echo "Data is not found!";
                }
                ?> 
							<div class="col-lg-12">
								<form class="card" method="POST" action="registerUser_edit.php"  enctype="multipart/form-data">
									<div class="card-header">
										<h3 class="card-title">Update User</h3>
									</div>
									<div class="card-body">
										<div class="row">
                                            <div class="col-sm-6 col-md-12">
												<div class="form-group">
													<label class="form-label">Full Name</label>
													<input type="text" class="form-control" name="name" id="name" value="<?php echo $name; ?>" readonly >
												</div>
											</div>
											<div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">NRIC / Passport</label>
													<input type="text" class="form-control" name="nric" id="nric" value="<?php echo $nric; ?>" readonly >
                                                    <input type="text" class="form-control" name="password" id="password" value="<?php echo $password; ?>" hidden>
                                                    <input type="text" class="form-control" name="user_id" id="user_id" value="<?php echo $user_id; ?>" hidden>
												</div>
											</div>
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Contact Number</label>
													<input type="text" class="form-control" name="phone" id="phone" value="<?php echo $phone; ?>" readonly>
												</div>
											</div>
                                             <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Email</label>
													<input type="text" class="form-control" name="email" id="email" value="<?php echo $email; ?>" readonly >
												</div>
											</div>
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">No. Unit House</label>
													<input type="text" class="form-control" name="NoHouse" id="NoHouse" value="<?php echo $NoHouse; ?>" readonly >
                                   
												</div>
											</div>
                                                <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Position</label>
													<input type="text" class="form-control" name="NoHouse" id="NoHouse" value="<?php echo $position; ?>" readonly>
												</div>
											</div>
                                           
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Status</label>
													<input type="text" class="form-control" name="status" id="status" value="<?php echo $status; ?>" readonly>
												</div>
											</div>
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Gender</label>
													<input type="text" class="form-control" name="gender" id="gender" value="<?php echo $gender; ?>" readonly>
												</div>
											</div>
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">State</label>
													<input type="text" class="form-control" name="state" id="state" value="<?php echo $state; ?>" readonly>
												</div>
											</div>
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Region</label>
													<input type="text" class="form-control" name="region" id="region" value="<?php echo $region; ?>" readonly>
												</div>
											</div>
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Descendant</label>
													<input type="text" class="form-control" name="descendant" id="descendant" value="<?php echo $descendant; ?>" readonly>
												</div>
											</div> 
                                            <div class="col-sm-6 col-md-12">
												<div class="form-group">
													<label class="form-label">Address</label>
													<textarea class="form-control" name="address" rows="6" readonly placeholder=""><?php echo $address; ?></textarea>
												</div>
											</div> 
											<div class="col-md-5">
												<div class="form-group">
													<label class="form-label">Postal Code</label>
													<input type="text" class="form-control"  placeholder="e.g : 86400" name="postal" id="postal"    value="<?php echo $postal; ?>" readonly >
												</div>
											</div>
											<div class="col-sm-6 col-md-3">
												<div class="form-group">
													<label class="form-label">City</label>
													<input type="text" class="form-control" placeholder="e.g : Parit Raja" name="city" id="city"  value="<?php echo $city; ?>" readonly >
												</div>
											</div>
											<div class="col-sm-6 col-md-4">
												<div class="form-group">
													<label class="form-label">Country</label>
													<input type="text" class="form-control" placeholder="" name="country" id="country"  value="<?php echo $country; ?>" readonly >
												</div>
											</div>						
										</div>
									</div>
									<div class="card-footer text-right">
                                        <button type="submit" class="btn btn-secondary">Back</button>
                                        
									</div>
                                    
								</form>
							</div>
						</div>
					</div>
					
<?php
    
    include('footer.php');
?>

