<?php
    
    include('header.php');
?>

				<div class="app-content my-3 my-md-5">
					<div class="side-app">
						<div class="page-header">
							<h4 class="page-title">Full Calendar</h4>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#">Calendar</a></li>
								<li class="breadcrumb-item active" aria-current="page">Full calendar</li>
							</ol>
						</div>

						<div class="">
							<div class="card">
								<div class="card-status bg-primary br-tr-3 br-tl-3"></div>
								<div class="card-body">
									 <div id='calendar1'></div>
								</div>
							</div>
						</div>
						<div class="">
							<div class="card">
								<div class="card-status bg-primary br-tr-3 br-tl-3"></div>
								<div class="card-body">
									 <div id='calendar'></div>
								</div>
							</div>
						</div>
					</div>
	
<?php
    
    include('footer.php');
?>
