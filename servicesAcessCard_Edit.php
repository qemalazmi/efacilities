<!doctype html>
<html lang="en" dir="ltr">
<?php
    
    include('header.php');
?>
				<div class="my-3 my-md-5 app-content">
					<div class="side-app">
						<div class="page-header">
							<h4 class="page-title">Manage Services > Apply For Access Card </h4>
							
						</div>

						<?php
                            if(isset($_GET["id"]))
                            {
                                $id = $_GET["id"];
                        $sql = 
                            
                       /*"SELECT incident_id, id,title, description, location, latitude, longitude, incident_type_id as type, assigned_to, reported_time ,related_incident, comment,incident.zone_id, incident.image, image_after, incident.status, validated, user_id AS staff_id, user_name, email,phone_no  FROM incident
                        INNER JOIN USER
                        ON incident.reported_by = USER.email
                        WHERE incident.incident_id = :id

                        UNION

                        SELECT incident_id, id, title, description, location, latitude, longitude, incident_type_id as type, assigned_to, reported_time, related_incident, comment,incident.zone_id, incident.image, image_after, incident.status, validated, staff_id AS staff_id, user_name, email,phone_no FROM incident
                        INNER JOIN staff
                        ON incident.reported_by = staff.email
                        WHERE incident.incident_id = :id";*/
                            "SELECT * FROM services_accesscard where accessCard_id = :id" ;
                         
                    $stmt = $conn->prepare($sql);
                    $stmt->bindParam(":id", $id);
                    $stmt->execute();

                                if($dt = $stmt->fetch(PDO::FETCH_ASSOC))
                                {
                                    $accessCard_id = $dt["accessCard_id"];
                                    $user_id = $dt["user_id"];
                                    $name = $dt["name"];
                                    $email = $dt["email"];                                    
                                    $phone = $dt["phone"];
                                    $NoHouse = $dt["NoHouse"];
                                    $Novehicle = $dt["Novehicle"];
                                    $typevehicle = $dt["typevehicle"];
                                    $fee = $dt["fee"];
                                    $status = $dt["status"];
                                }
                            }
                            else
                            {
                                echo "Data is not found!";
                            }
                            ?>
							
							<div class="col-lg-12">
								<form class="card" method="POST" action="servicesAccessCard_update.php">
									<div class="card-header">
										<h3 class="card-title">Apply For Access Card </h3>
									</div>
									<div class="card-body">
										<div class="row">
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Full Name</label>
													<input type="text" name="name" id="name" class="form-control" readonly value="<?php  echo $name; ?>" >
													<input type="text" name="user_id" id="user_id" class="form-control" readonly value="<?php  echo $user_id; ?>" hidden="" >
													<input type="text" name="accessCard_id" id="accessCard_id" class="form-control" readonly value="<?php  echo $accessCard_id; ?>" hidden="" >
												</div>
											</div>
											<div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Contact Number</label>
													<input type="text" name="phone" id="phone" class="form-control" readonly value="<?php  echo $phone; ?>" >
												</div>
											</div>                                             
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Email</label>
													<input type="email" name="email" id="emai" class="form-control" readonly value="<?php  echo $email; ?>" >
												</div>
											</div>
											<div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">No. Unit House</label>
													<input type="text" name="NoHouse" id="NoHouse" class="form-control" readonly value="<?php  echo $NoHouse; ?>" >
												</div>
											</div>
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">No. vehicle</label>
													<input type="text" name="Novehicle" id="Novehicle" class="form-control" placeholder="ABC 1234"  value="<?php  echo $Novehicle; ?>">
												</div>
											</div>
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Type vehicle</label>
                                                    <select class="form-control select2 custom-select" data-placeholder="Choose one" name="typevehicle" id="typevehicle" required>
                                                            <option value="" <?php if($typevehicle == "NULL"){ echo "selected"; }else{} ?>>-- Choose --</option>
                                                           <option value="Car" <?php if($typevehicle == "Car"){ echo "selected"; }else{} ?>>Car</option>
                                                            <option value="motorcycle" <?php if($typevehicle == "motorcycle"){ echo "selected"; }else{} ?>>motorcycle</option>
                                                        
                                                     </select>
												</div>
											</div>
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Fee Access Card</label>
													<input type="text" name="fee" id="fee" class="form-control" value="RM50.00" readonly>
												</div>
											</div>
                                            <?php
                                          if($job == "Admin")
                                          {
                                            ?>
                                               <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Status</label>
													<select class="form-control select2 custom-select" data-placeholder="Choose one" name="status" id="status">
                                                             <option value="" <?php if($status == "NULL"){ echo "selected"; }else{} ?>>-- Choose --</option>
                                                           <option value="New" <?php if($status == "New"){ echo "selected"; }else{} ?>>New</option>
                                                            <option value="Approve" <?php if($status == "Approve"){ echo "selected"; }else{} ?>>Approve</option>
                                                            <option value="Reject" <?php if($status == "Reject"){ echo "selected"; }else{} ?>>Reject</option>
                                                     </select>
                                                    
												</div>
											</div>
                                            <?php   
                                          }
                                          
                                          else
                                          {
                                              ?>
                                                
                                                <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">status</label>
													<input type="text" class="form-control" name="status" id="status" value="<?php echo $status; ?>" readonly>
                                                    
												</div>
											</div>
                                              <?php
                                          }
                                            ?>
                                            
                                             
                                            
                                             <div class="col-sm-6 col-md-12">
												<div class="form-group">
													<p>Syarat-syarat Pengguna kawasan kolam renang</p>
                                                    <p>1. Pihak pengurusan tidak akan bertanggungjawab ke atas kehilangan atau kerosakan kenderaan yang diletakkan disekitar kawasan tempat letak kereta di dalam kondominium ini. </p>
                                                    <p>2. permohon dengan ini bersetuju untuk meletak didalam petak yang telah ditetapkan. Sekiranya permohon gagal untuk berbuat demikian akan menyebabkan kenderaan permohon dikunci oleh pihak pengurusan dan denda RM50 serta tambahan RM10 mengikut jumlah hari kenderaan dikunci unutk membuka kunci tersebut.</p>
                                                    <p>3. Motosikal hendaklah diletakkan di kawasan parkir motosikal di tangkat 1A sahaja.</p>
                                                    <p>4. Permohon dengan ini bersetuju untuk tidak meletakkan apa-apa objek di petak tersebut. Pihak pengurusan berhak mengalih oleh tersebut tanpa notis diberikan.</p>
                                                    <p>5. Petak parkir pelawat hanya dikhaskan kepada pelawat sahaja.</p>
                                                    <p>6. permohon bersetuju untuk mematuhi syarat-syarat yang ditetapkan.</p>
												</div>
											</div>
										</div>
									</div>
                                        <?php
                                          if($job == "Admin")
                                          {
                                            ?>
                                               
                                            <?php   
                                          }
                                          
                                          else
                                          {
                                              ?>
                                                
                                                 <div class="col-sm-6 col-md-12">
												<div class="form-group">
													<div class="form-label">Toggle switch single</div>
													<label class="custom-switch">
														<input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input" required>
														<span class="custom-switch-indicator"></span>
														<span class="custom-switch-description">I agree with terms and conditions</span>
													</label>
												</div>
											</div>
                                              <?php
                                          }
                                            ?>
                                  
										
									
									<div class="card-footer text-right">
                                        <button type="submit" class="btn btn-secondary" >Back</button>
										<button type="submit" class="btn btn-primary" >Update For Apply Access Card</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					
<?php
    
    include('footer.php');
?>

