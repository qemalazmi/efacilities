<?php
    include('header.php');
    include("session.php");

            // Set Default Time Zone for Asia/Kuala_Lumpur
            date_default_timezone_set("Asia/Kuala_Lumpur");

            // Check, if username session is NOT set then this page will jump to login page
            if (!isset($_SESSION['session']) && !isset($_SESSION['job'])) {
                header('Location: login.php');
                //session_destroy();
            }
$job = $_SESSION["job"];


?>

				<div class="app-content my-3 my-md-5">
					<div class="side-app">
						<div class="page-header">
							<h4 class="page-title">Manage Services > Swimming Pool</h4>
							<div class="card-footer text-right">
                                       
										      <button type="submit" class="btn btn-primary" onclick="window.location.href='servicesPool_Apply.php'" >Apply for Use Swimming Pool</button>
                                       
				            </div>
                            
						</div>
						<div class="row">
							<div class="col-md-12 col-lg-12">
								<div class="card">
									<div class="card-status bg-yellow br-tr-3 br-tl-3"></div>
									<div class="card-header">
										<div class="card-title">Record Apply for Use Swimming Pool</div>
									</div>
                                    
									<div class="card-body">
										<div class="table-responsive">
											<table id="example" class="table table-striped table-bordered" style="width:100%">
												<thead>
													<tr>
														<th class="wd-20p">Detail Application </th>
														<th class="wd-20p">Date</th>
														<th class="wd-10p">Time</th>
														<th class="wd-10p">Status</th>
														<th class="wd-10p">Total Use</th>
														
                                                        <th class="wd-20p">Action</th>
                                                        <th class="wd-20p">Action</th>
														
													</tr>
												</thead>
												<tbody>
													 <?php
                                                     $counter = 1;
                                                       if($job == "Admin"){
                                                           
                                                                $statement = $conn->prepare("SELECT * FROM services_pool WHERE status = 'New'");
                                                           
                                                       }else {
                                                            $statement = $conn->prepare("SELECT * FROM services_pool ");
                                                           
                                                       }
                                                                $statement->execute();

                                                               while($data = $statement->fetch(PDO::FETCH_ASSOC))
                                                               {
                                                                   
                                                               ?>
                                                                    <tr>
                                                                      <td><?php echo $data["name"]; ?>
                                                                        <?php echo $data["email"]; echo "<br>";
                                                                              echo $data["phone"]; echo "<br>";
                                                                              echo $data["NoHouse"]; echo "<br>";
                                                                              
                                                                          ?>
                                                                        </td>
                                                                      <td><?php echo $data["setdate"]; ?></td> 
                                                                      <td><?php echo $data["time"]; ?></td> 
                                                                      <td><?php echo $data["status"]; ?></td> 
                                                                      <td><?php echo $data["total"]; ?></td> 
                                                                      
                                                                        
                                                                        <td style="text-align: center;vertical-align: middle;">
                                                                            <a href="servicesPool_View.php?id=<?php echo $data["servicesPool_id"]; ?>">
                                                                              <button type="button" name="update" id="update" class="btn btn-info btn-sm">
                                                                                  <i class="fa fa-pencil"></i>&nbsp;&nbsp;View
                                                                              </button>

                                                                            </a>
                                                                        </td>
                                                                        <td style="text-align: center;vertical-align: middle;">
                                                                            <a href="servicesPool_Edit.php?id=<?php echo $data["servicesPool_id"]; ?>">
                                                                              <button type="button" name="update" id="update" class="btn btn-info btn-sm">
                                                                                  <i class="fa fa-pencil"></i>&nbsp;&nbsp;Edit
                                                                              </button>

                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                               <?php
                                                                   $counter++;
                                                               }
                                                               ?>
                                                                        
												</tbody>
											</table>
										</div>
									</div>
									<!-- table-wrapper -->
								</div>
								<!-- section-wrapper -->

							</div>
						</div>
					</div>
                    <?php
    
                        include('footer.php');
                    ?>
