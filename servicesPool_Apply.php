<!doctype html>
<html lang="en" dir="ltr">
<?php
    
    include('header.php');
?>
				<div class="my-3 my-md-5 app-content">
					<div class="side-app">
						<div class="page-header">
							<h4 class="page-title">Manage Services > Swimming Pool </h4>
							
						</div>

						<?php
                            if(isset($_SESSION["session"]))
                            {
                                $email = $_SESSION["session"];
                                $sql = "SELECT * FROM user WHERE email = :email";
                                $stmt = $conn->prepare($sql);
                                $stmt->bindParam(":email", $email);
                                $stmt->execute();

                                if($dt = $stmt->fetch(PDO::FETCH_ASSOC))
                                {
                                   $user_id = $dt["user_id"];
                                    $name = $dt["name"];
                                    $email = $dt["email"];
                                    $password = $dt["password"];
                                    $phone = $dt["phone"];
                                    $NoHouse = $dt["NoHouse"];
                                }
                            }
                            else
                            {
                                echo "Data is not found!";
                            }
                            ?>
							
							<div class="col-lg-12">
								<form class="card" method="POST" action="servicesPool_insert.php">
									<div class="card-header">
										<h3 class="card-title">Register For Use Swimming Pool</h3>
									</div>
									<div class="card-body">
										<div class="row">
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Full Name</label>
													<input type="text" name="name" id="name" class="form-control" readonly value="<?php  echo $name; ?>" >
													<input type="text" name="user_id" id="user_id" class="form-control" readonly value="<?php  echo $user_id; ?>" hidden="" >
												</div>
											</div>
											<div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Contact Number</label>
													<input type="text" name="phone" id="phone" class="form-control" readonly value="<?php  echo $phone; ?>" >
												</div>
											</div>                                             
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Email</label>
													<input type="email" name="email" id="emai" class="form-control" readonly value="<?php  echo $email; ?>" >
												</div>
											</div>
											<div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">No. Unit House</label>
													<input type="text" name="NoHouse" id="NoHouse" class="form-control" readonly value="<?php  echo $NoHouse; ?>" >
												</div>
											</div>
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Date</label>
                                                    <input class="form-control fc-datepicker" name="applydate" id="applydate" placeholder="MM/DD/YYYY" type="date" required>
												</div>
											</div>
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Time</label>
                                                    <input class="form-control" id="tp2" placeholder="Set time" name="applytime" type="text">
                                                    
												</div>
											</div>
                                            <div class="col-sm-6 col-md-12">
												<div class="form-group">
													<label class="form-label">Total for use Swimming Pool</label>
													<select class="form-control select2 custom-select" data-placeholder="Choose one" name="total" id="total">
                                                            <option label="Choose one"></option>
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                     </select>
												</div>
											</div>
                                            
                                             <div class="col-sm-6 col-md-12">
												<div class="form-group">
													<p>Syarat-syarat Pengguna kawasan kolam renang</p>
                                                    <p>1. Hanya pemilik dan penyewa yang telah berdaftar sahaja yang boleh memohon untuk penggunaan kolam renang. </p>
                                                    <p>2. Semua permohonan mesti melalui pejabat pengurusan, pemohon dikehendaki mengisi borang permohonan</p>
                                                    <p>3. Permohon hendaklah tidak mempunyai sebarang tunggakan dalam pembayaran MF/SF</p>
                                                    <p>4. Bayaran sewa dan deposit mesti dibayar sebelum kebenaran secara bertulis diberikan kepada permohon</p>
                                                    <p>5. Tanpa kebenaran bertulis daripada pihak JMB, pengguna kolam untuk acara peribadi adalah tidak dibenarkan</p>
                                                    <p>6. Permohonan pengguna kawasan kolam renang hendaklah dibuat sekurang-kurangnya dalam tempoh dua minggu sebelum tarikh yang dirancang</p>
                                                    <p>7. Penyewa adalah bertanggungjawab sepenuhnya dari segi keselamatan dan kesersihan</p>
                                                    <p>8. Pihak JMB tidak akan bertanggungjawab keatas sebarang kecedereaan, kecelakaan dan kerosakan yang berlaku semasa acara dijalankan.</p>
                                                    <p>9. Tiada minuman keras dibenarkan di sekitar premis</p>
                                                    <p>10. Hanya makanan halal sahaja dibenarkan.</p>
                                                    <p>11. Peraturan dan tatacara mengguna kolam renang hendaklah diikuti pada setiap masa.</p>
                                                    <p>12. Sebarang kerosakan harta benda perilaku tetamu-tetamu adalah tanggungjawab permohon, Kos pembersihan dan permbaikan akan ditanggung oleh permohon</p>
                                                    <p>13. Permohon bertanggungjawab menjaga kebersihan kawasan sekitar kolam renang sebarang kekotoran dan kerosakan harta benda akan mengakibatkan deposit tidak kembalikan.</p>
                                                    <p>14. Sebarang kekotoran dan pelanggaran peraturan-peraturan yang ditetapkan, deposit tidak akan dipulangkan dan nama penyewa/permohon akan disenaraikan hitam.</p>
                                                    <p>15. Kebenaran pengguna kolam renang adalah tertakluk kepada budi bicara pihak JMB dan bergantung kepada keadaaan semasa.</p>
												</div>
											</div>
										</div>
									</div>
                                   <div class="col-sm-6 col-md-12">
												<div class="form-group">
													<div class="form-label">Toggle switch single</div>
													<label class="custom-switch">
														<input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input" required>
														<span class="custom-switch-indicator"></span>
														<span class="custom-switch-description">I agree with terms and conditions</span>
													</label>
												</div>
											</div>
										
									
									<div class="card-footer text-right">
                                        <button type="submit" class="btn btn-secondary" >Cancel</button>
										<button type="submit" class="btn btn-primary" >Apply For Use Swimming Pool</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					
<?php
    
    include('footer.php');
?>

