<?php
// Connecting to the database
include("connection.php");
include("session.php");

  if (!isset($_SESSION['session'])) {
        header('Location: index.php');
        session_destroy();
    }
      
header('Location: servicesVisitor.php');
?>