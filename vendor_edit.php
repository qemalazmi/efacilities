<?php
    include('header.php');
    include("session.php");

            // Set Default Time Zone for Asia/Kuala_Lumpur
            date_default_timezone_set("Asia/Kuala_Lumpur");

            // Check, if username session is NOT set then this page will jump to login page
            if (!isset($_SESSION['session']) && !isset($_SESSION['job'])) {
                header('Location: login.php');
                //session_destroy();
            }
?>
    
   
    
				<div class="my-3 my-md-5 app-content">
					<div class="side-app">
						<div class="page-header">
							<h4 class="page-title">Edit Vendor</h4>
							
						</div>
							<div class="col-lg-12">
								<form class="card" method="POST" action="vendor_update.php">
									<div class="card-header">
										<h3 class="card-title">UPDATE INFORMATION VENDOR</h3>
									</div>
                                    
									<div class="card-body">
                                        <?php
                                         //--------------code get data ----------------

                                            if(isset($_GET["VendorID"]))
                                            {
                                                $VendorID = $_GET["VendorID"];

                                                $sql ="SELECT * FROM vendor where VendorID = :VendorID"; 

                                                $stmt = $conn->prepare($sql);
                                                $stmt->bindParam(":VendorID", $VendorID);
                                                $stmt->execute();

                                            if($dt = $stmt->fetch(PDO::FETCH_ASSOC))
                                            {
                                                $VendorID = $dt["VendorID"];
                                                $CompanyName = $dt["CompanyName"];
                                                $OfficeAddress = $dt["OfficeAddress"];
                                                $PostalCode = $dt["PostalCode"];
                                                $City = $dt["City"];
                                                $Country = $dt["Country"];
                                                $OfficeContact = $dt["OfficeContact"];
                                                $StartMaintenance = $dt["StartMaintenance"];
                                                $EndMaintenance = $dt["EndMaintenance"];
                                                $NameOfOfficer = $dt["NameOfOfficer"];
                                                $TypeMaintenance = $dt["TypeMaintenance"];
                                                $ContactOfOfficer = $dt["ContactOfOfficer"];
                                                //Current Date/Time
                                                $now = date("Y/m/d h:m:s");
                                            }
                                        }
                                        else
                                        {
                                            echo "Data is not found!";
                                        }
                                        ?>
                                       
		
                                        
										<div class="row">
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">													
													<input type="text" name="CompanyName" id="CompanyName" class="form-control" value="<?php echo $CompanyName; ?>" >
                                                    <input type="text" name="VendorID" id="VendorID" class="form-control" value="<?php echo $VendorID; ?>" hidden>
												</div>
											</div>
											<div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Office Contact</label>
													<input type="text" name="OfficeContact" id="OfficeContact" class="form-control" value="<?php echo $OfficeContact; ?>" >
												</div>
											</div>                                             
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Name of Officer</label>
													<input type="text" name="NameOfOfficer" id="NameOfOfficer" class="form-control" value="<?php echo $NameOfOfficer; ?>" >
												</div>
											</div>
											<div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Contact of Officer</label>
													<input type="text" name="ContactOfOfficer" id="ContactOfOfficer" class="form-control"  value="<?php echo $ContactOfOfficer; ?>"   >
												</div>
											</div>
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Starting Vendor Maintenance</label>
                                                    <input class="form-control fc-datepicker" name="StartMaintenance" id="StartMaintenance" placeholder="MM/DD/YYYY" type="date"  value="<?php echo $StartMaintenance; ?>" >
												</div>
											</div>
                                            <div class="col-sm-6 col-md-6">
												<div class="form-group">
													<label class="form-label">Ending Vendor Maintenance</label>
                                                    <input class="form-control fc-datepicker" name="EndMaintenance" id="EndMaintenance" placeholder="MM/DD/YYYY" type="date"  value="<?php echo $EndMaintenance; ?>" >
												</div>
											</div>
                                            <div class="col-sm-6 col-md-12">
												<div class="form-group">
													<label class="form-label">List Maintenance</label>
													<input type="text" name="TypeMaintenance" id="TypeMaintenance" class="form-control"  value="<?php echo $TypeMaintenance; ?>"  >
												</div>
											</div>
                                            <div class="col-sm-6 col-md-12">
												<div class="form-group">
													<label class="form-label">Office Address</label>
													<input type="text" name="OfficeAddress" id="OfficeAddress" class="form-control" value="<?php echo $OfficeAddress; ?>"   >
												</div>
											</div> 
											<div class="col-md-5">
												<div class="form-group">
													<label class="form-label">Postal Code</label>
													<input type="text" name="PostalCode" id="PostalCode" class="form-control"  placeholder=""  value="<?php echo $PostalCode; ?>"  >
												</div>
											</div>
											<div class="col-sm-6 col-md-3">
												<div class="form-group">
													<label class="form-label">City</label>
													<input type="text" name="City" id="City" class="form-control" placeholder="" value="<?php echo $City; ?>" >
												</div>
											</div>
											<div class="col-sm-6 col-md-4">
												<div class="form-group">
													<label class="form-label">Country</label>
													<input type="text" name="Country" id="Country" class="form-control" placeholder=""  value="<?php echo $Country; ?>" >
												</div>
											</div>						
										</div>
									</div>
									<div class="card-footer text-right">
                                        <button type="submit" class="btn btn-secondary" >Cancel</button>
										<button type="submit" class="btn btn-primary" >Update</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					
<?php
    
    include('footer.php');
?>

