<?


/*******************************************************************************
You can add as many questions and answers as you like
This is a PHP script, so make sure you follow the correct syntax
Always escape special characters such as apostrophe, quotes with backslashes

Format: $answers['CommonQuestion 1'] = "Common Answer 1";

*********************************************************************************/



$answers['SUBMIT GAMBAR TAPI TAK SUBMIT BORANG']="Terima kasih kerana menghantar salinan gambar bukti ini.

Bagaimanapun, sila isi borang notifikasi supaya saya dapat berikan userid dan password yang khas dan unik untuk anda.

Link untuk ke borang notifikasi ada di dalam e-mail asal saya yang mengandungi maklumat akaun bank saya. Periksa kembali e-mail yang anda dapat sebelum ini dan cari link ke borang notifikasi ini. Pastikan anda attach gambar bukti di sini.

Jika tidak jumpa link untuk submit borang notifikasi, sila tekan butang tempahan pada laman web semula, ikut segala arahan semula dan lakukan tempahan lagi sekali.

Selepas anda lakukan ini, sila hubungi saya semula di sini. Saya akan proses dengan secepat mungkin selepas anda isi borang notifikasi dengan betul.

Diharap anda dapat membantu saya supaya dapat membantu anda dapat userid dan password unik yang khas untuk anda ini.

Jika saya tidak menerima apa-apa respon dalam masa 24 jam saya akan delete email ini.

Kerjasama anda adalah amat di hargai. Terima kasih.
";
$answers['LAYAK KOMISYEN, TAPI USER CHECK TAKDE DALAM AKAUN']="Pembayaran komisen adalah di lakukan pada setiap hari Selasa dan Jumaat. Untuk pemegang akaun bank selain dari Maybank, pembayaran dilakukan melalui interbank transfer (IBG) yang akan mengambil masa antara 1 ke 2 hari untuk clear.

Jadi, saya minta anda bersabar dan tunggu untuk duit clear dulu. Jika tidak menerima apa-apa selepas 4 hari, baru hubungi saya semula mengenai ini.

Jika nak cepat lain kali gunakan Maybank untuk dapatkan komisen serta-merta pada hari yang sama.

Juga perlu diingatkan BSN, Muamalat dan Agrobank tidak diterima untuk pembayaran komisen. 

Kerjasama anda adalah amat di hargai. Terima kasih.
";
$answers['LINK X DAPAT WALAUPUN DAH BANK IN']="Terima kasih kerana pembelian anda.

Proses untuk memberi link bersama userid dan password selalunya mengambil masa antara 1 ke 2 hari untuk di lakukan.

Jika anda membayar dengan bank yang selain dari Maybank dengan menggunakan interbank transfer (IBG), duit selalunya akan clear hanya dalam masa 24 jam. Jika pembayaran di lakukan pada lewat Jumaat, kemungkinan hanya akan clear pada hari Isnin berikutnya. Sila bersabar dan tunggu dulu sebelum bertanya lagi sekali dengan saya.

Juga, diingatkan anda supaya mengisi borang notifikasi dan attach gambar bukti untuk memudahkan proses memberi userid dan password kepada anda.
";



$answers['BNM ALERT LIST']="Untuk makluman, pihak JA tidak pernah terlibat dalam skim pelaburan atau ponzi. 
Pihak JA menjual produk Aplikasi Android yang berfungsi sebagai alat pemasaran mudahalih untuk mempromosikan program affiliate serta produk produk lain.

Aplikasi ini mampu untuk

1) Mencari prospek.
2) Menghantar mesej promosi secara auto.
3) Mencipta mesej promosi.
4) Meletakkan affiliate link peribadi anda pada mesej-mesej promosi.


Disebabkan populariti & banyak pengguna yg bertanya kepada BNM samada JA adalah Finance Company yg berdaftar.
JA hanya menjual Aplikasi Android untuk Internet Marketing.
Dan disebabkan itu, Consumer Alert itu dikeluarkan sekiranya ada yg beranggapan JA melakukan Financial Trading, MLM dan sebagainya.


Sekali lagi dinyatakan, JutawanApp bukan sebarang program pelaburan atau ponzi dan tidak memerlukan apa-apa lesen dari Bank Negara atau Suruhanjaya Sekuriti.




Sekian.";


$answers['SCREEN OVERLAY ERROR']="Salam, selalunya ini berlaku jika anda add Apps lain yg and install sedang menggunakan skrin tersebut..rujuk laman web bawah untuk penyelesaian

https://www.howtogeek.com/271519/how-to-fix-the-screen-overlay-detected-error-on-android/";


$answers['AUTO POST FB']="Salam, untuk menggunakan Auto Post FB, sila pergi ke laman VIP di http://www.jutawanapp.com/bravo. Masukkan username dan passwod anda, kemudian baca panduan penggunaan Auto Post Auto Schedule FB di ruang Bonus.";

$answers['Download Link']="Untuk makluman, tuan/puan boleh muat turun aplikasi beserta bonus-bonus lain dari laman web download. 
Alamat laman web download adalah seperti berikut

http://www.jutawanapp.com/bravo/

Anda perlu login menggunakan username dan password seperti dibawah:";

$answers['Bagaimana nak mula?']="Pertamanya, terima kasih kerana telah membeli produk aplikasi yang saya telah cipta.

Ini langkah-langkah penting yang anda perlu lakukan...

1. Pastikan telah membaca ebook dulu dan mengikut segala arahan yang telah diberi.

2. Pastikan Akaun Gmail/Yahoo/Outlook atau GMX yang baru telah di daftar

3. Pastikan setting akaun gmail/yahoo betul seperti yang telah diceritakan dalam ebook (terutamanya Allow less Secure Apps settings)

4. Sila periksa log untuk lihat jika ada apa-apa message auto-promo. Jika tiada bermakna anda telah setting app tak betul.

Saya minta ambil masa untuk membaca ebook arahan dulu dan mengikut betul-betul.

Sekian
Afiq";

$answers['Proses automatik berjaya']="Ok begini, setiap beberapa minit , app saya akan membuat auto-promo untuk anda. Mesej promosi akan dihantar secara automatik dari akaun baru Gmail yang anda cipta jika anda menurut arahan dalam ebook. Perlu ditekankan, app akan promo database contacts bukan dalam phone anda...jadi anda tidak perlu risau jika mesej sampai ke contacts anda...

Setiap kali proses otomatik berjaya, mesej notifikasi akan dipaparkan untuk menunjukkan yg app berfungsi seperti normal..";

$answers['Bila dapat hasil?']="Salam Tuan/Puan,

Pertama, tolong cek log app/notification bar - dan berapa banyak notifikasi yg mengatakan proses automatik berjaya..?

Jika baru sedikit, sila beri masa lagi..kadang-kadang akan mengambil masa sikit untuk mesej auto-promo tu dihantar. kemungkinan juga recipient lambat membaca email mereka...

Jika app anda berfungsi dengan betul  , robot sedang tekun buat kerja promosi untuk tuan...seperti lumrah alam, setengah org dapat cepat , setengah lewat...";

$answers['Enable less secure apps (Gmail/Yahoo)']="Sila pastikan email anda dah enable Less Secure App seperti yang saya terangkan di dalam ebook. Contoh berikut untuk Gmail. Login ke akaun email yang anda ingin masukkan dan pergi ke laman berikut:

https://myaccount.google.com/u/0/lesssecureapps?pli=1

Lihat status, jika laman itu menunjukkan maklumat berikut:

Allow less secure apps: OFF

Klik butang di situ supaya status menjadi:

Allow less secure apps: ON

Selepas itu baru masukkan maklumat ke Setting.";


$answers['Enable less secure apps (Hotmail/Live)']="Sila pastikan email anda dah enable Less Secure App seperti yang saya terangkan di dalam ebook. 

Rujuk di mukasurat 33 pada ebook panduan penggunaan.";


$answers['Refund 1']="Salam....Terima kasih atas pembelian anda.

Maaf anda tak puas hari dengan produk ini. Untuk makluman, Ini bukan skim cepat kaya, duit datang dari sumber yg tak tahu atau Syubhah. Sumber keuntungan adalah dari hasil jualan dan telah dijelaskan dalam syarat dan terma program affiliate yang anda telah setuju semasa anda membuat pembelian tempohari.

Apa-apa pun, anda dah beli produk, gunakan kelebihannya. Kalau ada masalah, bagi details apa yg telah anda lakukan dan lain-lain supaya boleh di bantu.

Saya di sini untuk membantu.


Juga, harap semak terma dan syarat. Saya hanya akan mengembalikan wang hanya sekiranya app rosak atau mempunyai masalah, contohnya tak boleh install, sentiasa crash atau lain-lain masalah. 

Jaminan pulangan wang dari tempat-tempat lain tidak diterimapakai. 

Sekiranya anda mendapat jaminan tersebut dari tempat-tempat lain, sila maklumkan laman web / page mereka di sini untuk kami ambil tindakan undang-undang. 
Pihak kami juga akan menamakan anda sebagai saksi dalam tuntutan kami nanti. Harap maklum dan terima kasih.


";

$answers['Refund 2']="Salam...

Saya meminta maaf kika anda tidak berpuas hati dengan pembelian anda...

Anda kena cuba dahulu. Kalau tidak cuba, anda tidak akan tahu keupayaan anda. Benda ini bukannya susah sangat. Teknik dia senang aje. Kalau dah biasa buat jadi kacang. Cam belajar bawa basikal. Setengah org dapat cepat setengah org dapat lambat.

Anda sebenarnya telah pun memiliki sebuah app yg secara automatik bekerja untuk anda membuat promosi dan mencari prospek. 

Saya akan tolong anda untuk mana-mana bahagian yang anda tidak tahu, tapi anda sendiri kena buat. Anda kena mulakan. Kalau tak mulakan langkah pertama tu, sampai bila-bila pun takkan kemana-mana.

Akan tetapi, jika anda membuat keputusan juga untuk refund, sila rujuk kepada terma dan syarat yang kita SAMA-SAMA setuju sebelum anda membuat pembelian, Terma dan syarat ini ada terkandung dalam order form laman web (http://www.jutawanapp.com/order.php). Sila kemukakan semua maklumat yang di minta terutamanya para 5(g) untuk memulakan proses refund.

Sekian";

$answers['Refund 3-Istiqamah']="Ada seorang pembeli nama dia Ganason, dia ni join awal sikit dari awak. Dia mula-mula tanya macam-macam, sama jugak cam awak. Cuma bila dia buat aje semua yang saya ajarkan tanpa jemu secara istiqmah baru dia berjaya buat duit. Pendapatan dia skrg? 4 angka seminggu, bukan sahaja dari site saya, tapi dari site-site orang lain. Awak tak perlu over convince orang lain untuk join, cuma perlu sedikit confidence. So, nasihat saya, istiqamah dan jangan mudah putus asa. Bila dah putus asa awak akan cakap program tak jalan la, tak boleh buat duit lah, semua tu doa. End up you'll get nothing sebab memang berdoa to get nothing...Tak lama lagi saya akan tambah lagi feature robot yg boleh buat auto-posting di banyak tempat lagi sebagai usaha untuk bantu anda...  Confident, doa dan istiqamah. OK?";



$answers['TUTORIAL 2 JAM EBOOK MANUAL - (HANYA UNTUK PEMBELI)']=" Salam..

Jika anda masih belum dapat memahami cara cara app JA berfungsi, rujuk tutorial video ini untuk panduan langkah demi langkah

https://www.youtube.com/watch?v=gEv-momScBw";



$answers['Bila nak dapat komisyen ni?']="Komisyen akan dapat apabila app membuat sale untuk anda....setengah orang dapat cepat - dalam beberapa jam, setengah org dapat lewat sedikit..beberapa-hari atau 1-2 minggu pun anda....ada juga yg dapat dalam x sampai 2 jam...pokoknya, app sedang bekerja keras untuk anda...jadi beri peluang untuk app membuat sale untuk anda...saya pun sentiasa sedang update aplikasi dan kaji lagi bagaimana app dapat mencari prospek yg sesuai untuk anda....

dia macam dalam hidup ini lah, kalau nasib baik dalam masa 24 jam dapat duit

saya ada nampak orang beli app, bila saya issue password dalam masa beberapa jam dia dah dapat sale

tapi ada setengah orang sampai 3 minggu tak dapat apa-apa

tapi kena mencuba, peralatan canggih dah diberi... kalau dah rezeki tak ke mana

saya cadangkan anda juga boleh buat promo sendiri - atau promo product lain guna robot ni..rujuk pada ebook panduan untuk cara-caranya..

Pastikan anda baca betul betul syarat dan terma program affiliate JutawanApp untuk memahami fungsi app dan bagaimana komisyen di jana.

";

$answers['Email Blocked']="Salam,

Kadang kala. pihak Google akan detect jika anda cuba membuka lebih dari 1 akaun email daripada IP yang sama. Ini tidak menyalahi apa-apa syarat dan terma cuma pihak Google ingin mengawal kuota pembukaan email. Cara yg mudah ialah untuk anda membuka akaun Gmail menggunakan alamat IP berbeza. Jika anda masuk melalui broadband di rumah seperti UNIFI, anda hanya perlu on-off atau reset modem. Jika anda masuk melalui 3G/4G dari phone anda, anda cuma perlu on-off phone anda untuk mendapatkan alamat IP yang baru.

Sepertimana yang anda mungkin tahu, app saya menggunakan teknologi canggih untuk menghantar auto-promo menggunakan sistem SMTP email seperti Gmail.

Anda juga boleh guna Yahoo, Outlook, GMX atau apa apa pelayan SMTP email yg anda ada";


$answers['Versi Baru']="
Anda masih menggunakankan app lama dan belum upgrade PERCUMA ke versi latest 10. Versi 10 ni ada feature baru power yg belum pernah di buat oleh sesiapa untuk generate prospek list.

Tolong login download page (http://www.jutawanapp.com/bravo) dan BACA EBOOK PANDUAN terbaru versi 14

Uninstall app lama kemudian install app baru version 10

Nak tanya dah masuk ethtrade dan gunakan app ini untuk auto- promosi?

Cuba dulu...

Apa-apa hal saya akan bantu";


$answers['Gmail Host & Port problem']="Ada 2 kemungkinan

1. Anda salah isi username dan password

2. Besar kemungkinan anda lupa nak set Allow Less Secure Apps pada setting Gmail tersebut.  Jika anda cuba masukkan settings untuk email penghantar Gmail dalam app JA SEBELUM anda Allow Less Secure Apps, pihak Gmail akan block akaun tersebut.

Cara menangani adalah anda perlu buka akaun Gmail baru, dan terus set Allow Less Secure Apps, kemudia baru masukkan ke dalam settings app JA.";

$answers['Apa maksud Host & Port']="Untuk makluman, Host dan Post adalah setting untuk pelayan email penghantar yang perlu di set untuk handphone anda. Sistem JA berkerja dengan membuat koneksi ke pelayan email menggunakan Host dan Port yg anda masukkan. Host dan Port berbeza untuk setiap pelayan email seperti Yahoo , Gmail, Outlook, GMX dan lain lain. (Anda sebenarnya boleh membuat koneksi ke beribu-ribu pelayan email termasuk syarikat anda (tapi saya tak galakkan anda sambung ke pelayan office anda).

Sila baca ebook panduan Versi 14 (download dari http://www.jutawanapp.com/bravo) yang memberi panduan jelas host dan port yang patut anda gunakan untuk pelayan email app.";


$answers['Macammana nak beli']="Sila lakukan tempahan secepat mungkin menggunakan

http://jutawanapp.com/order.php

Satu email bersama keterangan bagaimana untuk membayar akan dihantar kepada anda.

Dapatkan App saya ini secepat mungkin ok. Saya baru lanjutkan lagi tempoh promosi saya atas permintaan ramai. Tak tahu sampai bila boleh tahan pasal orang putih daripada Amerika pun dah desak saya hantar produk ini secepat mungkin. Tunggu kawan alih bahasa dulu. So harga sure melambung lepas ni!";



$answers['Tak isi borang notifikasi']="Salam Tuan/Puan,

Jika anda telah melakukan pembayaran, penting untuk anda isi Payment Notification Form. Link untuk form ini terdapat dalam email yg mengandungi maklumat pembayaran. Jika borang ini tidak diisi, sistem saya tidak dapat memproses pembayaran anda. Jadi, sila semak balik dan masukkan maklumat yg betul tentang cara dan tarikh bayaran anda di borang notifikasi tersebut.

Sekian";


$answers['Bagaimana app berfungsi']="Salam Tuan/Puan

App ini dihasilkan untuk memenuhi kehendak orang ramai yang secara amnya tidak mahu berjumpa orang, tidak mahu buat sale, tetapi nak duit dengan cepat tanpa usaha. Untuk makluman, tidak ada app seperti ini didalam pasaran di mana-mana di dunia..ini buan skim pelaburan haram yg menjanjikan pulangan peratusan tetap setiap bulan. Bayangkan, dengan app  saya akan:

1) Mencari dan merayau-rayau dalam internet dan media social untuk mendapatkan contact prospek database yg berminat dalam penjanaan wang dari Internet
2) Menghantar contact-contact ini ke phone anda (Bermakna anda tak perlu cari orang lagi).
3) Menerbitkan mesej-mesej promosi yg power yang custom built untuk anda. Memasukkan link affiliate anda secara auto dalam mesej anda.
4) Menghantar mesej email secara auto ke prospek-prospek anda walaupun anda sedang tidor (Ada tak perlu promo secara manual) - Apa-apa produk atau promosii pun boleh dicustom untuk anda.

App ini sebenarnya adalah hamba abdi anda yg bekerja siang malam selagi mana ada sambungan Internet di app anda.";


$answers['Semak Log Aktiviti']="Salam Tuan/Puan,

Minta tuan cek Log Aktiviti di app anda. Ini untuk pastikan tiada error message. Jika ada error atau warning meseg, check dulu kod error tersebut. Robot akan bagi cadangan untuk tukar email penghantar dan lain-lain.

Kadang-kadang ada warning tapi robot akan keep retry sampai boleh send email. Anda juga boleh log masuk akaun email penghantar jikalau ada mesej dari Gmail atau Yahoo yang suruh reset email anda

";



$answers['Macammana nak install']="Langkah pertama yang anda perlu lakukan ialah untuk muat turun aplikasi  dan install ke telefon bimbit Android anda.

Pastikan anda mengikuti arahan betul-betul langkah demi langkah. Jika anda mengalami apa-apa masalah, anda boleh berhubung dengan saya melalui sistem bantuan pelanggan.

PALING PENTING Pertama sekali anda perlu menukar setting perkakas Android anda dengan pergi ke Settings -> Security dan kemudian tandakan kotak Unknown Sources.

Sila lakukan ini kalau tidak anda tidak akan dapat install app.";


$answers['Saya dah bayar mana app']="Salam Tuan/Puan,

Penghantaran produk app melalui email akan dilakukan 24 jam selepas pengesahan pembayaran oleh saya. 

Jika pada hujung minggu, mungkin lewat sedikit...tetapi semua akan diproses dalam tempoh 24-36 jam...

Sekian";
$answers['Log Aktivity tak gerak']="App JA memerlukan sambungan Internet untuk proses penjanaan wang (menghantar auto-promo message keluar).

Apabila battery phone anda rendah melebihi sesuatu paras, phone Android anda akan menghentikan/memperlahankan background process seperti mobile data atau penghantaran keluar email  dan lain-lain app. Ini memang feature of Android Operating System.

Ada juga setengah jenama phone yang akan sentiasa surpress background data untuk memanjangkan jangka hayat bateri anda.

Cara untuk menangani masalah ini adalah untuk anda pergi ke Settings phone anda, dan disable segala bentuk battery / power saver di phone anda.";
$answers['Xiaomi/ASUS problem']="Xiaomi dan ASUS menggunakan versi Android yg telah dimodify sedikit iaitu MIUI.  

Anda perlu enable background data/multi-tasking untuk app JutawanApp. 

LANGKAH 1 : TUTUP BATTERY SAVER

1. Via MIUI Data Manager
Go to Security Center >> Data >> Data Usage Stats >> [Click the desired App] >> [Scroll Down] >> Background Data. (Pastikan Allow)

ATAU

2. Via Power Saver
Go to Settings >> Additional Settings >> Battery >> Manager Apps Battery Usage >> Choose Apps >> [Select Desired App] >> Custom >> Background Network >> Anda perlu enable background data untuk app JutawanApp. 

LANGKAH 2 : TUTUP MEMORY OPTIMIZATION

Turn Off Memory Optimization. Anda perlu enable Developer Options. Caranya seperti dibawah
i) Pergi ke Settings - > About Phone
ii) Tap banyak kali pada MIUI version sehingga popup You are now a developer muncul
iii) Go back, click balik Settings - > Additional Settings -> Developer Options - > Memory Optimization -> PIlih Off 
iv) Reboot Phone

LANGKAH 3: KUNCI APP PADA MEMORI 
Lock Apps In Memory

- You can lock your desired / most frequently used apps in the memory.
- Open your JutawanApp, let it get fully launched, open the recent apps menu.
- Swipe down the JA app icon or recent app screen to reveal a lock icon on top left corner.
- Tap on it, this locks the app into the memory & it wont get force killed in the background.
- To unlock the app repeat the same process and tap it again to unlock it from memory.

Contoh di sini https://www.softwarert.com/lock-background-apps-xiaomi-redmi-phones/";


$answers['SMTP AsiaWeBMail']="Cuba masukkan settings SMTP dibawah di Robot - > Settings dan tekan simpan


Email : jutawan97@asiawebmail.pw
Password : password123
Host : mail.asiawebmail.pw
Port : 26
Auth (Slide ke Oren/Biru atau ON)
TLS (Biarkan Putih atau OFF)";


$answers['Referrer pergi orang lain']="Dalam kes nie susah pasal dia dah beli dengan affiliate lain... duit tu pun dah dibayar dengan dia

Jarang2 benda camnie terjadi sebenarnya...

Mungkin dia cakap yang dia beli dengan anda. tapi dia end up pegi dengan orang lain...

lain kali kalau directly deal dengan prospect suruh dia tengok referrer sapa...ada keluar kat notification page tu... untuk pastikan siapa referrer dia...";




$answers['JA tak perlu promosi']="

Pertama, terima kasih kerana membeli app ciptaan saya ini. 

Sekali lagi di tekan kan, anda TIDAK PERLU membuat promosi. Ini kerana jika anda ikut semua langkah dalam ebook, 
saya menyuruh anda membuka akaun email baru dan memasukkan username dan password akaun email tersebut ke dalam settings. 

App saya akan menggunakan emel tersebut untuk membuat auto-promo ke seluruh negara.
Anda tidak perlu ada apa-apa contacts pun, kerana semua database contacts bukan dari dalam phone anda. 
 
Sistem canggih ini akan memastikan yg phone anda yg bekerja untuk anda siang dan malam....
ianya akan bergerak selagi ada network connection di phone anda...jadi tidak menggunakan credit phone anda...

Di dalam ebook, saya juga cadangkan jikalau anda nak promosi sendiri pun OK, untuk memantapkan lagi usaha jana wang anda. Robot pun kerja, anda pun kerja..makin bagus peluang jana wang

";





?>